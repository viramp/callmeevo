package callme.evo.fragments;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.moez.QKSMS.mmssms.Utils;
import com.moez.QKSMS.ui.compose.ComposeActivity;
import com.moez.QKSMS.ui.dialog.DefaultSmsHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.channels.FileChannel;
import java.util.List;

import callme.evo.R;
import callme.evo.activity.HomeScreen;
import callme.evo.activity.LoginActivity;
import callme.evo.global.BroadcastHelper;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.Sms;
import callme.evo.models.User;


public class SettingsFragment extends Fragment {

    private CheckBox chkUnknownNumbers;
    private CheckBox chkHiddenNumber;
    private CheckBox chkFccDoNotCall;
    private CheckBox chkIdentifiedNumber;
    private CheckBox chkAllNumber;
    private CheckBox chkMsgUnknownNumber;
    private CheckBox chkMsgHiddenNumber;
    private CheckBox chkMsgAllNumbers;
    private TextView textUpgrade;

    private BroadcastHelper broadcastHelper;

    private TextView text_my_referal_code;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_eight, container, false);

        TextView txtReferalCode = (TextView) view.findViewById(R.id.text_my_referal_code);

        User user = Utility.getLoggedInUser(getActivity());
        if (Utility.getLoggedInUser(getActivity()) != null) {
            txtReferalCode.setText(user.referal_code);
        } else {
            txtReferalCode.setText("");
        }

        chkUnknownNumbers = (CheckBox) view.findViewById(R.id.chk_unknown_numbers);
        chkHiddenNumber = (CheckBox) view.findViewById(R.id.chk_hidden_number);
        chkFccDoNotCall = (CheckBox) view.findViewById(R.id.chk_fcc_do_not_call);
        chkIdentifiedNumber = (CheckBox) view.findViewById(R.id.chk_identified_number);
        chkAllNumber = (CheckBox) view.findViewById(R.id.chk_all_number);
        chkMsgUnknownNumber = (CheckBox) view.findViewById(R.id.chk_msg_unknown_number);
        chkMsgHiddenNumber = (CheckBox) view.findViewById(R.id.chk_msg_hidden_number);
        chkMsgAllNumbers = (CheckBox) view.findViewById(R.id.chk_msg_all_numbers);

        broadcastHelper = new BroadcastHelper(getActivity(), ConstantCodes.BROADCAST_PAYMENT_REFRESH);
        broadcastHelper.register(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (textUpgrade != null && isAdded()) {
                    User user = Utility.getLoggedInUser(getActivity());
                    if (user != null && user.auto_renewel == 1) {
                        textUpgrade.setText(getString(R.string.label_cancel_auto_renewal));
                    } else {
                        textUpgrade.setText(getString(R.string.label_opt_auto_renew));
                    }
                }
            }
        });
        view.findViewById(R.id.txt_share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exportDatabse("callmeevo.db");
            }
        });

        if (user != null)
            ((TextView) view.findViewById(R.id.expiry_date)).setText(Utility.changeDateFormat(user.getExpiredDate()));
        else
            ((TextView) view.findViewById(R.id.expiry_date)).setText("");

        view.findViewById(R.id.text_logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility.logoutAndRemoveLoggedInUser(getActivity());
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                getActivity().finish();
            }
        });

        final TextView textUpgradedText = (TextView) view.findViewById(R.id.upgraded_text);
        if (Utility.isPaidApplication(getActivity())) {
            textUpgradedText.setText("Upgrade Status [Upgraded Version]");
        } else {
            textUpgradedText.setText("Upgrade Status [Free Version]");
        }

        textUpgrade = (TextView) view.findViewById(R.id.text_upgrade);
        if (user != null && user.auto_renewel == 1) {
            textUpgrade.setText(getString(R.string.label_cancel_auto_renewal));
        } else {
            textUpgrade.setText(getString(R.string.label_opt_auto_renew));
        }

        textUpgrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textUpgrade.getText().toString().trim().equalsIgnoreCase(getString(R.string.label_opt_auto_renew))) {
                    if (getActivity() instanceof HomeScreen) {
                        HomeScreen homeScreen = (HomeScreen) getActivity();
                        homeScreen.upgradeAccountUsingPaypal();
                    }
                } else if (textUpgrade.getText().toString().trim().equalsIgnoreCase(getString(R.string.label_cancel_auto_renewal))) {
                    if (getActivity() instanceof HomeScreen) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                        //  builder.setTitle("Confirm");
                        builder.setMessage("Are you sure you want to cancel auto renewal?");

                        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                HomeScreen homeScreen = (HomeScreen) getActivity();
                                homeScreen.cancelAccountUsingPaypal();
                                dialog.dismiss();
                            }
                        });

                        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                // Do nothing
                                dialog.dismiss();
                            }
                        });

                        AlertDialog alert = builder.create();
                        alert.show();


                    }
                }
            }
        });

        chkUnknownNumbers.setOnCheckedChangeListener(listener);
        chkHiddenNumber.setOnCheckedChangeListener(listener);
        chkFccDoNotCall.setOnCheckedChangeListener(listener);
        chkIdentifiedNumber.setOnCheckedChangeListener(listener);
        chkAllNumber.setOnCheckedChangeListener(listener);
        chkMsgUnknownNumber.setOnCheckedChangeListener(listener);
        chkMsgHiddenNumber.setOnCheckedChangeListener(listener);
        chkMsgAllNumbers.setOnCheckedChangeListener(listener);

        SharedPreferences sharedPreferences = Utility.getSharedPreference(getActivity());
        chkUnknownNumbers.setChecked(sharedPreferences.getBoolean(ConstantCodes.UNKNOWN_NUMBER, false));
        chkHiddenNumber.setChecked(sharedPreferences.getBoolean(ConstantCodes.HIDDEN_NUMBER, false));
        chkFccDoNotCall.setChecked(sharedPreferences.getBoolean(ConstantCodes.DO_NOT_CALL, false));
        chkIdentifiedNumber.setChecked(sharedPreferences.getBoolean(ConstantCodes.IDENTIFIED_NUMBER, false));
        chkAllNumber.setChecked(sharedPreferences.getBoolean(ConstantCodes.ALL_NUMBER, false));
        chkMsgUnknownNumber.setChecked(sharedPreferences.getBoolean(ConstantCodes.MSG_UNKNOWN_NUMBER, false));
        chkMsgHiddenNumber.setChecked(sharedPreferences.getBoolean(ConstantCodes.MSG_HIDDEN_NUMBER, false));
        chkMsgAllNumbers.setChecked(sharedPreferences.getBoolean(ConstantCodes.MSG_ALL_NUMBER, false));

        view.findViewById(R.id.txt_share_facebook).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_text, Utility.getLoggedInUser(getActivity()).referal_code));
                PackageManager pm = getActivity().getPackageManager();
                List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
                for (final ResolveInfo app : activityList) {
                    if ((app.activityInfo.name).contains("facebook")) {
                        final ActivityInfo activity = app.activityInfo;
                        final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
                        shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                        shareIntent.setComponent(name);
                        startActivity(shareIntent);
                        break;
                    }
                }
            }
        });

        view.findViewById(R.id.txt_share_twitter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tweetIntent = new Intent(Intent.ACTION_SEND);
                tweetIntent.putExtra(Intent.EXTRA_TEXT, "This is a Test.");
                tweetIntent.setType("text/plain");

                PackageManager packManager = getActivity().getPackageManager();
                List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(tweetIntent, PackageManager.MATCH_DEFAULT_ONLY);

                boolean resolved = false;
                for (ResolveInfo resolveInfo : resolvedInfoList) {
                    if (resolveInfo.activityInfo.packageName.startsWith("com.twitter.android")) {
                        tweetIntent.setClassName(
                                resolveInfo.activityInfo.packageName,
                                resolveInfo.activityInfo.name);
                        resolved = true;
                        break;
                    }
                }
                if (resolved) {
                    startActivity(tweetIntent);
                } else {
                    String message = getString(R.string.share_text, Utility.getLoggedInUser(getActivity()).referal_code);
                    Intent i = new Intent();
                    i.putExtra(Intent.EXTRA_TEXT, message);
                    i.setAction(Intent.ACTION_VIEW);
                    i.setData(Uri.parse("https://twitter.com/intent/tweet?text=" + urlEncode(message)));
                    startActivity(i);
                    Toast.makeText(getActivity(), "Twitter app isn't found", Toast.LENGTH_LONG).show();
                }
            }
        });


        view.findViewById(R.id.txt_share_email).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String shareBody = getString(R.string.share_text, Utility.getLoggedInUser(getActivity()).referal_code);
                ;
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject Here");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share using"));
            }
        });
        view.findViewById(R.id.txt_share_sms).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String shareBody = getString(R.string.share_text, Utility.getLoggedInUser(getActivity()).referal_code);

                Intent sendIntent = new Intent(getActivity(), ComposeActivity.class);
                sendIntent.putExtra("body", shareBody);
                startActivity(Intent.createChooser(sendIntent, "Share using"));

                /*Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setData(Uri.parse("sms:"));
                sendIntent.putExtra("sms_body", shareBody);
                startActivity(Intent.createChooser(sendIntent, "Share using"));*/
            }
        });

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (broadcastHelper != null)
            broadcastHelper.unRegister();
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible) {
            if (getActivity() != null && isAdded()) {
                User user = Utility.getLoggedInUser(getActivity());
                if (user != null && user.auto_renewel == 1) {
                    textUpgrade.setText(getString(R.string.label_cancel_auto_renewal));
                } else {
                    textUpgrade.setText(getString(R.string.label_opt_auto_renew));
                }
            }
        }
    }

    private String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.wtf("TAG", "UTF-8 should always be supported", e);
            e.printStackTrace();
            return "";
        }
    }

    public void exportDatabse(String databaseName) {
        try {
            File sd = new File(Environment.getExternalStorageDirectory() + "/CallMeEvo/");
            File data = Environment.getDataDirectory();
            if (!sd.exists())
                sd.mkdirs();
            if (sd.canWrite()) {
                String currentDBPath = "//data//" + getActivity().getPackageName() + "//databases//" + databaseName + "";
                String backupDBPath = "callmeevo";
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    CompoundButton.OnCheckedChangeListener listener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            String key = null;
            switch (buttonView.getId()) {
                case R.id.chk_unknown_numbers:
                    key = ConstantCodes.UNKNOWN_NUMBER;
                    break;
                case R.id.chk_hidden_number:
                    key = ConstantCodes.HIDDEN_NUMBER;
                    break;
                case R.id.chk_fcc_do_not_call:
                    key = ConstantCodes.DO_NOT_CALL;
                    break;
                case R.id.chk_identified_number:
                    key = ConstantCodes.IDENTIFIED_NUMBER;
                    break;
                case R.id.chk_all_number:
                    key = ConstantCodes.ALL_NUMBER;
                    break;
                case R.id.chk_msg_unknown_number:
                    if(isChecked){
                        if (!Utils.isDefaultSmsApp(getActivity())) {
                            // Ask to become the default SMS app
                            new DefaultSmsHelper(getActivity(), R.string.not_default_send)
                                    .showIfNotDefault((ViewGroup)getActivity().getWindow().getDecorView().getRootView());
                            chkMsgUnknownNumber.setChecked(false);
                        }else{

                            key = ConstantCodes.MSG_UNKNOWN_NUMBER;

                        }
                    }else{
                        key = ConstantCodes.MSG_UNKNOWN_NUMBER;
                    }


                    break;
                case R.id.chk_msg_hidden_number:
                    if(isChecked){
                        if (!Utils.isDefaultSmsApp(getActivity())) {
                            // Ask to become the default SMS app
                            new DefaultSmsHelper(getActivity(), R.string.not_default_send)
                                    .showIfNotDefault((ViewGroup)getActivity().getWindow().getDecorView().getRootView());
                            chkMsgHiddenNumber.setChecked(false);
                        }else{

                            key = ConstantCodes.MSG_HIDDEN_NUMBER;
                        }
                    }else{
                        key = ConstantCodes.MSG_HIDDEN_NUMBER;
                    }

                    break;
                case R.id.chk_msg_all_numbers:
                    if(isChecked){
                        if (!Utils.isDefaultSmsApp(getActivity())) {
                            // Ask to become the default SMS app
                            new DefaultSmsHelper(getActivity(), R.string.not_default_send)
                                    .showIfNotDefault((ViewGroup)getActivity().getWindow().getDecorView().getRootView());
                            chkMsgAllNumbers.setChecked(false);
                        }else{

                            key = ConstantCodes.MSG_ALL_NUMBER;
                        }
                    }else{
                        key = ConstantCodes.MSG_ALL_NUMBER;
                    }

                    break;
            }

            if (!TextUtils.isEmpty(key)) {
                Utility.getSharedPreference(getActivity()).edit().putBoolean(key, isChecked).commit();
            }
        }
    };
}
