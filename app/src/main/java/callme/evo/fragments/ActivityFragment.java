package callme.evo.fragments;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CallLog;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.moez.QKSMS.mmssms.Utils;
import com.moez.QKSMS.receiver.BootReceiver;
import com.moez.QKSMS.ui.dialog.DefaultSmsHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import callme.evo.R;
import callme.evo.activity.WhiteListContactDetail;
import callme.evo.adapter.ActivityAdapter;
import callme.evo.global.BroadcastHelper;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.ContactCallLog;
import callme.evo.models.Contacts;
import callme.evo.models.Sms;


public class ActivityFragment extends Fragment {

    private ActivityAdapter mAdapter;
    private SwipeMenuListView mListView;
    private TextView txtError;
    private ProgressBar progressBar;
    private BroadcastHelper broadcastHelper;

    public ActivityFragment() {

    }

    View view;

    //    9429801969
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view != null && mListView != null && mAdapter != null) {
            mListView.setAdapter(mAdapter);
            return view;
        }
        view = inflater.inflate(R.layout.fragment_four, container, false);

        txtError = (TextView) view.findViewById(R.id.txt_error);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        broadcastHelper = new BroadcastHelper(getActivity(), ConstantCodes.BROADCAST_REFRESH);
        broadcastHelper.register(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                loadList(false);
            }
        });

        ImageView action = (ImageView) view.findViewById(R.id.action);
        action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                //  builder.setTitle("Confirm");
                builder.setMessage("Are you sure you want to delete all activities?");

                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                        new deleteActivityLog().execute();

                        dialog.dismiss();
                    }
                });

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // Do nothing
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });
        mListView = (SwipeMenuListView) view.findViewById(R.id.listView);

        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getActivity());
                // set item background
                openItem.setBackground(R.color.red);
                // set item width
                openItem.setWidth(dp2px(90));
                // set a icon
                openItem.setIcon(R.drawable.slide_delete);
                // add to menu
                menu.addMenuItem(openItem);
            }
        };
        // set creator
        mListView.setMenuCreator(creator);
        // Left

   /*@author added by Viram Purohit*/

        mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        // delete
                        ContactCallLog item = mAdapter.getItem(position);
                        Utility.deleteCallLog(getActivity(), item.callLogId, item.number);
                        new Delete().from(ContactCallLog.class).where("callLogId like '" + item.callLogId + "'").execute();
                        new Delete().from(Sms.class).where("smsId like '" + item.callLogId + "' AND message like '" + item.message + "'").execute();
                        Utility.deleteSms(getActivity(), item.callLogId);
                        mAdapter.removeItem(position);

                        break;
                    case 1:
                        // unblock
                        Toast.makeText(getActivity(), "Unblock Item", Toast.LENGTH_SHORT).show();
                        break;
                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });


        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ContactCallLog contacts = mAdapter.getItem(position);
                Intent intent = new Intent(getActivity(), WhiteListContactDetail.class);
                intent.putExtra(ConstantCodes.INTENT_NAME, contacts.name);
                intent.putExtra(ConstantCodes.INTENT_NUMBER, contacts.number);
                intent.putExtra(ConstantCodes.INTENT_PHOTO, contacts.thumbnailImage);
                intent.putExtra(ConstantCodes.INTENT_FROM, ConstantCodes.BLACK_LIST.equalsIgnoreCase(contacts.blackListOrWhiteList) ? ConstantCodes.BLACK_LIST : ConstantCodes.WHITE_LIST);
                startActivity(intent);
            }
        });
        loadList(true);

        return view;
    }


    private void  removeAllSMS(){

        if (!Utils.isDefaultSmsApp(getActivity())) {
            // Ask to become the default SMS app
            new DefaultSmsHelper(getActivity(), R.string.not_default_send)
                    .showIfNotDefault((ViewGroup)getActivity().getWindow().getDecorView().getRootView());
        }else{
            new Delete().from(Sms.class).execute();

            Cursor c = getActivity().getApplicationContext().
                    getContentResolver().query(Uri.parse("content://sms/"), null, null, null,null);
            try {
                while (c.moveToNext()) {
                    int id = c.getInt(0);
                    getActivity().getApplicationContext().
                            getContentResolver().delete(Uri.parse("content://sms/" + id), null, null);
                }

            }catch(Exception e){
                Log.e(this.toString(),"Error deleting sms",e);
            }finally {
                c.close();
            }
        }

//        Uri deleteUri = Uri.parse("content://sms/inbox");
//        int count = 0;
//        ContentResolver cr = getActivity().getContentResolver();
//
//        Cursor c1 = null; // Default sort order
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
//             final String[] PROJECTION = {
//                    Telephony.Sms.Inbox._ID,
//                    Telephony.Sms.Inbox.THREAD_ID,
//                    Telephony.Sms.Inbox.ADDRESS,
//                    Telephony.Sms.Inbox.BODY,
//                    Telephony.Sms.Inbox.PERSON,
//            };
//
//            c1 = cr.query(Telephony.Sms.Inbox.CONTENT_URI, // Official CONTENT_URI from docs
//                    PROJECTION, // Select body text
//                    null,
//                    null,
//                    Telephony.Sms.Inbox.DEFAULT_SORT_ORDER);
//            int totalSMS = c1.getCount();
//
//            if (c1.moveToFirst()) {
//                for (int i = 0; i < totalSMS; i++) {
//                    System.out.print("\ncurgetCount --------SUBSCRIPTION_ID " + c1.getString(0));
//                    System.out.print("\ncurgetCount --------ADDRESS " + c1.getString(2));
//                    count = getActivity().getContentResolver().delete(
//                            Telephony.Sms.CONTENT_URI,"address = "+ c1.getString(2),null);
//
//                    c1.moveToNext();
//                }
//            } else {
//                System.out.print("\n You have no SMS in Inbox");
//            }
//            c1.close();
//        }else{
//            Cursor cursor = getActivity().getContentResolver().query(deleteUri,
//                    new String[] { "_id", "thread_id", "address",
//                            "person", "date", "body" }, null, null, null);
//            System.out.print("\ncurgetCount --------sor " + cursor.getCount());
//            while (cursor.moveToNext()) {
//                try {
//                    // Delete the SMS
//                    long pid = cursor.getLong(0); // Get id;
//                    Uri uriSms = Uri.parse("content://sms/inbox");
//                    Cursor c = getActivity().getContentResolver().query(uriSms,
//                            new String[] { "_id", "thread_id", "address",
//                                    "person", "date", "body" }, null, null, null);
//
//                    if (c != null && c.moveToFirst()) {
//                        do {
//                            long id = c.getLong(0);
//                            long threadId = c.getLong(1);
//
//                            if (id == pid) {
//                                System.out.print("\n Deleting SMS with id: " + threadId);
//                                count = getActivity().getContentResolver().delete(
//                                        Uri.parse("content://sms/" + id), null, null);
//                            }
//                        } while (c.moveToNext());
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//            cursor.close();
//        }
//
//
//
//
//
//        return count;
    }

    private int removeAllCall(){
        int count = 0;
        String[] projection = new String[]{CallLog.Calls.NUMBER,
                CallLog.Calls.TYPE,
                CallLog.Calls.DURATION,
                CallLog.Calls.CACHED_NAME,
                CallLog.Calls._ID};

        Cursor c;
        c=getActivity().getContentResolver().query(CallLog.Calls.CONTENT_URI, projection, null, null, CallLog.Calls._ID + " DESC");
        System.out.println("getCount---------:   "+c.getCount());
        while(c.moveToNext()){
            String lastCallnumber = c.getString(0);
            String type=c.getString(1);
            String duration=c.getString(2);
            String name=c.getString(3);
            String id=c.getString(4);
            System.out.println("CALLLLing:"+lastCallnumber+"Type:"+type);
            System.out.println("id:   "+id+" name: "+name);

            count = getActivity().getContentResolver().delete(android.provider.CallLog.Calls.CONTENT_URI,"_ID = "+ id,null);

            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        c.close();
        return count;
    }
@Override
    public void onDestroy() {
        super.onDestroy();
        if(broadcastHelper!=null)
            broadcastHelper.unRegister();
    }

    ArrayList<ContactCallLog> list;

    private void loadList(final boolean showProgress) {

        new AsyncTask<Void, Void, ArrayList<ContactCallLog>>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if (showProgress)
                    progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected ArrayList<ContactCallLog> doInBackground(Void... params) {
                list = getCallDetailsNew();
                return null;
            }

            @Override
            protected void onPostExecute(ArrayList<ContactCallLog> contactCallLogs) {
                super.onPostExecute(contactCallLogs);
                progressBar.setVisibility(View.GONE);

                if (list != null && list.size() > 0) {

                    mAdapter = new ActivityAdapter(getActivity(), list);

                    mListView.setAdapter(mAdapter);

                    mListView.setVisibility(View.VISIBLE);

                    txtError.setVisibility(View.GONE);

                } else {

                    txtError.setText("There are no recent activity");

                    txtError.setVisibility(View.VISIBLE);

                    mListView.setVisibility(View.GONE);

                }
            }
        }.execute();
    }

    private ArrayList<ContactCallLog> getCallDetailsNew() {

        String query = "SELECT tab.mainId, tab.number, Contacts.name, tab.message, " +
                "Contacts.blackListOrWhiteList, tab.timestamp, " +
                "Contacts.thumbnailImage FROM (SELECT ContactCallLog.callLogId as mainId,ContactCallLog.number," +
                " \"\" as message, ContactCallLog.timestamp as timestamp FROM ContactCallLog\n" +
                "UNION \n" +
                "SELECT Sms.smsId  as mainId,Sms.number, Sms.message, Sms.date as timestamp FROM Sms)\n" +
                "as tab\n" +
                "LEFT JOIN" +
                " Contacts on contacts.number like '%'||tab.number OR tab.number like '%'||contacts.number\n" +
                "GROUP BY Contacts.name,tab.number\n" +
                "ORDER BY tab.timestamp DESC ,Contacts.priority DESC";//@Viram Purohit Changes
//                "ORDER BY Contacts.priority DESC, tab.timestamp DESC ";

//        String query = "SELECT tab.mainId, Contacts.priority, Contacts.name,tab.number, tab.message, tab.timestamp, Contacts.blackListOrWhiteList, Contacts.thumbnailImage FROM " +
//                "( " +
//                "SELECT ContactCallLog.callLogId as mainId, ContactCallLog.number as number,ContactCallLog.message, ContactCallLog.timestamp as timestamp,'' as name FROM ContactCallLog " +
//                "UNION SELECT Sms.smsId as mainId, Sms.number as number,Sms.message, Sms.date as timestamp, Sms.name as name FROM Sms " +
//                " ) as tab  LEFT JOIN Contacts ON " +
//                " (Contacts.number like '%'||tab.number OR tab.number like '%'||Contacts.number)  " +
//                "GROUP BY tab. name, tab.number, tab.message " +
//                "ORDER BY Contacts.priority DESC,tab.timestamp DESC;";
        /*String query = "SELECT tab.mainId, Contacts.name,tab.number, tab.message, tab.timestamp, Contacts.blackListOrWhiteList, Contacts.thumbnailImage FROM ( " +
                "SELECT ContactCallLog.callLogId as mainId, ContactCallLog.number as number,ContactCallLog.message, ContactCallLog.timestamp as timestamp,'' as name FROM ContactCallLog " +
                "UNION " +
                "SELECT Sms.smsId as mainId, Sms.number as number,Sms.message, Sms.date as timestamp, Sms.name FROM Sms GROUP BY name, number " +
                ") as tab " +
                "LEFT JOIN Contacts ON (Contacts.cleanNumber like '%'||tab.number OR tab.number like '%'||Contacts.cleanNumber) " +
                "GROUP BY tab.name " +
                "ORDER BY tab.timestamp DESC; ";*/

        /*String query = "SELECT Contacts.name,tab.number, tab.message, tab.timestamp, Contacts.blackListOrWhiteList, Contacts.thumbnailImage FROM (\n" +
                "SELECT ContactCallLog.number as number,ContactCallLog.message, ContactCallLog.timestamp as timestamp,'' as name FROM ContactCallLog\n" +
                "UNION\n" +
                "SELECT Sms.number as number,Sms.message, Sms.date as timestamp, Sms.name FROM Sms\n" +
                ") as tab\n" +
                "LEFT JOIN Contacts ON (Contacts.number like '%'||tab.number OR tab.number like '%'||Contacts.number)\n" +
                "ORDER BY tab.timestamp DESC;";*/


//        String query = "SELECT ContactCallLog.name as name, Contacts.number as number,ContactCallLog.message, ContactCallLog.timestamp as timestamp, Contacts.blackListOrWhiteList as blackListOrWhiteList, Contacts.thumbnailImage as thumbnailImage FROM ContactCallLog LEFT JOIN Contacts ON ContactCallLog.number like '%' || Contacts.number  ORDER BY Contacts.priority DESC, ContactCallLog.timestamp DESC";
        Cursor cursor = ActiveAndroid.getDatabase().
                rawQuery(query, null);

        ArrayList<ContactCallLog> list = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            do {
                ContactCallLog contacts = new ContactCallLog();
                contacts.callLogId = cursor.getLong(cursor.getColumnIndex("mainId"));
                contacts.name = cursor.getString(cursor.getColumnIndex("name"));
                contacts.message = cursor.getString(cursor.getColumnIndex("message"));
                contacts.number = cursor.getString(cursor.getColumnIndex("number"));
                contacts.timestamp = cursor.getLong(cursor.getColumnIndex("timestamp"));
                contacts.blackListOrWhiteList = cursor.getString(cursor.getColumnIndex("blackListOrWhiteList"));
                contacts.thumbnailImage = cursor.getString(cursor.getColumnIndex("thumbnailImage"));
                if (contacts.number != null)
                    list.add(contacts);
            } while (cursor.moveToNext());
        }
        return list;
    }

    private ArrayList<Contacts> getCallDetails() {

        ArrayList<Contacts> recentsCalls = new ArrayList<Contacts>();
        StringBuffer sb = new StringBuffer();
        Cursor managedCursor = getActivity().managedQuery(CallLog.Calls.CONTENT_URI, null, null, null, CallLog.Calls.DATE + " DESC");
        int name = managedCursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
//        sb.append("Call Details :");
        while (managedCursor.moveToNext()) {
            String phName = managedCursor.getString(name);
            String phNumber = managedCursor.getString(number);
            String callType = managedCursor.getString(type);
            String callDate = managedCursor.getString(date);
            Date callDayTime = new Date(Long.valueOf(callDate));
            String callDuration = managedCursor.getString(duration);
            String dir = null;
            int dircode = Integer.parseInt(callType);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "OUTGOING";
                    break;

                case CallLog.Calls.INCOMING_TYPE:
                    dir = "INCOMING";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "MISSED";
                    break;
            }

            Contacts contacts = new Contacts();

            contacts.name = phName;
            contacts.number = phNumber;
            contacts.timestamp = Long.valueOf(callDate);

            recentsCalls.add(contacts);

//            sb.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- " + dir + " \nCall Date:--- " + callDayTime + " \nCall duration in sec :--- " + callDuration);
//            sb.append("\n----------------------------------");
        }

//        managedCursor.close();
        return mergeAndSort(recentsCalls);
    }

    private ArrayList<Contacts> mergeAndSort(ArrayList<Contacts> list) {
        /*ArrayList<Contacts> listAllContacts = new Select().from(Contacts.class).execute();
        for (Contacts listAllContact : listAllContacts) {
            for (Contacts contacts : list) {
                if (Utility.getFinalNumber(listAllContact.number).equalsIgnoreCase(Utility.getFinalNumber(contacts.number))) {
                    contacts.priority = listAllContact.priority;
                    contacts.name = listAllContact.name;
                }
            }
        }*/

        Collections.sort(list, new Comparator<Contacts>() {
            @Override
            public int compare(Contacts lhs, Contacts rhs) {
                if (lhs.priority > rhs.priority) return -1;
                if (lhs.priority < rhs.priority) return 1;
                else return 0;
            }
        });

        return list;
    }


    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }
    private class deleteActivityLog extends AsyncTask<Void,Void,Boolean>{

        @Override
        protected Boolean doInBackground(Void... params) {
            Log.e( " TAG "," Delete Call  "+ removeAllCall());
            new Delete().from(ContactCallLog.class).execute();
//            removeAllSMS();
            boolean isDefault =Utils.isDefaultSmsApp(getActivity());
            if(isDefault){
                new Delete().from(Sms.class).execute();

                Cursor c = getActivity().getApplicationContext().
                        getContentResolver().query(Uri.parse("content://sms/"), null, null, null,null);
                try {
                    while (c.moveToNext()) {
                        int id = c.getInt(0);
                        getActivity().getApplicationContext().
                                getContentResolver().delete(Uri.parse("content://sms/" + id), null, null);
                    }

                }catch(Exception e){
                    Log.e(this.toString(),"Error deleting sms",e);
                }finally {
                    c.close();
                }
            }
            return isDefault;
        }

        @Override
        protected void onPostExecute(Boolean isDefault) {
            super.onPostExecute(isDefault);
            if(!isDefault){
                // Ask to become the default SMS app
                new DefaultSmsHelper(getActivity(), R.string.not_default_send)
                        .showIfNotDefault((ViewGroup)getActivity().getWindow().getDecorView().getRootView());
            }
            loadList(false);
        }
    }
}
