package callme.evo.fragments;

import android.support.v4.app.Fragment;
import android.util.Log;

/**
 * Created by pankaj on 1/8/16.
 */
public class BaseFragment extends Fragment {
    int _menuVisible = -1;
    boolean resumeCalled = false;

    @Override
    public void onResume() {
        super.onResume();

        if (_menuVisible == -1) {
            if (isMenuVisible())
                onFragmentResume();
        }
        _menuVisible = -1;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (_menuVisible == -1) {
            if (isMenuVisible())
                onFragmentPause();
        }
        _menuVisible = -1;
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);

        if (menuVisible) {
            this._menuVisible = 1;
            onFragmentResume();
        } else {
            this._menuVisible = 0;
            if (resumeCalled)
                onFragmentPause();
        }
    }

    public void onFragmentResume() {
        resumeCalled = true;
        Log.d("STATES", "onFragmentResume() called with: " + "");
    }

    public void onFragmentPause() {
        resumeCalled = false;
        Log.d("STATES", "onFragmentPause() called with: " + "");
    }
}
