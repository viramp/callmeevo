package callme.evo.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.activeandroid.OnReadAsync;
import com.activeandroid.query.Select;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import java.util.ArrayList;

import callme.evo.R;
import callme.evo.activity.HomeScreen;
import callme.evo.activity.WhiteListContactDetail;
import callme.evo.activity.dialog_box.blocklist_dialog.AddtoBlocklist;
import callme.evo.adapter.CallAdapter;
import callme.evo.global.BroadcastHelper;
import callme.evo.global.ConstantCodes;
import callme.evo.models.Contacts;


public class BlacklistFragment extends Fragment {


    public BlacklistFragment() {
        // Required empty public constructor
    }


    private ImageView imgAction;
    private CallAdapter mAdapter;
    private SwipeMenuListView mListView;
    private TextView txtError;
    private ProgressBar progressBar;
    private BroadcastHelper broadcastHelper;

    private View view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        if (view != null && mAdapter != null)
            return view;

        view = inflater.inflate(R.layout.fragment_two, container, false);

        broadcastHelper = new BroadcastHelper(getActivity(), ConstantCodes.BROADCAST_REFRESH);
        broadcastHelper.register(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                loadList();
            }
        });

        txtError = (TextView) view.findViewById(R.id.txt_error);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mListView = (SwipeMenuListView) view.findViewById(R.id.listView);

        mAdapter = new CallAdapter(getActivity());
        mListView.setAdapter(mAdapter);

        imgAction = (ImageView) view.findViewById(R.id.action);


        mListView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        loadList();


        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getActivity());
                // set item background
                openItem.setBackground(R.color.red);
                // set item width
                openItem.setWidth(dp2px(90));
                // set a icon
                openItem.setIcon(R.drawable.slide_delete);
                // add to menu
                menu.addMenuItem(openItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getActivity());
                // set item background
                deleteItem.setBackground(R.color.green);
                // set item width
                deleteItem.setWidth(dp2px(90));
                // set a icon
                deleteItem.setIcon(R.drawable.slide_unblock);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        // set creator
        mListView.setMenuCreator(creator);

        // Left
        mListView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);

           /*@author added by Viram Purohit*/

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Contacts contacts = mAdapter.getItem(position);
                Intent intent = new Intent(getActivity(), WhiteListContactDetail.class);
                intent.putExtra(ConstantCodes.INTENT_NAME, contacts.name);
                intent.putExtra(ConstantCodes.INTENT_NUMBER, contacts.number);
                intent.putExtra(ConstantCodes.INTENT_PHOTO, contacts.thumbnailImage);
                intent.putExtra(ConstantCodes.INTENT_FROM, ConstantCodes.BLACK_LIST);
                startActivity(intent);
            }
        });


        mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        // delete

                        // delete
//                        Toast.makeText(getActivity(), "Delete Item", Toast.LENGTH_SHORT).show();
                        Contacts item1 = mAdapter.getItem(position);
                        item1.block_type = "";
                        item1.blackListOrWhiteList = "";
                        item1.update(new String[]{"block_type", "blackListOrWhiteList"});
                        loadList();
//                        Intent intentBroadcast = new Intent(ConstantCodes.BROADCAST_REFRESH);
//                        BroadcastHelper.sendBroadcast(getActivity(), intentBroadcast);
//                        Toast.makeText(getActivity(), "Delete Item", Toast.LENGTH_SHORT).show();

                        break;
                    case 1:
                        // unblock
                        Contacts item = mAdapter.getItem(position);
                        item.block_type = "";
                        item.blackListOrWhiteList = "";
                        item.update(new String[]{"block_type", "blackListOrWhiteList"});
                        loadList();
//                        Intent intentBroadcast1 = new Intent(ConstantCodes.BROADCAST_REFRESH);
//                        BroadcastHelper.sendBroadcast(getActivity(), intentBroadcast1);
                        break;
                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadList();
    }

    private void loadList() {
        new Select().from(Contacts.class)
                .where("blackListOrWhiteList=?", new String[]{ConstantCodes.BLACK_LIST})
                .orderBy("name ASC")
                .executeAsync(new OnReadAsync<Contacts>() {
                    @Override
                    public void onRead(ArrayList<Contacts> list) {

                        progressBar.setVisibility(View.GONE);

                        if (list != null && list.size() > 0) {

                            mAdapter.updateItem(list);

                            mListView.setVisibility(View.VISIBLE);

                            txtError.setVisibility(View.GONE);

                        } else {

                            txtError.setText("There are no numbers on your Blacklist");

                            txtError.setVisibility(View.VISIBLE);

                            mListView.setVisibility(View.GONE);

                        }

                    }
                });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        imgAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intentAddToBlockList = new Intent(getActivity(), AddtoBlocklist.class);
                startActivityForResult(intentAddToBlockList, ACTION_BLOCK_NUMBER);


            }
        });

    }


    private final int ACTION_BLOCK_NUMBER = 101;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == ACTION_BLOCK_NUMBER) {
            ((HomeScreen) getActivity()).viewPager.setCurrentItem(data.getIntExtra(ConstantCodes.INTENT_TO_SCREEN, 1));
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (broadcastHelper != null)
            broadcastHelper.unRegister();
    }

    private int dp2px(int dp) {

        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,

                getResources().getDisplayMetrics());

    }



    /*class AppAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return 10;
        }

        @Override
        public ApplicationInfo getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(getActivity(),
                        R.layout.list_item_block_today, null);
            }

            return convertView;
        }


    }*/

}
