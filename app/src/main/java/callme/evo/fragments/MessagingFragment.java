package callme.evo.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.database.Cursor;
import android.database.sqlite.SqliteWrapper;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.moez.QKSMS.LogTag;
import com.moez.QKSMS.data.Contact;
import com.moez.QKSMS.data.Conversation;
import com.moez.QKSMS.data.RecipientIdCache;
import com.moez.QKSMS.transaction.SmsHelper;
import com.moez.QKSMS.ui.compose.ComposeActivity;
import com.moez.QKSMS.ui.messagelist.MessageListActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import callme.evo.BlockableFrameLayout;
import callme.evo.R;
import callme.evo.activity.MessageSetting;
import callme.evo.activity.TranscribedMessagedetail;
import callme.evo.activity.WhiteListContactDetail;
import callme.evo.activity.dialog_box.blocklist_dialog.AddtoBlocklistManually;
import callme.evo.adapter.MessageAdapter;
import callme.evo.controller.DeleteTranscribeMessageController;
import callme.evo.global.BroadcastHelper;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.Contacts;
import callme.evo.models.Sms;
import callme.evo.models.TranscribedMessageModel;
import callme.evo.models.User;


public class MessagingFragment extends Fragment {
    private static final String TAG = "MessagingFragment";
    private static final int REQ_CODE_SPEECH_INPUT = 101;
    private static final int REQ_OPEN_DETAIL = 102;

    public MessagingFragment() {
        // Required empty public constructor
    }

    View view;

    private AppAdapter2 appAdapter2;
    //    private AppAdapter mAdapter;
    private SwipeMenuListView mListView, mListView1;
    private ProgressBar progressBarMessage, progressBarMessageVoice;
    private MessageAdapter mAdapter;

    ImageView messagesetting;
    private BroadcastHelper broadcastHelper, broadcastHelperReadFlag;
    private BlockableFrameLayout layoutTranscribedMsg;
    private TextView text_transcribed_message;
    //    SpeechSynthesis synthesis;
    private DeleteTranscribeMessageController deleteTranscribeMessageController;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view != null && mAdapter != null && mListView != null && mAdapter.getCount() > 0) {
            mListView.setAdapter(mAdapter);
            Utility.prepareTTSEngine(getActivity());
            return view;
        }
        copyDbToSdcard();

        Utility.prepareTTSEngine(getActivity());
        // create ContextThemeWrapper from the original Activity Context with the custom theme
        final Context contextThemeWrapper = new ContextThemeWrapper(getActivity(), R.style.CustomToolbarTheme);

// clone the inflater using the ContextThemeWrapper
        LayoutInflater localInflater = inflater.cloneInContext(contextThemeWrapper);

// inflate the layout using the cloned inflater, not default inflater

        view = localInflater.inflate(R.layout.fragment_six, container, false);

        deleteTranscribeMessageController = new DeleteTranscribeMessageController(getActivity(), new DeleteTranscribeMessageController.DeleteTranscribeMessageCallback() {
            @Override
            public void onSuccess() {
                Toast.makeText(getActivity(), "Successfully deleted", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFail(String message) {
                showLogE(message);
            }
        });


        text_transcribed_message = (TextView) view.findViewById(R.id.text_transcribed_message);
        layoutTranscribedMsg = (BlockableFrameLayout) view.findViewById(R.id.layout_transcribed_msg);
        broadcastHelper = new BroadcastHelper(getActivity(), ConstantCodes.BROADCAST_REFRESH);
        broadcastHelper.register(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                loadAllSms();
                User user = Utility.getLoggedInUser(getActivity());
                loadAllVoiceMessageFromServer(user.iUserID);
            }
        });

        broadcastHelperReadFlag = new BroadcastHelper(getActivity(), ConstantCodes.BROADCAST_READ_FLAG);
        broadcastHelperReadFlag.register(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                loadAllSms();
            }
        });

        if (Utility.isPaidApplication(getActivity())) {
            text_transcribed_message.setText("TRANSCRIBED VOICEMAIL MESSAGES");
            layoutTranscribedMsg.setBlockChild(false);
        } else {
            text_transcribed_message.setText("TRANSCRIBED MESSAGES(UPGRADED FEATURE)");
            layoutTranscribedMsg.setBlockChild(true);
        }

        progressBarMessage = (ProgressBar) view.findViewById(R.id.progressBarMessage);
        progressBarMessageVoice = (ProgressBar) view.findViewById(R.id.progressBarMessageVoice);

        messagesetting = (ImageView) view.findViewById(R.id.messagesetting);
        messagesetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MessageSetting.class);
                startActivity(intent);
            }
        });
        mListView = (SwipeMenuListView) view.findViewById(R.id.listView);
        mAdapter = new MessageAdapter(getActivity());
        mListView.setAdapter(mAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Sms contacts = mAdapter.getItem(position);
                contacts.isRead = 1;
                mAdapter.notifyDataSetChanged();

                Utility.updateReadStatus(getActivity(), contacts.smsId + "");

                MessageListActivity.launch(getActivity(), contacts.smsId, -1, null, true);

                Intent intent = new Intent(getActivity(), WhiteListContactDetail.class);
                intent.putExtra(ConstantCodes.INTENT_FROM_MESSAGE, true);
                intent.putExtra(ConstantCodes.INTENT_NAME, contacts.name);
                intent.putExtra(ConstantCodes.INTENT_NUMBER, contacts.number);
                intent.putExtra(ConstantCodes.INTENT_PHOTO, contacts.thumbnailImage);
                intent.putExtra(ConstantCodes.INTENT_FROM, ConstantCodes.BLACK_LIST.equalsIgnoreCase(contacts.blackListOrWhiteList) ? ConstantCodes.BLACK_LIST : ConstantCodes.WHITE_LIST);
//                startActivity(intent);
            }
        });

//        mAdapter = new AppAdapter();
//        mListView.setAdapter(mAdapter);
        loadAllSms();

        mListView1 = (SwipeMenuListView) view.findViewById(R.id.listView1);

        User user = Utility.getLoggedInUser(getActivity());
        if (user != null) {
            loadAllVoiceMessageFromServer(user.iUserID);
        }
        /*
        mListView1.setAdapter(new AppAdapter2());
        mListView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), TranscribedMessagedetail.class);
                startActivity(intent);
            }
        });*/

        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getActivity());
                // set item background
                openItem.setBackground(R.color.red);
                // set item width
                openItem.setWidth(dp2px(90));
                // set a icon
                openItem.setIcon(R.drawable.slide_delete);
                // add to menu
                menu.addMenuItem(openItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(getActivity());
                // set item background
                deleteItem.setBackground(R.color.green);
                // set item width
                deleteItem.setWidth(dp2px(90));
                // set a icon
                deleteItem.setIcon(menu.getViewType() == MessageAdapter.VIEW_TYPE_UNBLOCKED ? R.drawable.slide_block : R.drawable.slide_unblock);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        // set creator
        mListView.setMenuCreator(creator);
        // Left
        mListView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
   /*@author added by Viram Purohit*/
        mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        // delete


                        final String myPackageName = getActivity().getPackageName();
                        boolean isDefaultApp = false;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            if (!Telephony.Sms.getDefaultSmsPackage(getActivity()).equals(myPackageName)) {
                                isDefaultApp = false;

                            } else {
                                isDefaultApp = true;
                            }
                        } else {
                            isDefaultApp = true;
                        }
//                        Toast.makeText(getActivity(), "Delete Item", Toast.LENGTH_SHORT).show();
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                        //  builder.setTitle("Confirm");
                        builder.setMessage(!isDefaultApp ? "CallMeEvo must be set as the default SMS app to delete messages, after deleteing, please change the default SMS app back to your previously selected app" : "Are you sure you want to delete this sms?");

                        if (!isDefaultApp) {
                            builder.setPositiveButton("MAKE DEFAULT", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                    Sms item = mAdapter.getItem(position);
                                    Intent intent = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
                                    intent.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, myPackageName);
                                    startActivity(intent);
                                }
                            });
                        } else {
                            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                    Sms item = mAdapter.getItem(position);
                                    Utility.deleteSms(getActivity(), item.smsId);
//                                    Toast.makeText(getActivity(), "Delete Item", Toast.LENGTH_SHORT).show();
                                    new Delete().from(Sms.class).where("smsId like '" + item.smsId + "'").execute();
                                    mAdapter.removeItem(position);
                                    dialog.dismiss();
                                }
                            });
                        }

                        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                // Do nothing
                                dialog.dismiss();
                            }
                        });

                        AlertDialog alert = builder.create();
                        alert.show();

                        break;
                    case 1:

                        Sms item = mAdapter.getItem(position);
                        if (menu.getViewType() == MessageAdapter.VIEW_TYPE_BLOCKED) {
                            //unblock this number here
                            Contacts contacts = new Select().from(Contacts.class).where("number LIKE '%" + item.number + "' OR '" + item.number + "' LIKE '%'||number").executeSingle();
                            contacts.block_type = "";
                            contacts.blackListOrWhiteList = "";
                            contacts.update(new String[]{"block_type", "blackListOrWhiteList"});

                            Intent intentBroadcast1 = new Intent(ConstantCodes.BROADCAST_REFRESH);
                            BroadcastHelper.sendBroadcast(getActivity(), intentBroadcast1);
                        } else {
                            //block this number here
                            Intent intent = new Intent(getActivity(), AddtoBlocklistManually.class);
                            intent.putExtra(ConstantCodes.INTENT_NAME, item.name);
                            intent.putExtra(ConstantCodes.INTENT_NUMBER, item.number);
                            startActivity(intent);
                        }
                        break;
                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });
//fab click listener
        ImageView action = (ImageView) view.findViewById(R.id.action);
        action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ComposeActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }



/*
    private void prepareTTSEngine() {
        try {
            synthesis = SpeechSynthesis.getInstance(getActivity());
            synthesis.setSpeechSynthesisEvent(new SpeechSynthesisEvent() {

                public void onPlaySuccessful() {
                    android.util.Log.i(TAG, "onPlaySuccessful");
                }

                public void onPlayStopped() {
                    android.util.Log.i(TAG, "onPlayStopped");
                }

                public void onPlayFailed(Exception e) {
                    android.util.Log.e(TAG, "onPlayFailed");
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Error[TTSActivity]: " + e.toString())
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }

                public void onPlayStart() {
                    android.util.Log.i(TAG, "onPlayStart");
                }

                @Override
                public void onPlayCanceled() {
                    android.util.Log.i(TAG, "onPlayCanceled");
                }
            });
            synthesis.setStreamType(AudioManager.STREAM_MUSIC);
        } catch (InvalidApiKeyException e) {
            android.util.Log.e(TAG, "Invalid API key\n" + e.getStackTrace());
            Toast.makeText(getActivity(), "ERROR: Invalid API key", Toast.LENGTH_LONG).show();
        }

    }
*/

    private void loadAllVoiceMessageFromServer(final String userId) {
        progressBarMessageVoice.setVisibility(View.VISIBLE);
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, ConstantCodes.Web.GET_ALL_VOICE_MAIL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressBarMessageVoice.setVisibility(View.GONE);
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.LOGIN);
                showLog("RESPONSE : " + response.toString());
                showLog("**********");

                android.util.Log.i("CONTACT", " RESPONSE" + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has(ConstantCodes.STATUS) && jsonObject.getString(ConstantCodes.STATUS).equalsIgnoreCase(ConstantCodes.SUCCESS)) {

                        JSONArray jsonArray = jsonObject.optJSONArray(ConstantCodes.DATA);
                        ArrayList<TranscribedMessageModel> transcribedMessageModelArrayList = new ArrayList<>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonTranscribedMessage = jsonArray.optJSONObject(i);
                            String incomingNumber = jsonTranscribedMessage.optString("vPhone");
                            ArrayList<Contacts> list = new Select().from(Contacts.class)
                                    .where(" '" + incomingNumber + "' LIKE '%'||number")
                                    .or(" number like '%" + incomingNumber + "'").execute();

                            TranscribedMessageModel transcribedMessageModel = new TranscribedMessageModel(jsonTranscribedMessage);
                            if (list != null && list.size() > 0) {
                                transcribedMessageModel.name = list.get(0).name;
                                transcribedMessageModel.priority = list.get(0).priority;
                            } else {
                                transcribedMessageModel.name = incomingNumber;
                                transcribedMessageModel.priority = 0;
                            }
                            transcribedMessageModelArrayList.add(transcribedMessageModel);
                        }

                        sortList(transcribedMessageModelArrayList);

                        appAdapter2 = new AppAdapter2(transcribedMessageModelArrayList);
                        mListView1.setAdapter(appAdapter2);
                        mListView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Intent intent = new Intent(getActivity(), TranscribedMessagedetail.class);
                                TranscribedMessageModel item = appAdapter2.getItem(position);
                                intent.putExtra(ConstantCodes.DATA, Utility.getJsonString(item));
                                intent.putExtra(ConstantCodes.POSITION, position);
                                startActivityForResult(intent, REQ_OPEN_DETAIL);
                            }
                        });
                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressBarMessageVoice.setVisibility(View.GONE);
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.LOGIN);
                showLogE("RESPONSE : " + (error.networkResponse == null ? (error != null ? error.getLocalizedMessage() : "NullVolleyError") : new String(error.networkResponse.data)));
                showLog("**********");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = Utility.getDefaultParams();
//                params.put("phone_number", incommingNumber);
//                params.put("eType", voiceType);
//                params.put("tScript", message);
                showLog(params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = Utility.getDefaultHeaders();
                headers.put(ConstantCodes.iUserID, userId);
                showLog(headers.toString());
                return headers;
                /*Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token 04af7328b2346602ad7d581ff738eb25a5482cbf");
                params.put("Apikey", "JXn8e6C29jhZ065i8wyQktY33YD3s9zy");
                return params;*/
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        Utility.getRequestQueue().add(jsonObjReq);
    }

    private void sortList(ArrayList<TranscribedMessageModel> transcribedMessageModelArrayList) {
        Collections.sort(transcribedMessageModelArrayList, new MyComparator());
    }

    class MyComparator implements Comparator<TranscribedMessageModel> {
        @Override
        public int compare(TranscribedMessageModel o1, TranscribedMessageModel o2) {
            if (o1.priority > o2.priority) {
                return -1;
            } else if (o1.priority < o2.priority) {
                return 1;
            }
            return 0;
        }
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible && text_transcribed_message != null && layoutTranscribedMsg != null) {
            if (Utility.isPaidApplication(getActivity())) {
                text_transcribed_message.setText("TRANSCRIBED VOICEMAIL MESSAGES");
                layoutTranscribedMsg.setBlockChild(false);
            } else {
                text_transcribed_message.setText("TRANSCRIBED MESSAGES(UPGRADED FEATURE)");
                layoutTranscribedMsg.setBlockChild(true);
            }
        }
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    class AppAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return 10;
        }

        @Override
        public ApplicationInfo getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(getActivity(),
                        R.layout.list_item_block_today, null);
            }

            return convertView;
        }
    }

    class AppAdapter2 extends BaseAdapter {
        ArrayList<TranscribedMessageModel> list;

        AppAdapter2(ArrayList<TranscribedMessageModel> list) {
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public TranscribedMessageModel getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder viewHolder;
            if (convertView == null) {
                convertView = View.inflate(getActivity(),
                        R.layout.transcribed_message_list_item, null);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            final TranscribedMessageModel model = getItem(position);
            viewHolder.textMessage.setText(model.tScript);
            viewHolder.textName.setText(model.name);

            viewHolder.textDate.setText(model.date);
            viewHolder.textTime.setText(model.time);

            viewHolder.imgPlayVoiceMessage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utility.speak(getActivity(), model.eType, model.tScript);
                }
            });
            viewHolder.textReplyByAudio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Utility.speak(getActivity(), model.eType, model.tScript);
                    promptSpeechInput(model.phone_number);
                }
            });
            viewHolder.textReplyByText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Utility.speak(getActivity(), model.eType, model.tScript);
                    sendByAudio(model.phone_number);
                }
            });

            viewHolder.imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteTranscribeMessageController.delete(model.iScriptID, Utility.getLoggedInUser(getActivity()).iUserID);
                    list.remove(position);
                    notifyDataSetChanged();
                }
            });
            return convertView;
        }

        public void removeItem(int position) {
            list.remove(position);
            notifyDataSetChanged();
        }

        public void update(int position, String isHelpfull) {
            list.get(position).isHelpfull = isHelpfull;
        }

        public class ViewHolder {
            public TextView textName, textMessage, textDate, textTime, reply, textReplyByAudio, textReplyByText;

            public ImageView imgDelete, imgPlayVoiceMessage;

            public ViewHolder(View convertView) {
                textName = (TextView) convertView.findViewById(R.id.text_name);
                textMessage = (TextView) convertView.findViewById(R.id.text_message);
                textDate = (TextView) convertView.findViewById(R.id.text_date);
                textTime = (TextView) convertView.findViewById(R.id.text_time);
//                textPlayVoiceMessage = (TextView) convertView.findViewById(R.id.text_play_voice_message);
                imgPlayVoiceMessage = (ImageView) convertView.findViewById(R.id.img_play_voice_message);
                reply = (TextView) convertView.findViewById(R.id.text_reply);
                textReplyByAudio = (TextView) convertView.findViewById(R.id.text_reply_audio);
                textReplyByText = (TextView) convertView.findViewById(R.id.text_reply);
                imgDelete = (ImageView) convertView.findViewById(R.id.img_delete);
            }
        }
    }

    private void sendByAudio(final String phoneNumberClickedForReply) {

        final View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_create_voice_msg, null, false);
        view.findViewById(R.id.edt_title).setVisibility(View.GONE);
        final EditText edtMessage = (EditText) view.findViewById(R.id.edt_message);
        view.findViewById(R.id.text_save).setVisibility(View.GONE);
//        edtSave.setText("Send");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
//        builder.setTitle("Voice Message by text");
//        builder.setMessage(message);
        builder.setPositiveButton("Send", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                String message = edtMessage.getText().toString().trim();
                sendVoiceMessage(Utility.getLoggedInUser(getActivity()).iUserID, phoneNumberClickedForReply, Utility.getLoggedInUser(getActivity()).voiceType, message);
            }
        });
        builder.setNegativeButton("Don't send", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertVoice = builder.create();
        alertVoice.show();
    }

    public void loadSms() {

        getLoaderManager().initLoader(1, null, new SMSLoader(getActivity()));

    }

    class SMSLoader implements LoaderManager.LoaderCallbacks<Cursor> {
        Context mContext;

        public SMSLoader(Context context) {
            this.mContext = context;
        }

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            return new CursorLoader(getActivity(), SmsHelper.CONVERSATIONS_CONTENT_PROVIDER, Conversation.ALL_THREADS_PROJECTION, null, null, "date DESC");
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            if (data != null && data.moveToFirst()) {

                progressBarMessage.setVisibility(View.VISIBLE);
                Log.e("SMS==", "===============================");
                ArrayList<Sms> list = new ArrayList<>();
                do {
                    Log.e("SMS==", "==========");
                    /*for (String columnName : data.getColumnNames()) {
                        Log.e("SMS==", columnName + " = " + data.getString(data.getColumnIndex(columnName)));
                    }*/

                    Sms sms = new Sms();
                    sms.smsId = data.getLong(data.getColumnIndex("_id"));

                    sms.threadId = data.getLong(data.getColumnIndex("_id"));
//                        sms.number = data.getString(data.getColumnIndex("number"));
                    sms.message = data.getString(data.getColumnIndex("snippet"));
//                        sms.blackListOrWhiteList = data.getString(data.getColumnIndex("blackListOrWhiteList"));
                    sms.date = data.getLong(data.getColumnIndex("date"));
//                        sms.thumbnailImage = data.getString(data.getColumnIndex("thumbnailImage"));

                    try {
                        String spaceSepIds = data.getString(data.getColumnIndex("recipient_ids"));

                        for (RecipientIdCache.Entry entry : RecipientIdCache.getAddresses(spaceSepIds)) {
                            if (entry != null && !TextUtils.isEmpty(entry.number)) {
                                Contact contact = Contact.get(entry.number, false);
                                contact.setRecipientId(entry.id);
                                sms.name = contact.getName();
                                sms.thumbnailImage = contact.getPhoneUri() + "";
                                contact.getName();
                                break;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.e("SMS_CONTACT", sms.toString());

                    list.add(sms);
                    Log.e("SMS==", "==========");
                } while (data.moveToNext());

                if (isAdded() == false)
                    return;

                progressBarMessage.setVisibility(View.GONE);

                if (list != null && list.size() > 0) {

                    mAdapter.updateItem(list);
                    mListView.setVisibility(View.VISIBLE);

                }

                Log.e("SMS==", "===============================");
            }

        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {

        }
    }

    private final Map<Long, String> mCache = new HashMap<>();

    public List<RecipientIdCache.Entry> getAddresses(String spaceSepIds) {

        List<RecipientIdCache.Entry> numbers = new ArrayList<>();
        String[] ids = spaceSepIds.split(" ");
        for (String id : ids) {
            long longId;

            try {
                longId = Long.parseLong(id);
            } catch (NumberFormatException ex) {
                // skip this id
                continue;
            }

            String number = mCache.get(longId);

            if (number == null) {
                Log.w(TAG, "RecipientId " + longId + " not in cache!");
                fill();
                number = mCache.get(longId);
            }

            if (TextUtils.isEmpty(number)) {
                Log.w(TAG, "RecipientId " + longId + " has empty number!");
            } else {
                numbers.add(new RecipientIdCache.Entry(longId, number));
            }
        }
        return numbers;

    }

    private Uri sAllCanonical = Uri.parse("content://mms-sms/canonical-addresses");

    public void fill() {
        if (LogTag.VERBOSE || Log.isLoggable(LogTag.THREAD_CACHE, Log.VERBOSE)) {
            LogTag.debug("[RecipientIdCache] fill: begin");
        }

        Context context = getActivity();
        Cursor c = SqliteWrapper.query(context, context.getContentResolver(),
                sAllCanonical, null, null, null, null);
        if (c == null) {
            Log.w(TAG, "null Cursor in fill()");
            return;
        }

        try {
            mCache.clear();
            while (c.moveToNext()) {
                // TODO: don't hardcode the column indices
                long id = c.getLong(0);
                String number = c.getString(1);
                mCache.put(id, number);
            }
        } finally {
            c.close();
        }
    }
    public void copyDbToSdcard() {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//"+getActivity().getPackageName()+"//databases//"+"callmeevo.db"+"";
                String backupDBPath = "backupname_viram.db";
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void loadAllSms() {
//        loadSms();
        new AsyncTask<Void, Void, ArrayList<Sms>>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBarMessage.setVisibility(View.VISIBLE);
            }

            @Override
            protected ArrayList<Sms> doInBackground(Void... params) {

                ArrayList<Sms> list = new ArrayList<Sms>();
                String query = "SELECT Contacts.name as name, Sms.threadId, Sms.isRead, Sms.smsId, Sms.message, Sms.number as number, Sms.date as date,Sms.priority as priority," +
                        " Contacts.blackListOrWhiteList as blackListOrWhiteList, Contacts.thumbnailImage as thumbnailImage  FROM Sms LEFT JOIN Contacts ON " +
                        "(Sms.number like '%'||Contacts.number " +
                        "OR " +
                        "Contacts.number like '%'|| Sms.number) " +
                        "GROUP BY Sms.number ORDER BY Contacts.priority DESC,Sms.date DESC, Sms.Id DESC";
                Cursor cursor = ActiveAndroid.getDatabase().rawQuery(query, null);
                if (cursor.moveToFirst()) {
                    do {
                        Sms sms = new Sms();
                        sms.smsId = cursor.getLong(cursor.getColumnIndex("smsId"));
                        sms.name = cursor.getString(cursor.getColumnIndex("name"));
                        sms.isRead = cursor.getInt(cursor.getColumnIndex("isRead"));
                        sms.threadId = cursor.getLong(cursor.getColumnIndex("threadId"));
                        sms.number = cursor.getString(cursor.getColumnIndex("number"));
                        sms.message = cursor.getString(cursor.getColumnIndex("message"));
                        sms.blackListOrWhiteList = cursor.getString(cursor.getColumnIndex("blackListOrWhiteList"));
                        sms.date = cursor.getLong(cursor.getColumnIndex("date"));
                        sms.thumbnailImage = cursor.getString(cursor.getColumnIndex("thumbnailImage"));
                          /*Added by Viram Purohit*/
                        ArrayList<Contacts> item = new Select().from(Contacts.class).
                                where("number='" + sms.number + "'").execute();
                        if (item.size() > 0) {
                            //update
                            sms.priority = item.get(0).priority;
                        }
//                        sms.priority = cursor.getInt(cursor.getColumnIndex("priority"));
                        list.add(sms);

                    } while (cursor.moveToNext());
                }

//                list = new Select().from(Sms.class).orderBy("date DESC").execute();

//                ArrayList<Sms> list = getAllSms();

//                ArrayList<Contacts> list =new ArrayList<Contacts>();
//                ArrayList<Contacts> list = Utility.readContacts(getActivity());

                return list;
            }

            @Override
            protected void onPostExecute(ArrayList<Sms> list) {
                super.onPostExecute(list);

                if (isAdded() == false)
                    return;
                progressBarMessage.setVisibility(View.GONE);

                if (list != null && list.size() > 0) {

                    mAdapter.updateItem(list);
                    mListView.setVisibility(View.VISIBLE);

                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (broadcastHelper != null)
            broadcastHelper.unRegister();

        if (broadcastHelperReadFlag != null)
            broadcastHelperReadFlag.unRegister();
    }

    public ArrayList<Sms> getAllSms() {
        ArrayList<Sms> lstSms = new ArrayList<Sms>();
        Sms objSms = new Sms();

//      Uri message = Uri.parse("content://sms/");
        Uri message = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            message = Telephony.MmsSms.CONTENT_CONVERSATIONS_URI;//Telephony.Sms.CONTENT_URI
        } else {
            message = Uri.parse("content://mms-sms/conversations/"); //content://sms/
        }

        String[] columns = new String[]{Telephony.Sms._ID, Telephony.Sms.PERSON, Telephony.Sms.ADDRESS, Telephony.Sms.BODY, Telephony.Sms.DATE};

        ContentResolver cr = getActivity().getContentResolver();
        Cursor c = cr.query(message, columns, null, null, Telephony.Sms.DATE + " DESC");

        int idPos = c.getColumnIndex(Telephony.Sms._ID);
        int namePos = c.getColumnIndex(Telephony.Sms.PERSON);
        int addressPos = c.getColumnIndex(Telephony.Sms.ADDRESS);
        int bodyPos = c.getColumnIndex(Telephony.Sms.BODY);
        int datePos = c.getColumnIndex(Telephony.Sms.DATE);

//        startManagingCursor(c);
        int totalSMS = c.getCount();

        if (c.moveToFirst()) {

            for (int i = 0; i < totalSMS; i++) {

                objSms = new Sms();
                objSms.smsId = c.getLong(idPos);
                objSms.number = c.getString(addressPos);
                objSms.name = c.getString(namePos);
                objSms.message = c.getString(bodyPos);
                objSms.date = c.getLong(datePos);

                lstSms.add(objSms);
                c.moveToNext();
            }
        }
        // else {
        // throw new RuntimeException("You have no SMS");
        // }
//        c.close();

        return lstSms;
    }

    public String getContactName(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return contactName;
    }

    private static void showLogE(String message) {
        Log.e("ws", message + "");
    }

    private static void showLog(String message) {
        Log.i("ws", message + "");
    }

    public String phoneNumberClickedForReply;

    /**
     * Showing google speech input dialog
     */
    private void promptSpeechInput(String phoneNumber) {
        phoneNumberClickedForReply = phoneNumber;
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Speek something");
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getActivity(), "Speech not supported by this phone", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Receiving speech input
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == Activity.RESULT_OK) {

                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    if (data != null && !TextUtils.isEmpty(phoneNumberClickedForReply) && result != null && result.size() > 0 && !TextUtils.isEmpty(result.get(0))) {

                        final String message = result.get(0);

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle("Voice Message to send");
                        builder.setMessage(message);
                        builder.setPositiveButton("Send", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                sendVoiceMessage(Utility.getLoggedInUser(getActivity()).iUserID, phoneNumberClickedForReply, Utility.getLoggedInUser(getActivity()).voiceType, message);
                            }
                        });
                        builder.setNegativeButton("Don't send", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        AlertDialog alertVoice = builder.create();
                        alertVoice.show();

                    } else {
                        Toast.makeText(getActivity(), "Can't recognize, Try again", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            }

            case REQ_OPEN_DETAIL: {
                if (resultCode == Activity.RESULT_OK) {
                    if (ConstantCodes.ACTION_DELETE.equalsIgnoreCase(data.getStringExtra(ConstantCodes.ACTION))) {
                        int position = data.getIntExtra(ConstantCodes.POSITION, -1);
                        if (position >= 0) {
                            appAdapter2.removeItem(position);
                        }
                    } else {
                        int position = data.getIntExtra(ConstantCodes.POSITION, -1);
                        String helpfull = data.getStringExtra(ConstantCodes.HELPFULL);
                        if (position >= 0) {
                            appAdapter2.update(position, helpfull);
                        }
                    }

                }
                break;
            }

        }
    }

    private void sendVoiceMessage(final String userId, final String incommingNumber, final String voiceType, final String message) {
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, ConstantCodes.Web.SEND_VOICE_MAIL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.LOGIN);
                showLog("RESPONSE : " + response.toString());
                showLog("**********");

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has(ConstantCodes.STATUS) && jsonObject.getString(ConstantCodes.STATUS).equalsIgnoreCase(ConstantCodes.SUCCESS)) {

                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.LOGIN);
                showLogE("RESPONSE : " + (error.networkResponse == null ? (error != null ? error.getLocalizedMessage() : "NullVolleyError") : new String(error.networkResponse.data)));
                showLog("**********");

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = Utility.getDefaultParams();
                params.put("phone_number", incommingNumber);
                params.put("eType", voiceType);
                params.put("tScript", message);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = Utility.getDefaultHeaders();
                headers.put(ConstantCodes.iUserID, userId);
                return headers;
                /*Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token 04af7328b2346602ad7d581ff738eb25a5482cbf");
                params.put("Apikey", "JXn8e6C29jhZ065i8wyQktY33YD3s9zy");
                return params;*/
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        Utility.getRequestQueue().add(jsonObjReq);
    }

    public void removeVoiceMessage() {

    }
}
