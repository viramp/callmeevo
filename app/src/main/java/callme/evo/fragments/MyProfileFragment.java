package callme.evo.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.imagepicker.activity.ImagePicker;
import com.imagepicker.activity.ImagePickerActivity;
import com.imagepicker.model.Image;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import callme.evo.R;
import callme.evo.Volley;
import callme.evo.VolleyNetworkRequest;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.User;
import customtextviews.MyTextViewBook;
import customtextviews.MyTextViewMedium;


public class MyProfileFragment extends Fragment {

    private static final int REQUEST_CODE_PICKER = 211;

    public MyProfileFragment() {
        // Required empty public constructor
    }

    MyTextViewBook tagline;
    MyTextViewMedium submit;
    TextView textName, txtNumber;
    EditText edtName, edtPassword;
    ImageView imgProfile;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_seven, container, false);

        final User user = Utility.getLoggedInUser(getActivity());
        TextView textName = (TextView) view.findViewById(R.id.text_name);
        imgProfile = (ImageView) view.findViewById(R.id.img_profile);
        edtName = (EditText) view.findViewById(R.id.edt_name);
        edtPassword = (EditText) view.findViewById(R.id.edt_password);
        txtNumber = (TextView) view.findViewById(R.id.txt_number);
        TextView textSave = (TextView) view.findViewById(R.id.text_save);
        if (user != null) {
            textName.setText(user.vUserName);
            edtName.setText(user.vUserName);
            Utility.showCircularImageView(getActivity(), imgProfile, user.vProfilePic);

            txtNumber.setText(user.vPhone);
        }
        tagline = (MyTextViewBook) view.findViewById(R.id.tagline);
        tagline.setPaintFlags(tagline.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        view.findViewById(R.id.img_upload).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagePicker.create(MyProfileFragment.this)
                        .folderMode(true) // folder mode (false by default)
                        .folderTitle("Folder") // folder selection title
                        .imageTitle("Tap to select") // image selection title
                        .single() // single mode
//                        .multi() // multi mode (default mode)
//                        .limit(10) // max images can be selected (99 by default)
                        .showCamera(true) // show camera or not (true by default)
                        .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
//                        .origin(images) // original selected images, used in multi mode
                        .start(REQUEST_CODE_PICKER);
            }
        });

        textSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveProfile(user.iUserID);
            }
        });

        submit = (MyTextViewMedium) view.findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                //  builder.setTitle("Confirm");
                builder.setMessage(Html.fromHtml("<b>Congratulations!</b><br/><br/>" + "Your seven days trial has been activated."));


                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        return view;


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PICKER && resultCode == AppCompatActivity.RESULT_OK && data != null) {


            ArrayList<Image> images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);
            // do your logic ....
            if (images != null && images.size() > 0) {
                String selectedImagePath = images.get(0).getPath();
                File file = new File(selectedImagePath);
                Uri imageUri = Uri.fromFile(file);

                Toast.makeText(getActivity(), "Image Selected is " + selectedImagePath, Toast.LENGTH_SHORT).show();
                Utility.showCircularImageView(getActivity(), imgProfile, imageUri);

                User user = Utility.getLoggedInUser(getActivity());
                HashMap<String, String> mHeaderParams = Utility.getDefaultHeaders();
                mHeaderParams.put(ConstantCodes.iUserID, user.iUserID);


                //Add File Params
                HashMap<String, File> mFileParams = new HashMap<>();
                mFileParams.put("vProfilePic", new File(selectedImagePath));

                VolleyNetworkRequest mVolleyNetworkRequest = new VolleyNetworkRequest(ConstantCodes.Web.UPDATE_PROFILE
                        , new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), "Error." + error, Toast.LENGTH_SHORT).show();
                    }
                }
                        , new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.toString());
                            User user = (User) Utility.parseFromString(jsonObject.optString(ConstantCodes.DATA), User.class);
                            Utility.saveLoggedInUser(getActivity(), user);
                            Utility.showCircularImageView(getActivity(), imgProfile, user.vProfilePic);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
                        , mHeaderParams
                        , null
                        , mFileParams);
                Volley.getInstance(getActivity()).addToRequestQueue(mVolleyNetworkRequest);
            }
        }
    }

    ProgressDialog progressBar;

    private void saveProfile(final String userId) {
        final String username = edtName.getText().toString().trim();
        final String password = edtPassword.getText().toString().trim();
        progressBar = Utility.progressDialog(getActivity(), "Please wait...");
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, ConstantCodes.Web.UPDATE_PROFILE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressBar.dismiss();
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.LOGIN);
                showLog("RESPONSE : " + response.toString());
                showLog("**********");

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has(ConstantCodes.STATUS) && jsonObject.getString(ConstantCodes.STATUS).equalsIgnoreCase(ConstantCodes.SUCCESS)) {
                        Toast.makeText(getActivity(), "Profile updated successfully", Toast.LENGTH_SHORT).show();
                    } else {
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.dismiss();
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.LOGIN);
                showLogE("RESPONSE : " + (error.networkResponse == null ? (error != null ? error.getLocalizedMessage() : "NullVolleyError") : new String(error.networkResponse.data)));
                showLog("**********");

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = Utility.getDefaultParams();
                params.put("vUserName", username);
                params.put("vPassword", password);
//                params.put("vTagline", message);
//                params.put("vProfilePic", message);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = Utility.getDefaultHeaders();
                headers.put(ConstantCodes.iUserID, userId);
                return headers;
                /*Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token 04af7328b2346602ad7d581ff738eb25a5482cbf");
                params.put("Apikey", "JXn8e6C29jhZ065i8wyQktY33YD3s9zy");
                return params;*/
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        Utility.getRequestQueue().add(jsonObjReq);
    }

    private static void showLogE(String message) {
        Log.e("ws", message + "");
    }

    private static void showLog(String message) {
        Log.i("ws", message + "");
    }
}
