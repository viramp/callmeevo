package callme.evo.fragments;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.andraskindler.quickscroll.QuickScroll;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import callme.evo.R;
import callme.evo.SyncContactService;
import callme.evo.activity.WhiteListContactDetail;
import callme.evo.adapter.AppAdapter;
import callme.evo.adapter.IndexAdapter;
import callme.evo.global.BroadcastHelper;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.Contacts;


public class ContactsFragment extends Fragment {

    int ADD_CONTACT = 100;
    private int CONTACT_DETAIS = 101;

    public ContactsFragment() {
        // Required empty public constructor
    }

    private AppAdapter mAdapter;
    private SwipeMenuListView mListView;
    private TextView txtError;
    private ProgressBar progressBar;
    private BroadcastHelper broadcastHelper;
    private View view;

//    private GestureDetector mGestureDetector;
    private List<String> alphabet = new ArrayList<String>();
    private HashMap<String, Integer> sections = new HashMap<String, Integer>();
//    private int sideIndexHeight;
//    private static float sideIndexX;
//    private static float sideIndexY;
    private int indexListSize;


    IndexAdapter indexAdapter;
    ListView sideIndex;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view != null && mAdapter != null && mListView != null && mAdapter.getCount() > 0) {
            mListView.setAdapter(mAdapter);
            return view;
        }
        view = inflater.inflate(R.layout.fragment_five, container, false);

        view.findViewById(R.id.purchase_screen).setVisibility(View.GONE);

        broadcastHelper = new BroadcastHelper(getActivity(), ConstantCodes.BROADCAST_CONTACT_REFRESH);
        broadcastHelper.register(new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, Intent intent) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Glide.get(context).clearDiskCache();
                    }
                }).start();

                readAllContacts();
            }
        });

        sideIndex = (ListView)view.findViewById(R.id.layout_alphabats);
        sideIndex.setFastScrollEnabled(true);

        indexAdapter = new IndexAdapter(getActivity());
        sideIndex.setAdapter(indexAdapter);


        mListView = (SwipeMenuListView) view.findViewById(R.id.listView);


        txtError = (TextView) view.findViewById(R.id.txt_error);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        mAdapter = new AppAdapter(getActivity());
        mListView.setAdapter(mAdapter);

        final QuickScroll fastTrack = QuickScroll.class.cast(view.findViewById(R.id.quickscroll));
        fastTrack.init(QuickScroll.TYPE_POPUP, mListView, mAdapter, QuickScroll.STYLE_NONE);
        fastTrack.setFixedSize(2);
        fastTrack.setPopupColor(ContextCompat.getColor(getActivity(), R.color.dark_grey), QuickScroll.BLUE_LIGHT_SEMITRANSPARENT, 1, Color.WHITE, 1);
        fastTrack.setVisibility(View.VISIBLE);

//        createAlphabetTrack();

//        final QuickScroll quickscroll = (QuickScroll) view.findViewById(R.id.quickscroll);
//        quickscroll.init(QuickScroll.TYPE_INDICATOR, mListView, mAdapter, QuickScroll.STYLE_HOLO);
//        quickscroll.setVisibility(View.VISIBLE);

//        mListView.setFastScrollEnabled(true);

        //Reading all the contacts and loading in listview.
        readAllContacts();

        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getActivity());
                // set item background
                openItem.setBackground(R.color.red);
                // set item width
                openItem.setWidth(dp2px(90));
                // set a icon
                openItem.setIcon(R.drawable.slide_delete);
                // add to menu
                menu.addMenuItem(openItem);

            }
        };
        // set creator
        mListView.setMenuCreator(creator);

        mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        // delete
//                        Toast.makeText(getActivity(), "Delete Item", Toast.LENGTH_SHORT).show();
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                        //  builder.setTitle("Confirm");
                        builder.setMessage("Are you sure you want to delete this contact?");

                        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                Contacts item = mAdapter.getItem(position);
                                Utility.delete(getActivity(), item.contactId);
//                                Toast.makeText(getActivity(), "Delete Item", Toast.LENGTH_SHORT).show();
                                new Delete().from(Contacts.class).where("contactId like '" + item.contactId + "'").execute();
                                mAdapter.removeItem(position);
                                dialog.dismiss();
                            }
                        });

                        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                // Do nothing
                                dialog.dismiss();
                            }
                        });

                        AlertDialog alert = builder.create();
                        alert.show();

                        break;
                    case 1:
                        // unblock
                        Toast.makeText(getActivity(), "Unblock Item", Toast.LENGTH_SHORT).show();
                        break;
                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Contacts contacts = mAdapter.getItem(position);
                Intent intent = new Intent(getActivity(), WhiteListContactDetail.class);
                intent.putExtra(ConstantCodes.INTENT_NAME, contacts.name);
                intent.putExtra(ConstantCodes.INTENT_NUMBER, contacts.number);
                intent.putExtra(ConstantCodes.INTENT_PHOTO, contacts.thumbnailImage);

                if (TextUtils.isEmpty(contacts.blackListOrWhiteList)) {
                    intent.putExtra(ConstantCodes.INTENT_FROM, ConstantCodes.CONTACT);
                } else {
                    intent.putExtra(ConstantCodes.INTENT_FROM, ConstantCodes.BLACK_LIST.equalsIgnoreCase(contacts.blackListOrWhiteList) ? ConstantCodes.BLACK_LIST : ConstantCodes.WHITE_LIST);
                }

                startActivityForResult(intent,CONTACT_DETAIS);
            }
        });

        //fab click listener
        ImageView action = (ImageView) view.findViewById(R.id.action);
        action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_INSERT);
                intent.setType(ContactsContract.Contacts.CONTENT_TYPE);

// Just two examples of information you can send to pre-fill out data for the
// user.  See android.provider.ContactsContract.Intents.Insert for the complete
// list.
//                intent.putExtra(ContactsContract.Intents.Insert.NAME, "some Contact Name");
//                intent.putExtra(ContactsContract.Intents.Insert.PHONE, "some Phone Number");

// Send with it a unique request code, so when you get called back, you can
// check to make sure it is from the intent you launched (ideally should be
// some public static final so receiver can check against it)

                startActivityForResult(intent, ADD_CONTACT);
            }
        });

        return view;
    }

//    private ViewGroup createAlphabetTrack() {
//
////        final LinearLayout layout = new LinearLayout(getActivity());
//        final LinearLayout layout = (LinearLayout) view.findViewById(R.id.layout_alphabats);
//        layout.setVisibility(View.VISIBLE);
//        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int) (30 * getResources().getDisplayMetrics().density), RelativeLayout.LayoutParams.MATCH_PARENT);
//        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//        layout.setLayoutParams(params);
//        layout.setOrientation(LinearLayout.VERTICAL);
//
//        final LinearLayout.LayoutParams textparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT,1);
////        textparams.weight = 1;
//        final int height = getResources().getDisplayMetrics().heightPixels;
//        int iterate = 0;
//        if (height >= 1024) {
//            iterate = 1;
//            layout.setWeightSum(26);
//        } else {
//            iterate = 2;
//            layout.setWeightSum(13);
//        }
//        for (char character = 'A'; character <= 'Z'; character += iterate) {
//            final TextView textview = new TextView(getActivity());
//            textview.setLayoutParams(textparams);
//            textview.setGravity(Gravity.CENTER);
//            textview.setTextSize(15);
//            textview.setText(Character.toString(character));
//            layout.addView(textview);
//        }
//
//        return layout;
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_CONTACT) {
            Intent intent = new Intent(getActivity(), SyncContactService.class);
            getActivity().startService(intent);
        }else if (requestCode == CONTACT_DETAIS) {
            readAllContacts();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (broadcastHelper != null) {
            broadcastHelper.unRegister();
        }
    }

    private void readAllContacts() {
        new AsyncTask<Void, Void, ArrayList<Contacts>>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected ArrayList<Contacts> doInBackground(Void... params) {

                Log.i("CONTACT", "READING CONTACT");
                ArrayList<Contacts> list = new Select().from(Contacts.class).where("name<>''").orderBy("priority DESC, name COLLATE NOCASE ASC").execute();
                Log.i("CONTACT", "READING CONTACT size" + list.size());

                /*for (Contacts contacts : list) {
                    Log.d("CONTACT", "Contact : " + contacts.name);
                }*/
//                ArrayList<Contacts> list =new ArrayList<Contacts>();
//                ArrayList<Contacts> list = Utility.readContacts(getActivity());

                return list;
            }

            @Override
            protected void onPostExecute(ArrayList<Contacts> list) {
                super.onPostExecute(list);

                progressBar.setVisibility(View.GONE);
                if (isAdded() == false)
                    return;
                Log.i("CONTACT", "READING CONTACT size" + list.size());


                if (list != null && list.size() > 0) {

                    mAdapter.updateItem(list);

                    mListView.setVisibility(View.VISIBLE);

                    txtError.setVisibility(View.GONE);

                    bindIndex(list);

                } else {

                    txtError.setText("There are no numbers on your Contacts");

                    txtError.setVisibility(View.VISIBLE);

                    mListView.setVisibility(View.GONE);

                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    /*NEw added */
    private void bindIndex(ArrayList<Contacts> list){
//        List<Row> rows = new ArrayList<Row>();
        int start = 0;
        int end = 0;
        String previousLetter = null;
        Pattern numberPattern = Pattern.compile("[0-9]");
        Pattern specialPattern = Pattern.compile("^[a-zA-Z0-9]*$");
        sections = new HashMap<>();
        alphabet = new ArrayList<>();
        alphabet.clear();
        for (int i =0; i < list.size(); i++) {
            String firstLetter = list.get(i).name.substring(0, 1);

            // Group numbers together in the scroller
            if (!specialPattern.matcher(firstLetter).matches()) {
                Log.e("specialPattern","specialPattern"+firstLetter);
                firstLetter = "..";
            }
            if (numberPattern.matcher(firstLetter).matches()) {
                firstLetter = "#";
            }


            // If we've changed to a new letter, add the previous letter to the alphabet scroller
            if (previousLetter != null && !firstLetter.equalsIgnoreCase(previousLetter)) {
//                end = rows.size() - 1;
                String tmpIndexItem = previousLetter.toUpperCase(Locale.UK);
                alphabet.add(tmpIndexItem);

                start = end + 1;
            }

            // Check if we need to add a header row
            if (!firstLetter.equalsIgnoreCase(previousLetter)) {
//                rows.add(new Section(firstLetter));
                sections.put(firstLetter.toString().toUpperCase(), i);
            }

            // Add the country to the list
//            rows.add(new Item(country));
            previousLetter = firstLetter;
        }

        if (previousLetter != null) {
            // Save the last letter
            String tmpIndexItem = previousLetter.toUpperCase(Locale.UK);
            alphabet.add(tmpIndexItem);
        }

        HashSet<String> objects = new HashSet<>();
        objects.addAll(alphabet);
        alphabet = new ArrayList<>();
        alphabet.clear();
        alphabet.addAll(objects);
        Collections.sort(alphabet);


        indexAdapter.updateItem((ArrayList<String>) alphabet);
        sections = sortByValues(sections);

        updateList();
    }
    public void updateList() {
        sideIndex.setVisibility(View.VISIBLE);

        sideIndex.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                displayListItem(position);
            }
        });


//        sideIndex.removeAllViews();
        indexListSize = alphabet.size();
        if (indexListSize < 1) {
            return;
        }
    }

    public void displayListItem(int position) {

        sideIndex.setVisibility(View.VISIBLE);
//        sideIndexHeight = sideIndex.getHeight();

        String indexItem = alphabet.get(position);
        int subitemPosition = sections.get(indexItem);

        mListView.setSelection(subitemPosition);
    }
    private static HashMap sortByValues(HashMap map) {
        List list = new LinkedList(map.entrySet());
        // Defined Custom Comparator here
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o1)).getValue())
                        .compareTo(((Map.Entry) (o2)).getValue());
            }
        });

        // Here I am copying the sorted list in HashMap
        // using LinkedHashMap to preserve the insertion order
        HashMap sortedHashMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedHashMap.put(entry.getKey(), entry.getValue());
        }
        return sortedHashMap;
    }
}