package callme.evo.fragments;

import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import callme.evo.R;
import callme.evo.global.BroadcastHelper;
import callme.evo.global.ConstantCodes;
import callme.evo.models.BluetoothModel;
import callme.evo.models.ContactCallLog;
import callme.evo.models.Contacts;
import callme.evo.services.BluetoothService;


public class BluetoothFragment extends Fragment {

    private BluetoothAdapter mAdapter;
    private SwipeMenuListView mListView;
    private TextView txtError;
    private ProgressBar progressBar;
    private BroadcastHelper broadcastHelper;
    android.bluetooth.BluetoothAdapter mBluetoothAdapter;
    public BluetoothFragment() {

    }
    
    View view;

    //    9429801969
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        if (view != null && mListView != null && mAdapter != null) {
            mListView.setAdapter(mAdapter);
            return view;
        }
        view = inflater.inflate(R.layout.fragment_bluetooth, container, false);

        txtError = (TextView) view.findViewById(R.id.txt_error);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        broadcastHelper = new BroadcastHelper(getActivity(), ConstantCodes.BROADCAST_BLUETOOTH_REFRESH);
        broadcastHelper.register(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                loadList(false);
            }
        });

//        checkBTStatus();

        mListView = (SwipeMenuListView) view.findViewById(R.id.listView);

        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getActivity());
                // set item background
                openItem.setBackground(R.color.red);
                // set item width
                openItem.setWidth(dp2px(90));
                // set a icon
                openItem.setIcon(R.drawable.slide_delete);
                // add to menu
                menu.addMenuItem(openItem);
            }
        };


        loadList(true);

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        broadcastHelper.unRegister();
    }

    ArrayList<BluetoothModel> list;

    private void loadList(final boolean showProgress) {

        new AsyncTask<Void, Void, ArrayList<ContactCallLog>>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if (showProgress)
                    progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected ArrayList<ContactCallLog> doInBackground(Void... params) {

                list = getCallDetailsNew();
                return null;
            }

            @Override
            protected void onPostExecute(ArrayList<ContactCallLog> contactCallLogs) {
                super.onPostExecute(contactCallLogs);
                progressBar.setVisibility(View.GONE);

                if (list != null && list.size() > 0) {

                    mAdapter = new BluetoothAdapter(getActivity(), list);

                    mListView.setAdapter(mAdapter);

                    mListView.setVisibility(View.VISIBLE);

                    txtError.setVisibility(View.GONE);

                } else {

                    txtError.setText("There are no bluetooth connections");

                    txtError.setVisibility(View.VISIBLE);

                    mListView.setVisibility(View.GONE);

                }
            }
        }.execute();
    }

    private ArrayList<BluetoothModel> getCallDetailsNew() {
        return new Select().from(BluetoothModel.class).execute();
    }


    private ArrayList<Contacts> mergeAndSort(ArrayList<Contacts> list) {

        Collections.sort(list, new Comparator<Contacts>() {
            @Override
            public int compare(Contacts lhs, Contacts rhs) {
                if (lhs.priority > rhs.priority) return -1;
                if (lhs.priority < rhs.priority) return 1;
                else return 0;
            }
        });

        return list;
    }


    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    public class BluetoothAdapter extends BaseAdapter {

        ArrayList<BluetoothModel> list;
        Context context;

        public BluetoothAdapter(Context context) {
            this.context = context;
            this.list = new ArrayList<BluetoothModel>();
        }

        public BluetoothAdapter(Context context, ArrayList<BluetoothModel> list) {
            this.list = list;
            this.context = context;
        }

        public void updateItem(ArrayList<BluetoothModel> list) {
            this.list = list;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public BluetoothModel getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {


            ViewHolder holder;
            if (convertView == null) {
                convertView = View.inflate(context, R.layout.list_item_bluetooth, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            BluetoothModel item = getItem(position);
            holder.txtName.setText(item.name);
            holder.checkBox.setChecked(item.isDefault == 1);
            holder.checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CheckBox checkBox = (CheckBox) v;
                    if (checkBox.isChecked()) {
                        Toast.makeText(context, "Checked true", Toast.LENGTH_SHORT).show();
                        item.isDefault = 1;
                        mBluetoothAdapter = android.bluetooth.BluetoothAdapter.getDefaultAdapter();
                        if (mBluetoothAdapter != null) {
                            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(item.address);
                            try {
//                                Log.e("TAG" ," Calling.......... onStartCommand createBond**********-" +createBond(device));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    } else {
                        item.isDefault = 0;
                    }
                    item.update(new String[]{"isDefault"});
                }
            });

            return convertView;
        }

        class ViewHolder {
            public CheckBox checkBox;
            public TextView txtName;

            public ViewHolder(View view) {
                checkBox = (CheckBox) view.findViewById(R.id.checkbox);
                txtName = (TextView) view.findViewById(R.id.txt_name);
            }
        }
    }
    public boolean createBond(BluetoothDevice btDevice)
            throws Exception
    {

        Class class1 = Class.forName("android.bluetooth.BluetoothDevice");
        Method createBondMethod = class1.getMethod("createBond");
        Boolean returnValue = (Boolean) createBondMethod.invoke(btDevice);
        return returnValue.booleanValue();
    }
    /*Check for blue tooth*/
    protected void checkBTStatus(){
         mBluetoothAdapter = android.bluetooth.BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            alertBTNoSupport();
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                alertBTEnable();
            }else{
                getActivity().startService(new Intent(getActivity(), BluetoothService.class));
            }
        }
    }

    private void alertBTNoSupport(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        //  builder.setTitle("Confirm");
        builder.setMessage("Sorry,Bluetooth not support with device!");


        builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
// Bluetooth is not enable :)
                mBluetoothAdapter.enable();
                dialog.dismiss();
            }
        });



        AlertDialog alert = builder.create();
        alert.show();
    }


    private void alertBTEnable(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        //  builder.setTitle("Confirm");
        builder.setMessage("Do you want to enable Bluetooth?");


        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
// Bluetooth is not enable :)
                getActivity().startService(new Intent(getActivity(), BluetoothService.class));
                mBluetoothAdapter.enable();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }
}