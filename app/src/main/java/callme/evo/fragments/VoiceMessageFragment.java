package callme.evo.fragments;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.github.siyamed.shapeimageview.CircularImageView;

import java.util.ArrayList;
import java.util.UUID;

import callme.evo.R;
import callme.evo.activity.HomeScreen;
import callme.evo.activity.VoiceMailDetailActivity;
import callme.evo.global.BroadcastHelper;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.VoiceMessageModel;


public class VoiceMessageFragment extends Fragment {

    public VoiceMessageFragment() {
        // Required empty public constructor
    }

    private CustomAdapter mAdapter;
    private SwipeMenuListView mListView;
    private TextView txtError;
    private ProgressBar progressBar;
    private BroadcastHelper broadcastHelper, broadcastHelperPayment;
    private FrameLayout purchaseScreen;
    View view;
    private ImageView imgAction;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view != null && mAdapter != null && mListView != null && mAdapter.getCount() > 0) {
            mListView.setAdapter(mAdapter);
            return view;
        }
        view = inflater.inflate(R.layout.fragment_five, container, false);

        broadcastHelper = new BroadcastHelper(getActivity(), ConstantCodes.BROADCAST_CONTACT_REFRESH);
        broadcastHelper.register(new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, Intent intent) {
                loadAllVoiceMails();
            }
        });

        broadcastHelperPayment = new BroadcastHelper(getActivity(), ConstantCodes.BROADCAST_PAYMENT_REFRESH);
        broadcastHelperPayment.register(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (purchaseScreen != null && isAdded()) {
                    if (Utility.isPaidApplication(getActivity())) {
                        purchaseScreen.setVisibility(View.GONE);
                    } else {
                        purchaseScreen.setVisibility(View.VISIBLE);
                    }

                }
            }
        });

        imgAction = (ImageView) view.findViewById(R.id.action);

        mListView = (SwipeMenuListView) view.findViewById(R.id.listView);

        mListView.setPadding(0,0,0,0);

        txtError = (TextView) view.findViewById(R.id.txt_error);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        purchaseScreen = (FrameLayout) view.findViewById(R.id.purchase_screen);

        purchaseScreen.findViewById(R.id.yes_upgrade).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getActivity() instanceof HomeScreen) {
                    ((HomeScreen) getActivity()).upgradeAccountUsingPaypal();
                }
            }
        });

        if (Utility.isPaidApplication(getActivity())) {
            purchaseScreen.setVisibility(View.GONE);
        } else {
            purchaseScreen.setVisibility(View.VISIBLE);
        }

        mAdapter = new CustomAdapter(getActivity());
        mListView.setAdapter(mAdapter);

        //Reading all the contacts and loading in listview.
        loadAllVoiceMails();

        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getActivity());
                // set item background
                openItem.setBackground(R.color.red);
                // set item width
                openItem.setWidth(dp2px(90));
                // set a icon
                openItem.setIcon(R.drawable.slide_delete);
                // add to menu
                menu.addMenuItem(openItem);

            }
        };
        // set creator
        mListView.setMenuCreator(creator);
        // Left
        mListView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
   /*@author added by Viram Purohit*/
        mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        // delete
//                        Toast.makeText(getActivity(), "Delete Item", Toast.LENGTH_SHORT).show();
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                        //  builder.setTitle("Confirm");
                        builder.setMessage("Are you sure you want to delete this voice message group?");

                        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                VoiceMessageModel item = mAdapter.getItem(position);
//                                Toast.makeText(getActivity(), "Delete Item", Toast.LENGTH_SHORT).show();
                                new Delete().from(VoiceMessageModel.class).where("voiceMessageId like '" + item.voiceMessageId + "'").execute();
                                mAdapter.removeItem(position);
                                dialog.dismiss();
                            }
                        });

                        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Do nothing
                                dialog.dismiss();
                            }
                        });

                        AlertDialog alert = builder.create();
                        alert.show();

                        break;
                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), VoiceMailDetailActivity.class);
                VoiceMessageModel model = mAdapter.getItem(position);
                intent.putExtra(ConstantCodes.INTENT_ID, model.voiceMessageId);
                intent.putExtra(ConstantCodes.INTENT_TITLE, model.title);
                intent.putExtra(ConstantCodes.INTENT_MESSAGE, model.message);
                startActivity(intent);
            }
        });
        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (broadcastHelper != null) {
            broadcastHelper.unRegister();
        }
        if (broadcastHelperPayment != null) {
            broadcastHelperPayment.unRegister();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        imgAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showAddEditMessageDialog();

            }
        });

    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible) {
            if (purchaseScreen != null) {
                if (Utility.isPaidApplication(getActivity())) {
                    purchaseScreen.setVisibility(View.GONE);
                } else {
                    purchaseScreen.setVisibility(View.VISIBLE);
                }

            }
        }
    }

    private void showAddEditMessageDialog() {
        final View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_create_voice_msg, null, false);
        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(view);
        final android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        view.findViewById(R.id.text_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                VoiceMessageModel voiceMessageModel = new VoiceMessageModel();
                voiceMessageModel.voiceMessageId = UUID.randomUUID().toString();
                voiceMessageModel.title = ((EditText) view.findViewById(R.id.edt_title)).getText().toString();
                voiceMessageModel.message = ((EditText) view.findViewById(R.id.edt_message)).getText().toString();
                voiceMessageModel.save();
                alertDialog.dismiss();
                loadAllVoiceMails();
            }
        });
    }

    private void loadAllVoiceMails() {
        new AsyncTask<Void, Void, ArrayList<VoiceMessageModel>>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected ArrayList<VoiceMessageModel> doInBackground(Void... params) {

                Log.i("CONTACT", "READING VOICE MESSAGE");

                ArrayList<VoiceMessageModel> list = new Select().from(VoiceMessageModel.class).execute();//.where("title<>'" + ConstantCodes.FREE_VOICE_MAIL_TITLE + "'").execute();
                Log.i("CONTACT", "READING VOICE MESSAGE size" + list.size());

                /*for (VoiceMessageModel contacts : list) {
                    Log.d("CONTACT", "Contact : " + contacts.name);
                }*/
//                ArrayList<Contacts> list =new ArrayList<Contacts>();
//                ArrayList<Contacts> list = Utility.readContacts(getActivity());

                return list;
            }

            @Override
            protected void onPostExecute(ArrayList<VoiceMessageModel> list) {
                super.onPostExecute(list);

                progressBar.setVisibility(View.GONE);
                if (isAdded() == false)
                    return;
                Log.i("CONTACT", "READING CONTACT size" + list.size());

                if (list != null && list.size() > 0) {

                    mAdapter.updateItem(list);

                    mListView.setVisibility(View.VISIBLE);

                    txtError.setVisibility(View.GONE);

                } else {

                    txtError.setText("There are no numbers on your VoiceMessage Group");

                    txtError.setVisibility(View.VISIBLE);

                    mListView.setVisibility(View.GONE);

                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    public class CustomAdapter extends BaseAdapter {

        Context context;
        private ArrayList<VoiceMessageModel> list;

        public CustomAdapter(Context context) {
            this.context = context;
            this.list = new ArrayList<>();
        }

        public CustomAdapter(Context context, ArrayList<VoiceMessageModel> list) {

            this.context = context;
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        public void updateItem(ArrayList<VoiceMessageModel> list) {
            this.list = list;
            notifyDataSetChanged();
        }

        public void removeItem(int position) {
            if (list.size() > position) {
                list.remove(position);
                notifyDataSetInvalidated();
            }
        }

        @Override
        public VoiceMessageModel getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder;
            if (convertView == null) {
                convertView = View.inflate(context, R.layout.row_voice_message, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.txtName.setText(getItem(position).title);
            holder.txtNumber.setText(getItem(position).message);
            if (ConstantCodes.FREE_VOICE_MAIL_TITLE.equalsIgnoreCase(getItem(position).voiceMessageId) || ConstantCodes.FREE_VOICE_MAIL_TITLE_FOR_DRIVING.equalsIgnoreCase(getItem(position).voiceMessageId)) {
//                holder.txtName.setText(ConstantCodes.DEFAULT_VOICEMESSAGE);
                holder.txtName.setTextColor(ContextCompat.getColor(context, R.color.blue_dark));
            } else {
//                holder.txtName.setText(getItem(position).title);
                holder.txtName.setTextColor(ContextCompat.getColor(context, R.color.dark_grey));
            }
            holder.txtName.setText(getItem(position).title);

            return convertView;
        }

        class ViewHolder {
            public CircularImageView imgProfile;
            public TextView txtName, txtNumber;

            public ViewHolder(View view) {
                imgProfile = (CircularImageView) view.findViewById(R.id.profileImage);
                txtName = (TextView) view.findViewById(R.id.txt_name);
                txtNumber = (TextView) view.findViewById(R.id.txt_number);
            }
        }
    }
}