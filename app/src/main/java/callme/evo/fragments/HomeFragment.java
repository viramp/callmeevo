package callme.evo.fragments;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Calendar;

import callme.evo.R;
import callme.evo.adapter.CallGroupedAdapter;
import callme.evo.global.BroadcastHelper;
import callme.evo.global.ConstantCodes;
import callme.evo.global.DateHelper;
import callme.evo.models.Contacts;
import callme.evo.services.BluetoothService;


public class HomeFragment extends Fragment {

    public HomeFragment() {
        // Required empty public constructor
    }

    private TextView txtCounterBlockedToday;
    private CallGroupedAdapter mAdapter;
    private SwipeMenuListView mListView;
    private ProgressBar progressBar;
    private BroadcastHelper broadcastHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_one, container, false);

        mListView = (SwipeMenuListView) view.findViewById(R.id.listView);

        broadcastHelper = new BroadcastHelper(getActivity(), ConstantCodes.BROADCAST_BLOCKED);
        broadcastHelper.register(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                readBlockedHistory();

            }
        });

        txtCounterBlockedToday = (TextView) view.findViewById(R.id.txt_counter_blocked_today);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        mAdapter = new CallGroupedAdapter(getActivity());
        mListView.setAdapter(mAdapter);

        view.findViewById(R.id.fcc).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:1-888-382-1222"));
                startActivity(intent);
                getActivity().overridePendingTransition(0, 0);
            }
        });

        readBlockedHistory();

        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getActivity());
                // set item background
                openItem.setBackground(R.color.red);
                // set item width
                openItem.setWidth(dp2px(90));
                // set a icon
                openItem.setIcon(R.drawable.slide_delete);
                // add to menu
                menu.addMenuItem(openItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getActivity());
                // set item background
                deleteItem.setBackground(R.color.green);
                // set item width
                deleteItem.setWidth(dp2px(90));
                // set a icon
                deleteItem.setIcon(R.drawable.slide_unblock);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        // set creator
        mListView.setMenuCreator(creator);
        // Left
        mListView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
   /*@author added by Viram Purohit*/
        mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        // delete
                        Contacts contacts = mAdapter.getItem(position);
//                        Toast.makeText(getActivity(), "Delete Item", Toast.LENGTH_SHORT).show();
                        ActiveAndroid.getDatabase().execSQL("DELETE FROM CallsBlocked WHERE " +
                                "number LIKE '%" + contacts.number + "' " +
                                "OR " +
                                "'" + contacts.number + "' LIKE '%'||number " +
                                "");
                        mAdapter.removeItem(position);

                        break;
                    case 1:
                        // unblock
                        Toast.makeText(getActivity(), "Unblock Item", Toast.LENGTH_SHORT).show();
                        break;
                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });

        /*@author Viram Purohit*/
        startBT();
        return view;
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (broadcastHelper != null) {
            broadcastHelper.unRegister();
        }
    }

    private void readBlockedHistory() {
        new AsyncTask<Void, Void, ArrayList<Contacts>>() {
            int totalSize=0;
            @Override
            protected ArrayList<Contacts> doInBackground(Void... params) {


                exportDatabse("callmeevo.db");
                Log.i("CONTACT", "READING BLOCKED HISTORY ");

                ArrayList<Contacts> list = new ArrayList<Contacts>();

//                String query = "SELECT Contacts.name as name," +
//                        " CallsBlocked.number as number, CallsBlocked.timestamp as timestamp," +
//                        " Contacts.blackListOrWhiteList as blackListOrWhiteList " +
//                        "FROM CallsBlocked " +
//                        "LEFT JOIN Contacts" +
//                        " ON ( " +
//                        "CallsBlocked.blackListOrWhiteList like '%'||Contacts.blackListOrWhiteList " +
//                        "OR " +
//                        "Contacts.blackListOrWhiteList like '%'||CallsBlocked.blackListOrWhiteList " +
//                        ") " +
////                        AND Contacts.blackListOrWhiteList='" + ConstantCodes.BLACK_LIST + "' " +
//                        "ORDER BY timestamp DESC";

                /*Added by Viram Purohit*/
                String query = "SELECT Contacts.name as name, CallsBlocked.number as number, " +
                        "CallsBlocked.timestamp as timestamp, " +
                        "Contacts.blackListOrWhiteList as blackListOrWhiteList " +
                        "FROM CallsBlocked " +
                        "LEFT JOIN Contacts " +
                        "ON ( " +
                        "CallsBlocked.number like '%'|| Contacts.number " +
                        " OR "+
                        "Contacts.number like '%'|| CallsBlocked.number " +
                        " ) "+
                        "Where Contacts.blackListOrWhiteList = '"+ConstantCodes.BLACK_LIST+"' " +
                        "ORDER BY timestamp DESC";


//                list = new Select()
//                        .from(Contacts.class)
//                        .leftJoin(CallsBlocked.class)
//                        .on("CallsBlocked.number like '%'|| Contacts.number")
//                        .where("Contacts.blackListOrWhiteList = ?", new String[] { ConstantCodes.BLACK_LIST })
//                        .execute();

                Cursor cursor = ActiveAndroid.getDatabase().rawQuery(query, null);


//              list = new Select().from(CallsBlocked.class).orderBy("timestamp DESC").execute();

                if (cursor.moveToFirst()) {
                    do {
                        Contacts call = new Contacts();
                        call.name = cursor.getString(cursor.getColumnIndex("name"));
                        call.number = cursor.getString(cursor.getColumnIndex("number"));
                        call.blackListOrWhiteList = cursor.getString(cursor.getColumnIndex("blackListOrWhiteList"));
                        call.timestamp = cursor.getLong(cursor.getColumnIndex("timestamp"));
                        list.add(call);
                    } while (cursor.moveToNext());
                }

                Log.i("CONTACT", "READING BLOCKED HISTORY size" + list.size());

//                ArrayList<Contacts> list =new ArrayList<Contacts>();
//                ArrayList<Contacts> list = Utility.readContacts(getActivity());

                totalSize= list.size();
                list = makeGroupedDate(list);

                return list;
            }

            @Override
            protected void onPostExecute(ArrayList<Contacts> list) {
                super.onPostExecute(list);
                if (isAdded() == false)
                    return;

                Log.i("CONTACT", "READING BLOCKED HISTORY size----" + list.size());
                progressBar.setVisibility(View.GONE);

                if (list != null && list.size() > 0) {

                    mAdapter.updateItem(list);

                    mListView.setVisibility(View.VISIBLE);

                    txtCounterBlockedToday.setText(getString(R.string.x_calls_block_today, totalSize + ""));

                } else {

                    txtCounterBlockedToday.setText(getString(R.string.x_calls_block_today, "0"));

                    mListView.setVisibility(View.GONE);

                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void exportDatabse(String databaseName) {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//"+getActivity().getPackageName()+"//databases//"+databaseName+"";
                String backupDBPath = "backupname1.db";
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private ArrayList<Contacts> makeGroupedDate(ArrayList<Contacts> list) {
        ArrayList<Contacts> listHeadersTrip = new ArrayList<Contacts>();

        int i = 0;
        for (Contacts trip : list) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(trip.timestamp);
            String header = getGroupedDate(calendar);
            if (i == 0) {
                listHeadersTrip.add(new Contacts(header));
            } else {
                if (!header.equals(listHeadersTrip.get(listHeadersTrip.size() - 1).header)) {
                    listHeadersTrip.add(new Contacts(header));
                }
            }
            trip.header = header;
            listHeadersTrip.add(trip);
            i++;
        }

        return listHeadersTrip;
    }

    private String getGroupedDate(Calendar calendar) {
        return DateHelper.getFormatedDate(calendar, DateHelper.DATE_FORMAT_DATE);
        /*Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        if (daysBetween(today, calendar) == 0) {
            return "TODAY";
        } else if (daysBetween(today, calendar) == 1) {
            return "YESTERDAY";
        } else if (isSameWeek(today, calendar)) {
            return "THIS WEEK";
        } else {
            return "OLDER";
        }*/
    }


    class AppAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return 10;
        }

        @Override
        public ApplicationInfo getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(getActivity(),
                        R.layout.list_item_block_today, null);
            }

            return convertView;
        }


    }

    /*Start BT service when BT enable with device*/
    private void startBT(){
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(mBluetoothAdapter !=null){
            if(mBluetoothAdapter.isEnabled()){
                getActivity().startService(new Intent(getActivity(), BluetoothService.class));
            }
        }
    }

}


