package callme.evo.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.activeandroid.OnReadAsync;
import com.activeandroid.query.Select;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import java.util.ArrayList;

import callme.evo.R;
import callme.evo.SyncContactService;
import callme.evo.activity.WhiteListContactDetail;
import callme.evo.activity.dialog_box.blocklist_dialog.AddtoBlocklistManually;
import callme.evo.adapter.CallAdapter;
import callme.evo.global.BroadcastHelper;
import callme.evo.global.ConstantCodes;
import callme.evo.models.Contacts;


public class WhitelistFragment extends Fragment {

    int ADD_CONTACT = 100;

    public WhitelistFragment() {
        // Required empty public constructor
    }

    private CallAdapter mAdapter;
    private SwipeMenuListView mListView;
    private TextView txtError;
    private ProgressBar progressBar;
    private BroadcastHelper broadcastHelper, broadcastHelperContact;
    View view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view != null && mAdapter != null)
            return view;
        view = inflater.inflate(R.layout.fragment_two, container, false);

        broadcastHelper = new BroadcastHelper(getActivity(), ConstantCodes.BROADCAST_REFRESH);
        broadcastHelper.register(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                loadList();
            }
        });

        broadcastHelperContact = new BroadcastHelper(getActivity(), ConstantCodes.BROADCAST_CONTACT_REFRESH);
        broadcastHelperContact.register(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                loadList();
            }
        });


        txtError = (TextView) view.findViewById(R.id.txt_error);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mListView = (SwipeMenuListView) view.findViewById(R.id.listView);

        mAdapter = new CallAdapter(getActivity());
        mListView.setAdapter(mAdapter);


        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Contacts contacts = mAdapter.getItem(position);
                Intent intent = new Intent(getActivity(), WhiteListContactDetail.class);
                intent.putExtra(ConstantCodes.INTENT_NAME, contacts.name);
                intent.putExtra(ConstantCodes.INTENT_NUMBER, contacts.number);
                intent.putExtra(ConstantCodes.INTENT_PHOTO, contacts.thumbnailImage);
                intent.putExtra(ConstantCodes.INTENT_FROM, ConstantCodes.WHITE_LIST);
                startActivity(intent);
            }
        });

        mListView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        loadList();

        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getActivity());
                // set item background
                openItem.setBackground(R.color.red);
                // set item width
                openItem.setWidth(dp2px(90));
                // set a icon
                openItem.setIcon(R.drawable.slide_delete);
                // add to menu
                menu.addMenuItem(openItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getActivity());
                // set item background
                deleteItem.setBackground(R.color.green);
                // set item width
                deleteItem.setWidth(dp2px(90));
                // set a icon
                deleteItem.setIcon(R.drawable.slide_block);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        // set creator
        mListView.setMenuCreator(creator);

        // Left
        mListView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);

        /*@author added by Viram Purohit*/

        mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        // delete
//                        Toast.makeText(getActivity(), "Delete Item", Toast.LENGTH_SHORT).show();
                        Contacts item1 = mAdapter.getItem(position);
                        item1.blackListOrWhiteList = "";
                        item1.update(new String[]{"blackListOrWhiteList"});

                        Intent intentBroadcast = new Intent(ConstantCodes.BROADCAST_REFRESH);
                        BroadcastHelper.sendBroadcast(getActivity(), intentBroadcast);

                        break;
                    case 1:
                        // unblock
                        Contacts item = mAdapter.getItem(position);

                        Intent intent = new Intent(getActivity(), AddtoBlocklistManually.class);
                        intent.putExtra(ConstantCodes.INTENT_NAME, item.name);
                        intent.putExtra(ConstantCodes.INTENT_NUMBER, item.number);
                        startActivity(intent);


                        //  Toast.makeText(getActivity(), "Unblock Item", Toast.LENGTH_SHORT).show();
                        break;
                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });


        //fab click listener
        ImageView action = (ImageView) view.findViewById(R.id.action);
        action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_INSERT);
                intent.setType(ContactsContract.Contacts.CONTENT_TYPE);

// Just two examples of information you can send to pre-fill out data for the
// user.  See android.provider.ContactsContract.Intents.Insert for the complete
// list.
//                intent.putExtra(ContactsContract.Intents.Insert.NAME, "some Contact Name");
//                intent.putExtra(ContactsContract.Intents.Insert.PHONE, "some Phone Number");

// Send with it a unique request code, so when you get called back, you can
// check to make sure it is from the intent you launched (ideally should be
// some public static final so receiver can check against it)

               startActivityForResult(intent, ADD_CONTACT);
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        broadcastHelper.unRegister();
        broadcastHelperContact.unRegister();
    }

    private void loadList() {
        Log.e("SyncContactService ", " -- 4 "+ System.currentTimeMillis());
        new Select().from(Contacts.class)
                .where("blackListOrWhiteList=?", new String[]{ConstantCodes.WHITE_LIST})
                .orderBy("name ASC")
                .executeAsync(new OnReadAsync<Contacts>() {
                    @Override
                    public void onRead(ArrayList<Contacts> list) {
                        Log.e("SyncContactService ", " -- 4 "+ System.currentTimeMillis());

                        progressBar.setVisibility(View.GONE);

                        if (list != null && list.size() > 0) {

                            mAdapter.updateItem(list);

                            mListView.setVisibility(View.VISIBLE);

                            txtError.setVisibility(View.GONE);

                            mAdapter.notifyDataSetChanged();
                            Log.e("SyncContactService ", " -- 7 "+ System.currentTimeMillis());
                        } else {

                            txtError.setText("There are no numbers on your Whitelist");

                            txtError.setVisibility(View.VISIBLE);

                            mListView.setVisibility(View.GONE);

                        }

                    }
                });
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_CONTACT) {
            Intent intent = new Intent(getActivity(), SyncContactService.class);
            getActivity().startService(intent);
        }
    }
}