package callme.evo.activity;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.android.internal.telephony.ITelephony;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.i18n.phonenumbers.PhoneNumberUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import callme.evo.R;
import callme.evo.global.BroadcastHelper;
import callme.evo.global.ConstantCodes;
import callme.evo.global.PhoneUtils;
import callme.evo.global.Utility;
import callme.evo.models.Contacts;
import callme.evo.models.User;

/**
 * Created by sahil desai on 6/1/2016.
 */
public class IncomingCall extends Activity {

    BroadcastHelper broadcastHelper;

    boolean isNumberIdentified = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isNumberIdentified = false;
        if (!getIntent().hasExtra(ConstantCodes.INTENT_NUMBER)) {
            finish();
        }
        setContentView(R.layout.recognized_call);
        broadcastHelper = new BroadcastHelper(this, ConstantCodes.BROADCAST_CALL_STATUS_DISCONNECT);
        broadcastHelper.register(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        });

        String incommingNumber = getIntent().getStringExtra(ConstantCodes.INTENT_NUMBER);

        TextView textNumber = (TextView) findViewById(R.id.text_number);
        textNumber.setText(incommingNumber);

        PhoneNumberUtil.PhoneNumberType phoneNumberType = PhoneUtils.getPhoneNumberType(this, incommingNumber);
        switch (phoneNumberType) {
            case MOBILE:
                ((TextView) findViewById(R.id.text_phone_type)).setText("Mobile Number");
                break;
            // In some regions (e.g. the USA), it is impossible to distinguish between fixed-line and
            // mobile numbers by looking at the phone number itself.
            case FIXED_LINE_OR_MOBILE:
                ((TextView) findViewById(R.id.text_phone_type)).setText("FixedLine Or Mobile");
                break;
            // Freephone lines
            case TOLL_FREE:
                ((TextView) findViewById(R.id.text_phone_type)).setText("Toll Free (Freephone lines)");
            case PREMIUM_RATE:
                ((TextView) findViewById(R.id.text_phone_type)).setText("Premium Rate Phones");
                break;
            // The cost of this call is shared between the caller and the recipient, and is hence typically
            // less than PREMIUM_RATE calls. See // http://en.wikipedia.org/wiki/Shared_Cost_Service for
            // more information.
            case SHARED_COST:
                ((TextView) findViewById(R.id.text_phone_type)).setText("Shared Cost Calls");
                break;
            // Voice over IP numbers. This includes TSoIP (Telephony Service over IP).
            case VOIP:
                ((TextView) findViewById(R.id.text_phone_type)).setText("VOIP Call");
                break;
            // A personal number is associated with a particular person, and may be routed to either a
            // MOBILE or FIXED_LINE number. Some more information can be found here:
            // http://en.wikipedia.org/wiki/Personal_Numbers
            case PERSONAL_NUMBER:
                ((TextView) findViewById(R.id.text_phone_type)).setText("Personal Number");
                break;
            case PAGER:
                ((TextView) findViewById(R.id.text_phone_type)).setText("Pager");
                break;
            // Used for "Universal Access Numbers" or "Company Numbers". They may be further routed to
            // specific offices, but allow one number to be used for a company.
            case UAN:
                ((TextView) findViewById(R.id.text_phone_type)).setText("Company Numbers");
                break;
            // Used for "Voice Mail Access Numbers".
            case VOICEMAIL:
                ((TextView) findViewById(R.id.text_phone_type)).setText("VoiceMail Message");
                break;
            // A phone number is of type UNKNOWN when it does not fit any of the known patterns for a
            // specific region.
            case UNKNOWN:
                ((TextView) findViewById(R.id.text_phone_type)).setText("UNKNOWN");
                break;
        }

        findViewById(R.id.img_accept).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptCall();
            }
        });
        findViewById(R.id.img_reject).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endCall();
            }
        });

        ArrayList<Contacts> contactsList = new Select().from(Contacts.class)
//                                .where("blackListOrWhiteList=? AND (block_type=? OR block_type=?)", new String[]{ConstantCodes.BLACK_LIST, ConstantCodes.BLOCK_TYPE_CALLS, ConstantCodes.BLOCK_TYPE_BOTH})
//                                .where("blackListOrWhiteList=?", new String[]{ConstantCodes.BLACK_LIST})
//                                .and("cleanNumber LIKE '%" + PhoneUtils.getPhoneWithoutCountryCode(context, incomingNumber) + "'")
                .and(" number <> '' ")
                .and(" '" + incommingNumber + "' LIKE '%'||number")
                .or(" number like '%" + incommingNumber + "'")
                .execute();


//        ArrayList<Contacts> contactsList = new Select().from(Contacts.class).where("(number LIKE '%" + incommingNumber + "' OR " + incommingNumber + " LIKE '%'||number ) AND name <> ''").execute();
        if (contactsList != null && contactsList.size() > 0) {

            for (Contacts contact : contactsList) {
                if (!TextUtils.isEmpty(contact.name)) {
                    isNumberIdentified = true;
                    ((TextView) findViewById(R.id.text_name)).setText(contact.name);
                    findViewById(R.id.text_name).setVisibility(View.VISIBLE);
                    String thumbnailImage = contact.thumbnailImage;
                    ImageView imgProfile = (ImageView) findViewById(R.id.img_profile);

                    if (!TextUtils.isEmpty(thumbnailImage))
                        Utility.showCircularImageView(this, imgProfile, Uri.parse(thumbnailImage));
                    else
                        imgProfile.setImageResource(R.drawable.ph_contact);
                    break;
                }
            }
        }

        //Changed here it to false so it comes inside condition
//        if (isNumberIdentified == false) {
        User user = Utility.getLoggedInUser(this);
        if (user != null)
            findNameFromServer(user.iUserID, incommingNumber);
//        }

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (broadcastHelper != null) {
            broadcastHelper.unRegister();
        }
    }

    private ITelephony telephonyService;

    private void endCall() { //919712630253
        TelephonyManager telephony = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        try {

            Method m = Class.forName(telephony.getClass().getName()).getDeclaredMethod("getITelephony");
            m.setAccessible(true);
            telephonyService = (ITelephony) m.invoke(telephony);
            telephonyService.silenceRinger();
            telephonyService.endCall();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void acceptCall() {
        TelephonyManager telephony = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        try {

            Method m = Class.forName(telephony.getClass().getName()).getDeclaredMethod("getITelephony");
            m.setAccessible(true);
            telephonyService = (ITelephony) m.invoke(telephony);
            //telephonyService.silenceRinger();
            telephonyService.answerRingingCall();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findNameFromServer(final String userId, final String incomingNumber) {
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, ConstantCodes.Web.PHONE_SEARCH, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.LOGIN);
                showLog("RESPONSE : " + response.toString());
                showLog("**********");

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has(ConstantCodes.STATUS) && jsonObject.getString(ConstantCodes.STATUS).equalsIgnoreCase(ConstantCodes.SUCCESS)) {
                        JSONObject jsonData = jsonObject.optJSONObject(ConstantCodes.DATA);
                        if (jsonData != null) {
                            String name = jsonData.optString("name");
                            String country = jsonData.optString("country");
                            String carrier = jsonData.optString("carrier");
                            String mobile_country_code = jsonData.optString("mobile_country_code");
                            String mobile_network_code = jsonData.optString("mobile_network_code");

                            String value = carrier + ", " + country;
                            value = value.trim().replaceAll("^,", "").trim();

                            ((TextView) findViewById(R.id.text_carrier_country)).setText(value);

                            if (isNumberIdentified == false)
                                ((TextView) findViewById(R.id.text_name)).setText(name);

                            findViewById(R.id.text_name).setVisibility(View.VISIBLE);
                            if (!TextUtils.isEmpty(jsonData.optString("spam"))) {
                                try {
                                    if (Integer.parseInt(jsonData.optString("spam")) > 0) {
                                        ((TextView) findViewById(R.id.text_spam)).setVisibility(View.VISIBLE);
                                    } else {
                                        ((TextView) findViewById(R.id.text_spam)).setVisibility(View.GONE);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    ((TextView) findViewById(R.id.text_spam)).setVisibility(View.GONE);
                                }
                            } else {
                                ((TextView) findViewById(R.id.text_spam)).setVisibility(View.GONE);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.LOGIN);
                showLogE("RESPONSE : " + (error.networkResponse == null ? (error != null ? error.getLocalizedMessage() : "NullVolleyError") : new String(error.networkResponse.data)));
                showLog("**********");

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = Utility.getDefaultParams();
                params.put("phone_number", incomingNumber);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = Utility.getDefaultHeaders();
                headers.put(ConstantCodes.iUserID, userId);
                return headers;
                /*Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token 04af7328b2346602ad7d581ff738eb25a5482cbf");
                params.put("Apikey", "JXn8e6C29jhZ065i8wyQktY33YD3s9zy");
                return params;*/
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        Utility.getRequestQueue().add(jsonObjReq);
    }


    private static void showLogE(String message) {
        Log.e("ws", message + "");
    }

    private static void showLog(String message) {
        Log.i("ws", message + "");
    }

}
