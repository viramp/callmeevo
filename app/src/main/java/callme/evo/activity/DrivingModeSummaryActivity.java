package callme.evo.activity;

import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;

import java.util.ArrayList;

import callme.evo.R;
import callme.evo.adapter.ActivityAdapter;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.ContactCallLog;
import callme.evo.models.DrivingModeSummary;

/**
 * Created by sahil desai on 6/21/2016.
 */
public class DrivingModeSummaryActivity extends AppCompatActivity {


    private ProgressBar progressBar;
    private ActivityAdapter mAdapter;
    private ListView mListView;
    private TextView txtError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(0,0);
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_driving_mode_summary);


        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        mListView = (ListView) findViewById(R.id.listView);
        txtError = (TextView) findViewById(R.id.txt_error);

//        getSupportFragmentManager().beginTransaction().replace(R.id.container, new ActivityFragment()).commit();

        findViewById(R.id.img_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        loadList(true);

        /*//getting information from database.
        ArrayList<DrivingModeSummary> listCall = new Select().from(DrivingModeSummary.class).where("type='" + ConstantCodes.CALL + "'").execute();
        ArrayList<DrivingModeSummary> listSMS = new Select().from(DrivingModeSummary.class).where("type='" + ConstantCodes.SMS + "'").execute();

        ((TextView) findViewById(R.id.text_calls_blocked)).setText("" + listCall.size() + " Calls Blocked");
        ((TextView) findViewById(R.id.text_sms_blocked)).setText("" + listSMS.size() + " Message Blocked");*/
    }

    @Override
    protected void onDestroy() {
        new Delete().from(DrivingModeSummary.class).execute();
        super.onDestroy();

    }

    //========================
    ArrayList<ContactCallLog> list;

    private void loadList(final boolean showProgress) {

        new AsyncTask<Void, Void, ArrayList<ContactCallLog>>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if (showProgress)
                    progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected ArrayList<ContactCallLog> doInBackground(Void... params) {
                list = getCallDetailsNew();
                return null;
            }

            @Override
            protected void onPostExecute(ArrayList<ContactCallLog> contactCallLogs) {
                super.onPostExecute(contactCallLogs);
                progressBar.setVisibility(View.GONE);

                if (list != null && list.size() > 0) {

                    mAdapter = new ActivityAdapter(DrivingModeSummaryActivity.this, list);

                    mListView.setAdapter(mAdapter);

                    mListView.setVisibility(View.VISIBLE);

                    txtError.setVisibility(View.GONE);

                } else {

                    txtError.setText("There are no activities.");//at time of driving.

                    txtError.setVisibility(View.VISIBLE);

                    mListView.setVisibility(View.GONE);

                }
            }
        }.execute();
    }

//    SELECT '1' as mainId, contacts.name, DrivingModeSummary.message as message, DrivingModeSummary.number, DrivingModeSummary.timestamp,  'WHITE_LIST' as blackListOrWhiteList, Contacts.thumbnailImage  FROM DrivingModeSummary LEFT JOIN Contacts on contacts.number like '%'||tab.number OR tab.number like '%'||contacts.number ORDER BY DrivingModeSummary.timestamp DESC

    private ArrayList<ContactCallLog> getCallDetailsNew() {

//        String query = "SELECT tab.mainId, tab.number, Contacts.name, tab.message, Contacts.blackListOrWhiteList, tab.timestamp, Contacts.thumbnailImage FROM (SELECT ContactCallLog.callLogId as mainId,ContactCallLog.number, \"\" as message, ContactCallLog.timestamp as timestamp FROM ContactCallLog\n" +
//                "UNION \n" +
//                "SELECT Sms.smsId  as mainId,Sms.number, Sms.message, Sms.date as timestamp FROM Sms)\n" +
//                "as tab\n" +
//                "LEFT JOIN Contacts on contacts.number like '%'||tab.number OR tab.number like '%'||contacts.number\n" +
//                "GROUP BY Contacts.name,tab.number\n" +
//                "ORDER BY tab.timestamp DESC ";

        String query = "SELECT '1' as mainId, contacts.name, DrivingModeSummary.body as message, DrivingModeSummary.number, DrivingModeSummary.timestamp, " +
                " Contacts.blackListOrWhiteList as blackListOrWhiteList, Contacts.thumbnailImage " +
                " FROM DrivingModeSummary " +
                " LEFT JOIN Contacts on contacts.number like '%'||DrivingModeSummary.number OR DrivingModeSummary.number like '%'||contacts.number" +
                " ORDER BY DrivingModeSummary.timestamp DESC ";

        Cursor cursor = ActiveAndroid.getDatabase().
                rawQuery(query, null);

        ArrayList<ContactCallLog> list = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            do {
                ContactCallLog contacts = new ContactCallLog();
                contacts.callLogId = cursor.getLong(cursor.getColumnIndex("mainId"));
                contacts.name = cursor.getString(cursor.getColumnIndex("name"));
                contacts.message = cursor.getString(cursor.getColumnIndex("message"));
                contacts.number = cursor.getString(cursor.getColumnIndex("number"));
                contacts.timestamp = cursor.getLong(cursor.getColumnIndex("timestamp"));
                contacts.blackListOrWhiteList = cursor.getString(cursor.getColumnIndex("blackListOrWhiteList"));
                contacts.thumbnailImage = cursor.getString(cursor.getColumnIndex("thumbnailImage"));
                if (contacts.number != null)
                    list.add(contacts);
            } while (cursor.moveToNext());
        }
        return list;
    }
}
