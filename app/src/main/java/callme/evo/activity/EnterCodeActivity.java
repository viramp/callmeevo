package callme.evo.activity;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import callme.evo.R;
import callme.evo.controller.ResendOtpController;
import callme.evo.controller.VerifyOtpController;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.User;
import customtextviews.MyTextViewBold;

/**
 * Created by sahil desai on 6/1/2016.
 */
public class EnterCodeActivity extends Activity {
    private static final String TAG = "EnterCodeActivity";
    private static final int REQUEST_PAYMENT = 101;
    MyTextViewBold getStarted;

    EditText edtOtp;
    TextView txtResendOtp;
    ResendOtpController resendOtpController;
    ProgressDialog progressDialog;

    VerifyOtpController verifyOtpController;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(0,0);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_code);

        user = (User) Utility.parseFromString(getIntent().getStringExtra(ConstantCodes.DATA), User.class);
        verifyOtpController = new VerifyOtpController(this, new VerifyOtpController.VerifyOtpCallback() {
            @Override
            public void onSuccess() {

                showWebViewWay(user.iUserID);

                /*Utility.saveLoggedInUser(EnterCodeActivity.this, user);
                Intent intent = new Intent(EnterCodeActivity.this, ThankYouReferral.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);*/


            }

            @Override
            public void onFail(String message) {
                Log.e(TAG, "onFail: " + message);
            }
        });

        edtOtp = (EditText) findViewById(R.id.edt_otp);
        txtResendOtp = (TextView) findViewById(R.id.txt_resend_otp);


        getStarted = (MyTextViewBold) findViewById(R.id.getStarted);
//        openPayment();
//        initLibrary();
//        showPayPalButton();
        getStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                onFuturePaymentPressed();
//                onSinglePayment();

                //Checking locally otp
                if (TextUtils.isEmpty(edtOtp.getText().toString().trim()) == false) {//&& edtOtp.getText().toString().trim().equalsIgnoreCase(user.activation_code)

//                    showWebViewWay(user.iUserID);
                    //Opening paypal screen for saving information for future payments
//                    openPayment();

                    //saving information
                    verifyOtpController.verifyUser(user.iUserID);

                } else {
                    Toast.makeText(EnterCodeActivity.this, getString(R.string.msg_invalid_otp), Toast.LENGTH_SHORT).show();
                }
            }
        });
        txtResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressDialog = Utility.progressDialog(EnterCodeActivity.this, "Please wait...");
                resendOtpController.sendOtp(user);
            }
        });

        resendOtpController = new ResendOtpController(this, new ResendOtpController.ResendOtpCallback() {
            @Override
            public void onSuccess(User user) {
                progressDialog.dismiss();
                Toast.makeText(EnterCodeActivity.this, getString(R.string.msg_otp_sent_to_mobile), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFail(String message) {
                progressDialog.dismiss();
                Toast.makeText(EnterCodeActivity.this, getString(R.string.msg_unable_to_send_otp), Toast.LENGTH_SHORT).show();
            }
        });
    }

    //===============WEB VIEW WAY==============
    private void showWebViewWay(String iUserID) {


        Intent intent = new Intent(EnterCodeActivity.this, PaymentInfoActivity.class);
        intent.putExtra(ConstantCodes.INTENT_ID, iUserID);
        intent.putExtra(ConstantCodes.DATA, getIntent().getStringExtra(ConstantCodes.DATA));
        intent.putExtra(ConstantCodes.INTENT_FROM, ConstantCodes.PAYMENT_PROCESS);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(0,0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_PAYMENT && resultCode == RESULT_OK) {
            verifyOtpController.verifyUser(user.iUserID);
        } else if (requestCode == REQUEST_PAYMENT && (resultCode == RESULT_CANCELED || resultCode == RESULT_FIRST_USER)) {
            Toast.makeText(this, "Payment processing failed, Try again", Toast.LENGTH_SHORT).show();
        }

    }

/*private static PayPalConfiguration config;

    private void openPayment() {

        // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
        // or live (ENVIRONMENT_PRODUCTION)
        config = new PayPalConfiguration().environment(PayPalConfiguration.ENVIRONMENT_NO_NETWORK)

                .clientId("<YOUR_CLIENT_ID>")

                // Minimally, you will need to set three merchant information properties.
                // These should be the same values that you provided to PayPal when you registered your app.
                .merchantName("Example Store")
                .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
                .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));

        Intent intent = new Intent(this, PayPalService.class);

        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        startService(intent);

//        onFuturePaymentPressed();
    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    public void onSinglePayment() {
        PayPalPayment payment = new PayPalPayment(new BigDecimal("1.75"), "USD", "sample item",
                PayPalPayment.PAYMENT_INTENT_SALE);

        Intent intent = new Intent(this, PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        startActivityForResult(intent, 0);
    }

    public void onFuturePaymentPressed() {
        Intent intent = new Intent(this, PayPalFuturePaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        startActivityForResult(intent, REQUEST_CODE_FUTURE_PAYMENT);
    }

    private static final int REQUEST_CODE_FUTURE_PAYMENT = 100;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            PayPalAuthorization auth = data
                    .getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
            if (auth != null) {
                try {
                    String authorization_code = auth.getAuthorizationCode();

                    sendAuthorizationToServer(auth);

                } catch (Exception e) {
                    Log.e("FuturePaymentExample", "an extremely unlikely failure occurred: ", e);
                }
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            Log.i("FuturePaymentExample", "The user canceled.");
        } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
            Log.i("FuturePaymentExample", "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
        }
    }

    private void sendAuthorizationToServer(PayPalAuthorization auth) {
        Log.d(TAG, "sendAuthorizationToServer() called with: " + "auth = [" + auth + "] = " + auth.toJSONObject());

    }*/

    /*//===================
    public void initLibrary() {
        PayPal pp = PayPal.getInstance();

        if (pp == null) {  // Test to see if the library is already initialized

            // This main initialization call takes your Context, AppID, and target server
            pp = PayPal.initWithAppID(this, "APP-80W284485P519543T", PayPal.ENV_NONE);

            // Required settings:

            // Set the language for the library
            pp.setLanguage("en_US");

            // Some Optional settings:

            // Sets who pays any transaction fees. Possible values are:
            // FEEPAYER_SENDER, FEEPAYER_PRIMARYRECEIVER, FEEPAYER_EACHRECEIVER, and FEEPAYER_SECONDARYONLY
            pp.setFeesPayer(PayPal.FEEPAYER_EACHRECEIVER);

            // true = transaction requires shipping
            pp.setShippingEnabled(true);

//            _paypalLibraryInit = true;
        }
    }

    CheckoutButton launchPayPalButton;

    private void showPayPalButton() {

        // Generate the PayPal checkout button and save it for later use
        PayPal pp = PayPal.getInstance();
        launchPayPalButton = pp.getCheckoutButton(this, PayPal.BUTTON_278x43, CheckoutButton.TEXT_PAY);

        // The OnClick listener for the checkout button
        launchPayPalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PayPalButtonClick(view);
            }
        });

        // Add the listener to the layout
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        params.bottomMargin = 10;
        launchPayPalButton.setLayoutParams(params);
        launchPayPalButton.setId(R.id.button_add_response);
        ((RelativeLayout) findViewById(R.id.RelativeLayout01)).addView(launchPayPalButton);
        ((RelativeLayout) findViewById(R.id.RelativeLayout01)).setGravity(Gravity.CENTER_HORIZONTAL);
    }

    public void PayPalButtonClick(View arg0) {
// Create a basic PayPal payment
        PayPalPayment payment = new PayPalPayment();

        // Set the currency type
        payment.setCurrencyType("USD");

        // Set the recipient for the payment (can be a phone number)
        payment.setRecipient("ppalav_1285013097_biz@yahoo.com");

        // Set the payment amount, excluding tax and shipping costs
        payment.setSubtotal(new BigDecimal(50));

        // Set the payment type--his can be PAYMENT_TYPE_GOODS,
        // PAYMENT_TYPE_SERVICE, PAYMENT_TYPE_PERSONAL, or PAYMENT_TYPE_NONE
        payment.setPaymentType(PayPal.PAYMENT_TYPE_GOODS);

        // PayPalInvoiceData can contain tax and shipping amounts, and an
        // ArrayList of PayPalInvoiceItem that you can fill out.
        // These are not required for any transaction.
        PayPalInvoiceData invoice = new PayPalInvoiceData();

        // Set the tax amount
        invoice.setTax(new BigDecimal(5));
    }

    public void PayPalActivityResult(int requestCode, int resultCode, Intent intent) {
        switch (resultCode) {
            // The payment succeeded
            case Activity.RESULT_OK:
                String payKey = intent.getStringExtra(PayPalActivity.EXTRA_PAY_KEY);
//                this.paymentSucceeded(payKey);
                Toast.makeText(EnterCodeActivity.this, "passkey"+payKey,Toast.LENGTH_SHORT).show();
                break;

            // The payment was canceled
            case Activity.RESULT_CANCELED:
//                this.paymentCanceled();
                Toast.makeText(EnterCodeActivity.this, "canceled",Toast.LENGTH_SHORT).show();
                break;

            // The payment failed, get the error from the EXTRA_ERROR_ID and EXTRA_ERROR_MESSAGE
            case PayPalActivity.RESULT_FAILURE:
                String errorID = intent.getStringExtra(PayPalActivity.EXTRA_ERROR_ID);
                String errorMessage = intent.getStringExtra(PayPalActivity.EXTRA_ERROR_MESSAGE);
                Toast.makeText(EnterCodeActivity.this, "error id "+errorID+", errorMesag:"+errorMessage,Toast.LENGTH_SHORT).show();
//                this.paymentFailed(errorID, errorMessage);
        }
    }*/
}
