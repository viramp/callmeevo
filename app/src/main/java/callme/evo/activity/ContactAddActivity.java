package callme.evo.activity;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.activeandroid.query.Update;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import callme.evo.R;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.SpamNumbers;
import callme.evo.models.User;

/**
 * Created by pankaj on 13/10/16.
 */
public class ContactAddActivity extends Activity {

    String number;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(0,0);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_add_dialog);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.80);
        getWindow().setLayout(metrics.widthPixels, ViewGroup.LayoutParams.WRAP_CONTENT);

        number = getIntent().getStringExtra(ConstantCodes.INTENT_NUMBER);

        ((EditText) findViewById(R.id.edt_name)).setHint(getString(R.string.label_who_is_x, number));
        findViewById(R.id.icon_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        findViewById(R.id.text_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = Utility.getLoggedInUser(getApplicationContext());
                if (user != null) {
                    EditText edtName = (EditText) findViewById(R.id.edt_name);
                    CheckBox chkSpam = (CheckBox) findViewById(R.id.chk_is_spam);
                    if (!TextUtils.isEmpty(edtName.getText().toString().trim())) {
                        if (Utility.isNetworkConnected(ContactAddActivity.this)) {
                            insertSpamNumber(number,chkSpam);
                            saveNumberToServer(user.iUserID, number, edtName.getText().toString().trim(), chkSpam.isChecked());
                        } else {
                            Toast.makeText(ContactAddActivity.this, getString(R.string.msg_internet_unavailable), Toast.LENGTH_SHORT).show();

                            insertSpamNumber(number,chkSpam);
                            finish();
                        }

                    }
                }
            }
        });

        if (getIntent().getBooleanExtra(ConstantCodes.FOR_REPORT, false)) {
            ((CheckBox) findViewById(R.id.chk_is_spam)).setChecked(true);
        } else {
            ((CheckBox) findViewById(R.id.chk_is_spam)).setChecked(false);
        }
    }

    private void insertSpamNumber(String edtNumber, CheckBox chkSpam){
        SpamNumbers spamNumbers = new SpamNumbers();
        spamNumbers.number = edtNumber.toString().trim();
        if(chkSpam.isChecked()){
            spamNumbers.type = ConstantCodes.SPAM;
        }else{
            spamNumbers.type = ConstantCodes.HIDDEN;
        }


        ArrayList<SpamNumbers> spamNumberses = new Select().from(SpamNumbers.class)
                .and(" number <> '' ")
                .and(" '" + edtNumber + "' LIKE '%'||number")
                .or(" number like '%" + edtNumber + "'")
                .execute();

        if (spamNumberses != null && spamNumberses.size() > 0) {
            new Update(SpamNumbers.class).
                    set("type=? ",
                    new String[]{spamNumbers.type
            }).where("number LIKE '%" + edtNumber +
                    "' OR '" +
                    edtNumber + "' LIKE '%'||number").execute();
        } else {
            spamNumbers.save();
        }
    }
    private void saveNumberToServer(final String userId, final String incomingNumber, final String name, final boolean isSpam) {
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, ConstantCodes.Web.SAVE_NUMBER, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.LOGIN);
                showLog("RESPONSE : " + response.toString());
                showLog("**********");

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has(ConstantCodes.STATUS) && jsonObject.getString(ConstantCodes.STATUS).equalsIgnoreCase(ConstantCodes.SUCCESS)) {

                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.LOGIN);
                showLogE("RESPONSE : " + (error.networkResponse == null ? (error != null ? error.getLocalizedMessage() : "NullVolleyError") : new String(error.networkResponse.data)));
                showLog("**********");

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = Utility.getDefaultParams();
                params.put("phone_number", incomingNumber);
                params.put("name", name);
                params.put("is_spam", isSpam ? "1" : "0");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = Utility.getDefaultHeaders();
                headers.put(ConstantCodes.iUserID, userId);
                return headers;
                /*Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token 04af7328b2346602ad7d581ff738eb25a5482cbf");
                params.put("Apikey", "JXn8e6C29jhZ065i8wyQktY33YD3s9zy");
                return params;*/
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        Utility.getRequestQueue().add(jsonObjReq);
        finish();
    }

    private static void showLogE(String message) {
        Log.e("ws", message + "");
    }

    private static void showLog(String message) {
        Log.i("ws", message + "");
    }
}
