package callme.evo.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import android.util.Log;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import callme.evo.R;
import callme.evo.controller.DeleteTranscribeMessageController;
import callme.evo.controller.IsUsefullTranscribeMessageController;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.TranscribedMessageModel;
import callme.evo.models.User;

/**
 * Created by sahil desai on 6/22/2016.
 */
public class TranscribedMessagedetail extends Activity {

    private TextView textName, textMessage, textDate, textTime, reply, textReplyByAudio, textReplyByText;

    private ImageView imgDelete, imgPlayVoiceMessage;
    private DeleteTranscribeMessageController deleteTranscribeMessageController;
    private ProgressDialog progressBar;
    private IsUsefullTranscribeMessageController isUsefullTranscribeMessageController;
    private String isHelpfull = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transcribed_message_detail);

        final int position = getIntent().getIntExtra(ConstantCodes.POSITION, -1);
        final TranscribedMessageModel model = (TranscribedMessageModel) Utility.parseFromString(getIntent().getStringExtra(ConstantCodes.DATA), TranscribedMessageModel.class);
        if (model == null) {
            finish();
        }

        if ("yes".equalsIgnoreCase(model.isHelpfull) || "no".equalsIgnoreCase(model.isHelpfull)) {
            findViewById(R.id.layout_helpful).setVisibility(View.GONE);
        }

        isUsefullTranscribeMessageController = new IsUsefullTranscribeMessageController(this, new IsUsefullTranscribeMessageController.IsUsefullTranscribeMessageCallback() {
            @Override
            public void onSuccess() {
                findViewById(R.id.layout_helpful).setVisibility(View.GONE);

                if (progressBar != null)
                    progressBar.dismiss();

                Intent data = new Intent();
                data.putExtra(ConstantCodes.ACTION, ConstantCodes.ACTION_UPDATE);
                data.putExtra(ConstantCodes.POSITION, position);
                data.putExtra(ConstantCodes.HELPFULL, isHelpfull);
                setResult(RESULT_OK, data);
                finish();
            }

            @Override
            public void onFail(String message) {
                if (progressBar != null)
                    progressBar.dismiss();
            }
        });
        deleteTranscribeMessageController = new DeleteTranscribeMessageController(this, new DeleteTranscribeMessageController.DeleteTranscribeMessageCallback() {
            @Override
            public void onSuccess() {
                if (progressBar != null)
                    progressBar.dismiss();

                Intent data = new Intent();
                data.putExtra(ConstantCodes.POSITION, position);
                data.putExtra(ConstantCodes.ACTION, ConstantCodes.ACTION_DELETE);
                setResult(RESULT_OK, data);
                finish();
            }

            @Override
            public void onFail(String message) {
                if (progressBar != null)
                    progressBar.dismiss();
                Toast.makeText(TranscribedMessagedetail.this, message, Toast.LENGTH_SHORT).show();
            }
        });

        imgDelete = (ImageView) findViewById(R.id.img_delete);
        textName = (TextView) findViewById(R.id.text_name);
        textMessage = (TextView) findViewById(R.id.text_message);
        textDate = (TextView) findViewById(R.id.text_date);
        textTime = (TextView) findViewById(R.id.text_time);
        imgPlayVoiceMessage = (ImageView) findViewById(R.id.img_play_voice_message);
        reply = (TextView) findViewById(R.id.text_reply);
        textReplyByAudio = (TextView) findViewById(R.id.text_reply_audio);
        textReplyByText = (TextView) findViewById(R.id.text_reply);

        TextView textNo = (TextView) findViewById(R.id.text_no);
        TextView textYes = (TextView) findViewById(R.id.text_yes);

        textNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = Utility.getLoggedInUser(TranscribedMessagedetail.this);
                if (user != null) {
                    isHelpfull = "no";
                    progressBar = Utility.progressDialog(TranscribedMessagedetail.this, "Please wait...");
                    isUsefullTranscribeMessageController.setUseful(false, model.iScriptID, user.iUserID);
                }
            }
        });
        textYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = Utility.getLoggedInUser(TranscribedMessagedetail.this);
                if (user != null) {
                    isHelpfull = "yes";
                    progressBar = Utility.progressDialog(TranscribedMessagedetail.this, "Please wait...");
                    isUsefullTranscribeMessageController.setUseful(true, model.iScriptID, user.iUserID);
                }
            }
        });

        textMessage.setText(model.tScript);
        textName.setText(model.name);

        textDate.setText(model.date);
        textTime.setText(model.time);

        imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = Utility.getLoggedInUser(TranscribedMessagedetail.this);
                if (user != null) {
                    progressBar = Utility.progressDialog(TranscribedMessagedetail.this, "Please wait...");
                    deleteTranscribeMessageController.delete(model.iScriptID, user.iUserID);
                }
            }
        });

        findViewById(R.id.imageOpenDrawer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        imgPlayVoiceMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.speak(TranscribedMessagedetail.this, model.eType, model.tScript);
            }
        });
        textReplyByAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    Utility.speak(getActivity(), model.eType, model.tScript);
                promptSpeechInput(model.phone_number);
            }
        });
        textReplyByText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    Utility.speak(getActivity(), model.eType, model.tScript);
                sendByAudio(model.phone_number);
            }
        });

        /*
        play_voicemail = (MyTextViewBook) findViewById(R.id.play_voicemail);
        play_voicemail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(TranscribedMessagedetail.this, PlayVoiceMaildialog.class);
                startActivity(intent);
            }
        });*/
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private static final int REQ_CODE_SPEECH_INPUT = 101;
    String phoneNumberClickedForReply;

    /**
     * Showing google speech input dialog
     */
    private void promptSpeechInput(String phoneNumber) {
        phoneNumberClickedForReply = phoneNumber;
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Speek something");
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(TranscribedMessagedetail.this, "Speech not supported by this phone", Toast.LENGTH_SHORT).show();
        }
    }

    /*private void setUseful(final boolean isUsefull, final String iScriptID, final String iUserId) {
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, ConstantCodes.Web.IS_USE_FULL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.LOGIN);
                showLog("RESPONSE : " + response.toString());
                showLog("**********");

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has(ConstantCodes.STATUS) && jsonObject.getString(ConstantCodes.STATUS).equalsIgnoreCase(ConstantCodes.SUCCESS)) {

                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.LOGIN);
                showLogE("RESPONSE : " + (error.networkResponse == null ? (error != null ? error.getLocalizedMessage() : "NullVolleyError") : new String(error.networkResponse.data)));
                showLog("**********");

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = Utility.getDefaultParams();
                params.put(ConstantCodes.is_helpful, isUsefull ? "yes" : "no");
                params.put(ConstantCodes.iScriptID, iScriptID);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = Utility.getDefaultHeaders();
                headers.put(ConstantCodes.iUserID, userId);
                return headers;
                *//*Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token 04af7328b2346602ad7d581ff738eb25a5482cbf");
                params.put("Apikey", "JXn8e6C29jhZ065i8wyQktY33YD3s9zy");
                return params;*//*
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        Utility.getRequestQueue().add(jsonObjReq);
    }*/

    /**
     * Receiving speech input
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == Activity.RESULT_OK) {

                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    if (data != null && !TextUtils.isEmpty(phoneNumberClickedForReply) && result != null && result.size() > 0 && !TextUtils.isEmpty(result.get(0))) {

                        final String message = result.get(0);

                        AlertDialog.Builder builder = new AlertDialog.Builder(TranscribedMessagedetail.this);
                        builder.setTitle("Voice Message to send");
                        builder.setMessage(message);
                        builder.setPositiveButton("Send", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                sendVoiceMessage(Utility.getLoggedInUser(TranscribedMessagedetail.this).iUserID, phoneNumberClickedForReply, Utility.getLoggedInUser(TranscribedMessagedetail.this).voiceType, message);
                            }
                        });
                        builder.setNegativeButton("Don't send", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        AlertDialog alertVoice = builder.create();
                        alertVoice.show();

                    } else {
                        Toast.makeText(TranscribedMessagedetail.this, "Can't recognize, Try again", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            }

        }
    }

    private void sendVoiceMessage(final String userId, final String incommingNumber, final String voiceType, final String message) {
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, ConstantCodes.Web.SEND_VOICE_MAIL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.LOGIN);
                showLog("RESPONSE : " + response.toString());
                showLog("**********");

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has(ConstantCodes.STATUS) && jsonObject.getString(ConstantCodes.STATUS).equalsIgnoreCase(ConstantCodes.SUCCESS)) {

                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.LOGIN);
                showLogE("RESPONSE : " + (error.networkResponse == null ? (error != null ? error.getLocalizedMessage() : "NullVolleyError") : new String(error.networkResponse.data)));
                showLog("**********");

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = Utility.getDefaultParams();
                params.put("phone_number", incommingNumber);
                params.put("eType", voiceType);
                params.put("tScript", message);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = Utility.getDefaultHeaders();
                headers.put(ConstantCodes.iUserID, userId);
                return headers;
                /*Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token 04af7328b2346602ad7d581ff738eb25a5482cbf");
                params.put("Apikey", "JXn8e6C29jhZ065i8wyQktY33YD3s9zy");
                return params;*/
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        Utility.getRequestQueue().add(jsonObjReq);
    }

    private static void showLogE(String message) {
        Log.e("ws", message + "");
    }

    private static void showLog(String message) {
        Log.i("ws", message + "");
    }

    private void sendByAudio(final String phoneNumberClickedForReply) {

        final View view = LayoutInflater.from(TranscribedMessagedetail.this).inflate(R.layout.dialog_create_voice_msg, null, false);
        view.findViewById(R.id.edt_title).setVisibility(View.GONE);
        final EditText edtMessage = (EditText) view.findViewById(R.id.edt_message);
        view.findViewById(R.id.text_save).setVisibility(View.GONE);
//        edtSave.setText("Send");

        AlertDialog.Builder builder = new AlertDialog.Builder(TranscribedMessagedetail.this);
        builder.setView(view);
//        builder.setTitle("Voice Message by text");
//        builder.setMessage(message);
        builder.setPositiveButton("Send", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                String message = edtMessage.getText().toString().trim();
                sendVoiceMessage(Utility.getLoggedInUser(TranscribedMessagedetail.this).iUserID, phoneNumberClickedForReply, Utility.getLoggedInUser(TranscribedMessagedetail.this).voiceType, message);
            }
        });
        builder.setNegativeButton("Don't send", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertVoice = builder.create();
        alertVoice.show();
    }
}