package callme.evo.activity;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import callme.evo.R;
import callme.evo.controller.ForgotPasswordController;
import callme.evo.global.Utility;
import customtextviews.MyTextViewMedium;

/**
 * Created by sahil desai on 6/1/2016.
 */
public class ForgotPasswordActivity extends Activity {

    MyTextViewMedium tvSignin, tvSendEmail;
    ForgotPasswordController forgotPasswordController;
    EditText edtEmail;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(0,0);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        edtEmail = (EditText) findViewById(R.id.edt_email);
        tvSignin = (MyTextViewMedium) findViewById(R.id.tvSignin);
        tvSendEmail = (MyTextViewMedium) findViewById(R.id.txt_send_email);

        tvSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        tvSendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressDialog = Utility.progressDialog(ForgotPasswordActivity.this, "Please wait...");
                forgotPasswordController.forgotPassword(edtEmail.getText().toString());
            }
        });

        forgotPasswordController = new ForgotPasswordController(this, new ForgotPasswordController.ForgotPasswordCallback() {
            @Override
            public void onSuccess() {
                progressDialog.dismiss();
                Toast.makeText(ForgotPasswordActivity.this, getString(R.string.msg_email_sent_to_mail), Toast.LENGTH_SHORT).show();
                edtEmail.setText("");
                finish();
            }

            @Override
            public void onFail(String message) {
                progressDialog.dismiss();
                Toast.makeText(ForgotPasswordActivity.this, message + "", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
