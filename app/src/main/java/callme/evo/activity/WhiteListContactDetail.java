package callme.evo.activity;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.moez.QKSMS.mmssms.Utils;
import com.moez.QKSMS.ui.messagelist.MessageListActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import callme.evo.R;
import callme.evo.activity.dialog_box.add_to_voicemail.AddtoVoiceMail;
import callme.evo.activity.dialog_box.blocklist_dialog.AddtoBlocklistManually;
import callme.evo.activity.dialog_box.priority_dialog.PriorityDialog;
import callme.evo.global.BroadcastHelper;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.Contacts;
import customtextviews.MyTextViewBook;

/**
 * Created by sahil desai on 6/21/2016.
 */
public class WhiteListContactDetail extends Activity {

    private SwipeMenuListView mListView;
    private AppAdapter mAdapter;

    MyTextViewBook priority, voicemail;
    private TextView txtName, txtStatus, txtRemove, txtBlockUnBlock;
    ImageView imgBlockUnBlock;
    BroadcastHelper broadcastHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whitelist_details);

        findViewById(R.id.img_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ImageView profileImage = (ImageView) findViewById(R.id.profileImage);

        String thumbnailImage = getIntent().getStringExtra(ConstantCodes.INTENT_PHOTO);
        if (!TextUtils.isEmpty(thumbnailImage)) {
//            Glide.with(context).load(Uri.parse(item.thumbnailImage)).into(holder.imgProfile);
            Utility.showCircularImageView(this, profileImage, Uri.parse(thumbnailImage));
        } else {
            profileImage.setImageResource(R.drawable.ph_contact);
        }

        mListView = (SwipeMenuListView) findViewById(R.id.listView);
        if (getIntent().getBooleanExtra(ConstantCodes.INTENT_FROM_MESSAGE, false)) {
            findViewById(R.id.label_calllog).setVisibility(View.GONE);
            mListView.setVisibility(View.GONE);
        }

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mAdapter != null) {
                    Contacts model = mAdapter.getItem(position);
                    if (!(model.duration > 0) && model.contactId > 0) {
                        //Here contactId is thread id
                        MessageListActivity.launch(WhiteListContactDetail.this, model.contactId, -1, null, true);
                    }
                }
            }
        });
        txtName = (TextView) findViewById(R.id.txt_name);
        txtStatus = (TextView) findViewById(R.id.txt_status);
        txtRemove = (TextView) findViewById(R.id.txt_remove);
        voicemail = (MyTextViewBook) findViewById(R.id.voicemail);
        imgBlockUnBlock = (ImageView) findViewById(R.id.img_block_unblock);
        txtBlockUnBlock = (TextView) findViewById(R.id.txt_block_unblock);

        if (getIntent() != null && getIntent().hasExtra(ConstantCodes.INTENT_NAME)) {
            if (ConstantCodes.WHITE_LIST.equalsIgnoreCase(getIntent().getStringExtra(ConstantCodes.INTENT_FROM))) {
                txtStatus.setText("Currently on your Whitelist");
                voicemail.setVisibility(View.VISIBLE);
                imgBlockUnBlock.setImageResource(R.drawable.profile_block);
                txtBlockUnBlock.setText("Block");
            } else if (ConstantCodes.BLACK_LIST.equalsIgnoreCase(getIntent().getStringExtra(ConstantCodes.INTENT_FROM))) {
                txtStatus.setText("Currently on your Blacklist");
                voicemail.setVisibility(View.GONE);
                imgBlockUnBlock.setImageResource(R.drawable.unblock);
                txtBlockUnBlock.setText("Unblock");
            } else {
                ((View) txtRemove.getParent()).setVisibility(View.GONE);
            }
            imgBlockUnBlock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    blockUnblock();
                }
            });
            txtRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Contacts contacts = new Contacts();
                    contacts.name = getIntent().getStringExtra(ConstantCodes.INTENT_NAME);
                    contacts.number = getIntent().getStringExtra(ConstantCodes.INTENT_NUMBER);
                    contacts.blackListOrWhiteList = "";
                    contacts.block_type = "";

                    new Update(Contacts.class).set("blackListOrWhiteList='', block_type=''")
                            .where("number LIKE '%" + getIntent().getStringExtra(ConstantCodes.INTENT_NUMBER) + "' OR " +
                                    "'" + getIntent().getStringExtra(ConstantCodes.INTENT_NUMBER) + "' LIKE '%'||number").execute();

                    Intent intentBroadcast = new Intent(ConstantCodes.BROADCAST_REFRESH);
                    BroadcastHelper.sendBroadcast(WhiteListContactDetail.this, intentBroadcast);

                    txtBlockUnBlock.setText("Block");
                    imgBlockUnBlock.setImageResource(R.drawable.profile_block);

                    ((View) txtRemove.getParent()).setVisibility(View.GONE);
                    Toast.makeText(WhiteListContactDetail.this, "Removed from whitelist", Toast.LENGTH_SHORT).show();
                }
            });
            if (!TextUtils.isEmpty(getIntent().getStringExtra(ConstantCodes.INTENT_NAME))) {
                txtName.setText(getIntent().getStringExtra(ConstantCodes.INTENT_NAME));
            } else {
                txtName.setText(getIntent().getStringExtra(ConstantCodes.INTENT_NUMBER));
            }
        }

        findViewById(R.id.img_call).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentCall = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + getIntent().getStringExtra(ConstantCodes.INTENT_NUMBER)));
                startActivity(intentCall);
                overridePendingTransition(0, 0);
            }
        });
        findViewById(R.id.img_sms).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String number=getIntent().getStringExtra(ConstantCodes.INTENT_NUMBER);
                long threadId = Utils.getOrCreateThreadId(WhiteListContactDetail.this, number);
                MessageListActivity.launch(WhiteListContactDetail.this, threadId, -1, null, true);

            }
        });

        findViewById(R.id.img_rate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ContactAddActivity.class);
                intent.putExtra(ConstantCodes.INTENT_NUMBER, getIntent().getStringExtra(ConstantCodes.INTENT_NUMBER));
                intent.putExtra(ConstantCodes.FOR_REPORT, true);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        voicemail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(WhiteListContactDetail.this, AddtoVoiceMail.class);
                intent.putExtra(ConstantCodes.INTENT_NAME, getIntent().getStringExtra(ConstantCodes.INTENT_NAME));
                intent.putExtra(ConstantCodes.INTENT_NUMBER, getIntent().getStringExtra(ConstantCodes.INTENT_NUMBER));
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        voicemail.setPaintFlags(voicemail.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        priority = (MyTextViewBook) findViewById(R.id.priority);
        priority.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WhiteListContactDetail.this, PriorityDialog.class);
                intent.putExtra(ConstantCodes.INTENT_NUMBER, getIntent().getStringExtra(ConstantCodes.INTENT_NUMBER));
                intent.putExtra(ConstantCodes.INTENT_NAME, getIntent().getStringExtra(ConstantCodes.INTENT_NAME));
                intent.putExtra(ConstantCodes.INTENT_PRIORITY, priority.getText().toString().trim());
                startActivityForResult(intent, ACTION_PRIORITY);
            }
        });

        priority.setPaintFlags(priority.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        getCallDetails(getIntent().getStringExtra(ConstantCodes.INTENT_NUMBER));


        //Geting priority from contacts
        Contacts contact = new Select().from(Contacts.class)
                .where("number LIKE '%" + getIntent().getStringExtra(ConstantCodes.INTENT_NUMBER) + "'")
                .or("'" + getIntent().getStringExtra(ConstantCodes.INTENT_NUMBER) + "' LIKE '%'||number")
                .executeSingle();

        if (contact != null && contact.priority == 1) {
            priority.setText("High");
        } else {
            priority.setText("Normal");
        }

    }

    private void blockUnblock() {
        if (getIntent() != null && getIntent().hasExtra(ConstantCodes.INTENT_NAME)) {
            if (ConstantCodes.WHITE_LIST.equalsIgnoreCase(getIntent().getStringExtra(ConstantCodes.INTENT_FROM))) {

                /*Contacts contacts = new Contacts();
                contacts.name = getIntent().getStringExtra(ConstantCodes.INTENT_NAME);
                contacts.number = getIntent().getStringExtra(ConstantCodes.INTENT_NUMBER);
                contacts.blackListOrWhiteList = ConstantCodes.BLACK_LIST;
                contacts.block_type = "";
                contacts.update(new String[]{"blackListOrWhiteList", "block_type"});*/

                Intent intent = new Intent(WhiteListContactDetail.this, AddtoBlocklistManually.class);
                intent.putExtra(ConstantCodes.INTENT_NAME, getIntent().getStringExtra(ConstantCodes.INTENT_NAME));
                intent.putExtra(ConstantCodes.INTENT_NUMBER, getIntent().getStringExtra(ConstantCodes.INTENT_NUMBER));
                startActivityForResult(intent, ACTION_BLOCK_NUMBER);

            } else {

                Contacts contacts = new Contacts();
                contacts.name = getIntent().getStringExtra(ConstantCodes.INTENT_NAME);
                contacts.number = getIntent().getStringExtra(ConstantCodes.INTENT_NUMBER);
                contacts.blackListOrWhiteList = "";
                contacts.block_type = "";
//                contacts.update(new String[]{"blackListOrWhiteList", "block_type"});

                new Update(Contacts.class).set("blackListOrWhiteList=?,block_type=? ", new String[]{
                        "",
                        ""
                }).where("number like '%" + contacts.number + "' OR '" + contacts.number + "' LIKE '%'||number")
                        .execute();

                Intent intentBroadcast = new Intent(ConstantCodes.BROADCAST_REFRESH);
                BroadcastHelper.sendBroadcast(WhiteListContactDetail.this, intentBroadcast);

                txtBlockUnBlock.setText("Block");
                imgBlockUnBlock.setImageResource(R.drawable.profile_block);
                getIntent().putExtra(ConstantCodes.INTENT_FROM, ConstantCodes.WHITE_LIST);

            }
        }
    }

    private final int ACTION_BLOCK_NUMBER = 23;
    private final int ACTION_PRIORITY = 24;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == ACTION_BLOCK_NUMBER) {
            txtBlockUnBlock.setText("Ublock");
            imgBlockUnBlock.setImageResource(R.drawable.unblock);
            getIntent().putExtra(ConstantCodes.INTENT_FROM, ConstantCodes.BLACK_LIST);
        } else if (resultCode == RESULT_OK && requestCode == ACTION_PRIORITY) {
            priority.setText(data.getStringExtra(ConstantCodes.INTENT_PRIORITY));
            BroadcastHelper.sendBroadcast(WhiteListContactDetail.this, ConstantCodes.BROADCAST_CONTACT_REFRESH);
            BroadcastHelper.sendBroadcast(WhiteListContactDetail.this, ConstantCodes.BROADCAST_REFRESH);
        }
    }

    private void block() {

    }

    private void unblock() {

    }

    class AppAdapter extends BaseAdapter {

        ArrayList<Contacts> contactList;

        public AppAdapter(ArrayList<Contacts> contactList) {
            this.contactList = contactList;
        }

        @Override
        public int getCount() {
            return contactList.size();
        }

        @Override
        public Contacts getItem(int position) {
            return contactList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = View.inflate(getApplicationContext(), R.layout.list_item_white_list_call_log, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            Contacts item = getItem(position);
            if (item.timestamp > 0) {
                try {

                    long time = item.timestamp;
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTimeInMillis(time);

                    SimpleDateFormat date = new SimpleDateFormat("dd MMM yy");
                    holder.txtDate.setText(date.format(calendar.getTime()));

                    SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
                    holder.txtTime.setText(timeFormat.format(calendar.getTime()));

                    if (item.duration >= 0) {
                        holder.txtDuration.setText(Utility.getHrsMinsSec(item.duration, "hh:mm:ss"));
                    } else {
                        holder.txtDuration.setText(item.testMessage + "");//SMS
                    }
                    holder.txtDate.setVisibility(View.VISIBLE);
                    holder.txtTime.setVisibility(View.VISIBLE);

                } catch (Exception e) {
                    e.printStackTrace();
                    holder.txtDate.setVisibility(View.GONE);
                    holder.txtTime.setVisibility(View.GONE);
                }
            } else {
                holder.txtDate.setVisibility(View.GONE);
                holder.txtTime.setVisibility(View.GONE);
            }

            return convertView;
        }

        class ViewHolder {
            TextView txtDate, txtTime, txtDuration;

            public ViewHolder(View view) {
                txtDate = (TextView) view.findViewById(R.id.txt_date);
                txtTime = (TextView) view.findViewById(R.id.txt_time);
                txtDuration = (TextView) view.findViewById(R.id.txt_duration);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    private ArrayList<Contacts> getCallDetails(String incommingNumber) {

        String query = "SELECT  * FROM (select \"SMS\",message as message,smsId as Id,date as timestamp, -1 as duration,message from Sms where number like '%" + incommingNumber + "%'\n" +
                "UNION\n" +
                "select \"CONTACT\", message as message, 0 as Id, timestamp as timestamp, duration as duration, name from ContactCallLog where number like '%" + incommingNumber + "%'\n" +
                ") ORDER BY timestamp DESC";

        Cursor cur = ActiveAndroid.getDatabase().rawQuery(query, null);
        ArrayList<Contacts> recentsCalls = new ArrayList<Contacts>();
        if (cur.moveToFirst()) {
            do {

                Contacts contacts = new Contacts();
                contacts.contactId = cur.getLong(cur.getColumnIndex("Id"));
                contacts.duration = cur.getLong(cur.getColumnIndex("duration"));
                contacts.timestamp = cur.getLong(cur.getColumnIndex("timestamp"));
                contacts.testMessage = cur.getString(cur.getColumnIndex("message"));
                recentsCalls.add(contacts);
            } while (cur.moveToNext());
        }
        cur.close();

        /*ArrayList<Contacts> recentsCalls = new ArrayList<Contacts>();
        StringBuffer sb = new StringBuffer();
        Cursor managedCursor = managedQuery(CallLog.Calls.CONTENT_URI, null, CallLog.Calls.NUMBER + " LIKE '%" + Utility.getFinalNumber(incommingNumber) + "'", null, CallLog.Calls.DATE + " DESC");
        int name = managedCursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
//        sb.append("Call Details :");
        while (managedCursor.moveToNext()) {
            String phName = managedCursor.getString(name);
            String phNumber = managedCursor.getString(number);
            String callType = managedCursor.getString(type);
            String callDate = managedCursor.getString(date);
            Date callDayTime = new Date(Long.valueOf(callDate));
            String callDuration = managedCursor.getString(duration);
            String dir = null;
            int dircode = Integer.parseInt(callType);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "OUTGOING";
                    break;

                case CallLog.Calls.INCOMING_TYPE:
                    dir = "INCOMING";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "MISSED";
                    break;
            }

            Contacts contacts = new Contacts();

            contacts.name = phName;
            contacts.number = phNumber;
            contacts.timestamp = Long.valueOf(callDate);
            contacts.duration = Long.valueOf(callDuration);

            recentsCalls.add(contacts);

//            sb.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- " + dir + " \nCall Date:--- " + callDayTime + " \nCall duration in sec :--- " + callDuration);
//            sb.append("\n----------------------------------");
        }
//        managedCursor.close();*/

        mAdapter = new AppAdapter(recentsCalls);
        mListView.setAdapter(mAdapter);

        return recentsCalls;
//        return mergeAndSort(recentsCalls);
    }
}
