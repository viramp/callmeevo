package callme.evo.activity;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import callme.evo.R;
import callme.evo.controller.LoginController;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.User;
import customtextviews.MyTextViewMedium;

/**
 * Created by sahil desai on 6/1/2016.
 */
public class LoginActivity extends Activity {

    MyTextViewMedium tvSignin, tvSignUp;
    LoginController loginController;
    EditText edtUsername, edtPassword;
    TextView txtForgotPassword;
    ProgressDialog progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(0,0);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Utility.getSharedPreference(getApplicationContext()).edit().
                putBoolean(ConstantCodes.IS_DRIVING_MODE_OPEN,false).commit();

        edtUsername = (EditText) findViewById(R.id.edt_username);
        edtPassword = (EditText) findViewById(R.id.edt_password);

        txtForgotPassword = (TextView) findViewById(R.id.txt_forgot_password);
        tvSignin = (MyTextViewMedium) findViewById(R.id.tvSignin);
        tvSignUp = (MyTextViewMedium) findViewById(R.id.tvSignUp);

        //Checking for default sms app
        // Otherwise... check if we're not the default SMS app
        /*boolean isDefaultSmsApp = Utils.isDefaultSmsApp(LoginActivity.this);

        if (!isDefaultSmsApp) {
            // Ask to become the default SMS app
            new DefaultSmsHelper(LoginActivity.this, R.string.not_default_block).showIfNotDefault((ViewGroup) findViewById(android.R.id.content));
        }*/

        txtForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
                overridePendingTransition(0,0);
            }
        });
        tvSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (true) {
                    progressBar = Utility.progressDialog(LoginActivity.this, "Please wait...");
                    loginController.login(edtUsername.getText().toString(), edtPassword.getText().toString());
                } else {
                    Intent intent = new Intent(LoginActivity.this, HomeScreen.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(0,0);
                }
            }
        });

        tvSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SIgnUpActivity.class);
                startActivity(intent);
                overridePendingTransition(0,0);
            }
        });

        loginController = new LoginController(this, new LoginController.LoginCallback() {
            @Override
            public void onSuccess(User user) {
//                user.token_varified = 0;
                progressBar.dismiss();
                if (user.token_varified == 1) {
                    if ("failed".equalsIgnoreCase(user.ePaymentStatus)) {
                        Intent intent = new Intent(LoginActivity.this, PaymentInfoActivity.class);
                        intent.putExtra(ConstantCodes.DATA, Utility.getJsonString(user));
                        startActivity(intent);
                        overridePendingTransition(0,0);
                    } else {
                        Utility.saveLoggedInUser(LoginActivity.this, user);
                        Intent intent = new Intent(LoginActivity.this, HomeScreen.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(0,0);
                    }
                } else {
                    Intent intent = new Intent(LoginActivity.this, EnterCodeActivity.class);
                    intent.putExtra(ConstantCodes.DATA, Utility.getJsonString(user));
                    startActivity(intent);
                    overridePendingTransition(0,0);
                }
            }

            @Override
            public void onFail(String message) {
                progressBar.dismiss();
                if (Utility.isInternetUnavailableMsg(message)) {
                    Toast.makeText(LoginActivity.this, getString(R.string.msg_internet_unavailable), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(LoginActivity.this, message + "", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}