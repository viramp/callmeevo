package callme.evo.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.bumptech.glide.Glide;
import com.imagepicker.activity.ImagePicker;
import com.imagepicker.activity.ImagePickerActivity;
import com.imagepicker.model.Image;

import java.io.File;
import java.util.ArrayList;

import callme.evo.R;
import callme.evo.global.BroadcastHelper;
import callme.evo.global.ConstantCodes;
import callme.evo.global.FocusableRadioGroup;
import callme.evo.global.Utility;
import callme.evo.models.User;
import callme.evo.models.VoiceMessageModel;

/**
 * Created by sahil desai on 6/22/2016.
 */
public class MessageSetting extends Activity {

    private final int REQUEST_CODE_PICKER = 100;
    private TextView textCreateMessage, textEdit, message;
    private VoiceMessageModel voiceMessageModel;
    private TextView text_play_voice_message;
    private TextView textEditChatBackground;
    private ImageView imgChatBackground;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.messagesetting);
        FocusableRadioGroup layoutVoiceType = (FocusableRadioGroup) findViewById(R.id.radio_group_voice_type);
        TextView textVoiceLabel = (TextView) findViewById(R.id.text_voice_label);
        message = (TextView) findViewById(R.id.message);
        text_play_voice_message = (TextView) findViewById(R.id.text_play_voice_message);
        textCreateMessage = (TextView) findViewById(R.id.text_create_message);
        textEdit = (TextView) findViewById(R.id.text_edit);
        imgChatBackground = (ImageView) findViewById(R.id.img_chat_background);
        textEditChatBackground = (TextView) findViewById(R.id.text_edit_chat_background);
        textEditChatBackground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImagePicker.create(MessageSetting.this)
                        .folderMode(true) // folder mode (false by default)
                        .folderTitle("Folder") // folder selection title
                        .imageTitle("Tap to select") // image selection title
                        .single() // single mode
//                        .multi() // multi mode (default mode)
//                        .limit(10) // max images can be selected (99 by default)
                        .showCamera(true) // show camera or not (true by default)
                        .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
//                        .origin(images) // original selected images, used in multi mode
                        .start(REQUEST_CODE_PICKER);
            }
        });

        User user = Utility.getLoggedInUser(this);
        String backgroundUrl = Utility.getBackgroundUrl(this);

        Glide
                .with(this)
                .load(backgroundUrl)
                .placeholder(R.drawable.ph_contact)
//                    .centerCrop()
                .error(R.drawable.ph_contact)
                .crossFade()
                .into(imgChatBackground);

        Utility.prepareTTSEngine(this);
        loadPrevMsg();

        if (Utility.isPaidApplication(this)) {
//            findViewById(R.id.layout_greeting).setVisibility(View.GONE);
//            findViewById(R.id.text_greeting).setVisibility(View.GONE);
//            findViewById(R.id.message).setVisibility(View.GONE);
            layoutVoiceType.setEnabled(false);
            textVoiceLabel.setText("VOICE");
            layoutVoiceType.isIntercept = false;
        } else {
//            findViewById(R.id.layout_greeting).setVisibility(View.VISIBLE);
//            findViewById(R.id.text_greeting).setVisibility(View.VISIBLE);
            findViewById(R.id.message).setVisibility(View.VISIBLE);
            layoutVoiceType.setVisibility(View.VISIBLE);
            textVoiceLabel.setText("VOICE (Upgraded Feature)");
            layoutVoiceType.setEnabled(true);
            layoutVoiceType.isIntercept = true;
        }

        if (ConstantCodes.VOICE_FEMALE.equalsIgnoreCase(user.voiceType)) {
            ((RadioButton) findViewById(R.id.radio_female)).setChecked(true);
        } else {
            ((RadioButton) findViewById(R.id.radio_male)).setChecked(true);
        }

        layoutVoiceType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                User user = Utility.getLoggedInUser(MessageSetting.this);
                if (checkedId == R.id.radio_female) {
                    user.voiceType = ConstantCodes.VOICE_FEMALE;
                } else {
                    user.voiceType = ConstantCodes.VOICE_MALE;
                }
                Utility.saveLoggedInUser(MessageSetting.this, user);
            }
        });

        findViewById(R.id.imageOpenDrawer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        /*findViewById(R.id.switch_sms_intercept_all_calls).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.getSharedPreference(MessageSetting.this).edit().putBoolean(ConstantCodes.SMS_INTERCEPT_ALL_CALLS, ((CheckBox) v).isChecked()).commit();
            }
        });
        findViewById(R.id.switch_sms_unwanted_calls).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.getSharedPreference(MessageSetting.this).edit().putBoolean(ConstantCodes.SMS_UNWANTED_CALLS, ((CheckBox) v).isChecked()).commit();
            }
        });
        findViewById(R.id.switch_sms_unknown_calls).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.getSharedPreference(MessageSetting.this).edit().putBoolean(ConstantCodes.SMS_UNKNOWN_CALLS, ((CheckBox) v).isChecked()).commit();
            }
        });*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PICKER && resultCode == AppCompatActivity.RESULT_OK && data != null) {

            ArrayList<Image> images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);
            // do your logic ....
            if (images != null && images.size() > 0) {
                String selectedImagePath = images.get(0).getPath();
                File file = new File(selectedImagePath);
                if (file.exists()) {
                    Uri imageUri = Uri.fromFile(file);

                    Utility.saveBackgroundUrl(MessageSetting.this, file.getPath());

                    Glide
                            .with(this)
                            .load(file.getPath())
                            .placeholder(R.drawable.ph_contact)
//                    .centerCrop()
                            .error(R.drawable.ph_contact)
                            .crossFade()
                            .into(imgChatBackground);
                }
            }
        }
    }

    private void showAddEditMessageDialog(Context context, String title, String message) {
        final View view = LayoutInflater.from(this).inflate(R.layout.dialog_create_voice_msg, null, false);

        final TextView edtTitle = (EditText) view.findViewById(R.id.edt_title);
        final TextView edtMessage = (EditText) view.findViewById(R.id.edt_message);
        edtTitle.setHint(ConstantCodes.DEFAULT_VOICEMESSAGE);
//        edtTitle.setText("Write a voice message and hit on save button");
        edtTitle.setText(ConstantCodes.DEFAULT_VOICEMESSAGE);
        edtMessage.setText(message);
        edtTitle.setEnabled(false);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(view);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        view.findViewById(R.id.text_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (voiceMessageModel == null) {
                    voiceMessageModel = new VoiceMessageModel();
                    voiceMessageModel.voiceMessageId = ConstantCodes.FREE_VOICE_MAIL_TITLE;
                }

                voiceMessageModel.title = edtTitle.getHint().toString();
                voiceMessageModel.message = edtMessage.getText().toString();
                voiceMessageModel.save();
                alertDialog.dismiss();
                loadPrevMsg();

                BroadcastHelper.sendBroadcast(MessageSetting.this, ConstantCodes.BROADCAST_CONTACT_REFRESH);
            }
        });
    }

    private void loadPrevMsg() {

        ArrayList<VoiceMessageModel> list = new Select().from(VoiceMessageModel.class).where("voiceMessageId = '" + ConstantCodes.FREE_VOICE_MAIL_TITLE + "'").execute();

        final User user = Utility.getLoggedInUser(MessageSetting.this);
        if (list != null && list.size() > 0) {
            voiceMessageModel = list.get(0);
            message.setText(voiceMessageModel.message);
            message.setVisibility(View.VISIBLE);
            textCreateMessage.setVisibility(View.GONE);
            textEdit.setVisibility(View.VISIBLE);
            textEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAddEditMessageDialog(MessageSetting.this, voiceMessageModel.title, voiceMessageModel.message);
                }
            });
            text_play_voice_message.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utility.speak(MessageSetting.this, user.voiceType, voiceMessageModel.message);
                }
            });
            text_play_voice_message.setEnabled(true);
        } else {
            message.setVisibility(View.GONE);
            textCreateMessage.setVisibility(View.VISIBLE);
            textEdit.setVisibility(View.GONE);
            textCreateMessage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAddEditMessageDialog(MessageSetting.this, null, "");
                }
            });
            text_play_voice_message.setEnabled(false);
        }
    }
}
