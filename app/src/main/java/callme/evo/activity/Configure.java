package callme.evo.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import callme.evo.R;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import customtextviews.MyTextViewMedium;

public class Configure extends AppCompatActivity {

    MyTextViewMedium saveSettings;

    SharedPreferences.Editor sharedPrefEditor;

    private CheckBox chkUnknownNumbers;
    private CheckBox chkHiddenNumber;
    private CheckBox chkFccDoNotCall;
    private CheckBox chkIdentifiedNumber;
    private CheckBox chkAllNumber;
    private CheckBox chkMsgUnknownNumber;
    private CheckBox chkMsgHiddenNumber;
    private CheckBox chkMsgAllNumbers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configure_device);

//        sharedPreferences = Utility.getSharedPreference(this);
        sharedPrefEditor = Utility.getSharedPreference(this).edit();

        chkUnknownNumbers = (CheckBox) findViewById(R.id.chk_unknown_numbers);
        chkHiddenNumber = (CheckBox) findViewById(R.id.chk_hidden_number);
        chkFccDoNotCall = (CheckBox) findViewById(R.id.chk_fcc_do_not_call);
        chkIdentifiedNumber = (CheckBox) findViewById(R.id.chk_identified_number);
        chkAllNumber = (CheckBox) findViewById(R.id.chk_all_number);
        chkMsgUnknownNumber = (CheckBox) findViewById(R.id.chk_msg_unknown_number);
        chkMsgHiddenNumber = (CheckBox) findViewById(R.id.chk_msg_hidden_number);
        chkMsgAllNumbers = (CheckBox) findViewById(R.id.chk_msg_all_numbers);

        chkUnknownNumbers.setOnCheckedChangeListener(listener);
        chkHiddenNumber.setOnCheckedChangeListener(listener);
        chkFccDoNotCall.setOnCheckedChangeListener(listener);
        chkIdentifiedNumber.setOnCheckedChangeListener(listener);
        chkAllNumber.setOnCheckedChangeListener(listener);
        chkMsgUnknownNumber.setOnCheckedChangeListener(listener);
        chkMsgHiddenNumber.setOnCheckedChangeListener(listener);
        chkMsgAllNumbers.setOnCheckedChangeListener(listener);

        saveSettings = (MyTextViewMedium) findViewById(R.id.saveSettings);
        saveSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sharedPrefEditor.commit();

                Intent intent = new Intent(Configure.this, HomeScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                overridePendingTransition(0,0);
            }
        });
    }

    CompoundButton.OnCheckedChangeListener listener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            String key = null;
            switch (buttonView.getId()) {
                case R.id.chk_unknown_numbers:
                    key = ConstantCodes.UNKNOWN_NUMBER;
                    break;
                case R.id.chk_hidden_number:
                    key = ConstantCodes.HIDDEN_NUMBER;
                    break;
                case R.id.chk_fcc_do_not_call:
                    key = ConstantCodes.DO_NOT_CALL;
                    break;
                case R.id.chk_identified_number:
                    key = ConstantCodes.IDENTIFIED_NUMBER;
                    break;
                case R.id.chk_all_number:
                    key = ConstantCodes.ALL_NUMBER;
                    break;
                case R.id.chk_msg_unknown_number:
                    key = ConstantCodes.MSG_UNKNOWN_NUMBER;
                    break;
                case R.id.chk_msg_hidden_number:
                    key = ConstantCodes.MSG_HIDDEN_NUMBER;
                    break;
                case R.id.chk_msg_all_numbers:
                    key = ConstantCodes.MSG_ALL_NUMBER;
                    break;
            }

            if (!TextUtils.isEmpty(key)) {
                sharedPrefEditor.putBoolean(key, isChecked);
            }

        }
    };

}
