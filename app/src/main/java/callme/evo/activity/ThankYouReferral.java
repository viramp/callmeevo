package callme.evo.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import callme.evo.R;
import customtextviews.MyTextViewBold;

/**
 * Created by sahil desai on 6/2/2016.
 */
public class ThankYouReferral extends Activity {

    MyTextViewBold getStarted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(0,0);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_referral_thank_you);

        getStarted = (MyTextViewBold) findViewById(R.id.ok);

        getStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ThankYouReferral.this, Configure.class);
                startActivity(intent);
                overridePendingTransition(0,0);
            }
        });
    }
}
