package callme.evo.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import callme.evo.R;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.User;
import customtextviews.MyTextViewBold;

/**
 * Created by sahil desai on 6/2/2016.
 */
public class PaymentInfoActivity extends Activity {

    private static final int REQUEST_PAYMENT = 101;
    MyTextViewBold getStarted;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(0,0);
        setContentView(R.layout.activity_payment_info);

        getStarted = (MyTextViewBold) findViewById(R.id.ok);

        user = (User) Utility.parseFromString(getIntent().getStringExtra(ConstantCodes.DATA), User.class);

        getStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PaymentInfoActivity.this, WebViewActivity.class);
                intent.putExtra(ConstantCodes.INTENT_ID, user.iUserID);
                intent.putExtra(ConstantCodes.INTENT_FROM, ConstantCodes.PAYMENT_PROCESS);
                startActivityForResult(intent, REQUEST_PAYMENT);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Sending information to back activity
        if (requestCode == REQUEST_PAYMENT && resultCode == RESULT_OK) {

            //Loading Thankyou screen -> home dashboard
            Utility.saveLoggedInUser(PaymentInfoActivity.this, user);
            Intent intent = new Intent(PaymentInfoActivity.this, ThankYouReferral.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(0,0);

        } else if (requestCode == REQUEST_PAYMENT && (resultCode == RESULT_CANCELED || resultCode == RESULT_FIRST_USER)) {
            Toast.makeText(this, "Payment processing failed, Try again", Toast.LENGTH_SHORT).show();
        }
    }
}
