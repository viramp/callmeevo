package callme.evo.activity;

/**
 * Created by sahil desai on 5/26/2016.
 */

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Telephony;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.moez.QKSMS.mmssms.Utils;

import java.util.ArrayList;
import java.util.List;

import callme.evo.R;
import callme.evo.SyncToServerService;
import callme.evo.activity.dialog_box.blockmode.BlockModeDialog;
import callme.evo.activity.dialog_box.drive_mode.DriveModeDialog;
import callme.evo.controller.ProfileController;
import callme.evo.fragments.ActivityFragment;
import callme.evo.fragments.BlacklistFragment;
import callme.evo.fragments.BluetoothFragment;
import callme.evo.fragments.ContactsFragment;
import callme.evo.fragments.HomeFragment;
import callme.evo.fragments.MessagingFragment;
import callme.evo.fragments.MyProfileFragment;
import callme.evo.fragments.SettingsFragment;
import callme.evo.fragments.VoiceMessageFragment;
import callme.evo.fragments.WhitelistFragment;
import callme.evo.global.BroadcastHelper;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.User;
import callme.evo.models.VoiceMessageModel;
import callme.evo.services.BluetoothService;
import commonclasses.SlidingTab.CustomTabLayout;
import commonclasses.ViewPager.CustomViewPager;

public class HomeScreen extends FragmentActivity {

    HomeFragment homeFragment = new HomeFragment();
    BlacklistFragment blacklistFragment = new BlacklistFragment();
    WhitelistFragment whitelistFragment = new WhitelistFragment();
    ActivityFragment activityFragment = new ActivityFragment();
    ContactsFragment contactsFragment = new ContactsFragment();
    MessagingFragment messagingFragment = new MessagingFragment();
    VoiceMessageFragment voiceMessageFragment = new VoiceMessageFragment();
    BluetoothFragment bluetoothFragment = new BluetoothFragment();
    MyProfileFragment myProfileFragment = new MyProfileFragment();
    SettingsFragment settingsFragment = new SettingsFragment();

    private static final int REQUEST_PAYMENT = 101;
    private static final int REQUEST_PAYMENT_CANCEL = 102;

    private Toolbar toolbar;
    private CustomTabLayout tabLayout;
    public CustomViewPager viewPager;

    LinearLayout linearLayoutUpgradeBar;

    Dialog dialog;
    ImageView driveMode, manageblock;
    BroadcastHelper broadcastHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(0, 0);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);

        Utility.getSharedPreference(getApplicationContext()).edit().
                putBoolean(ConstantCodes.IS_DRIVING_MODE_OPEN,false).commit();

        if (getIntent() != null && getIntent().getBooleanExtra(ConstantCodes.INTENT_AFTER_LOGIN, false)) {
            Intent intent = new Intent(this, SyncToServerService.class);
            startService(intent);
        }

//        Intent intentBroadcast = new Intent(ConstantCodes.BROADCAST_DND);
        broadcastHelper = new BroadcastHelper(this, ConstantCodes.BROADCAST_DND);
        broadcastHelper.register(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                updateDNDIcon();
            }
        });

        manageblock = (ImageView) findViewById(R.id.manageblock);

        manageblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeScreen.this, BlockModeDialog.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        createDefaultVoiceMessageGroup();
        updateDNDIcon();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        driveMode = (ImageView) findViewById(R.id.driveMode);

        linearLayoutUpgradeBar = (LinearLayout) findViewById(R.id.linear_upgrade_bar);

        ColorDrawable[] color = {new ColorDrawable(Color.BLUE), new ColorDrawable(Color.RED)};
        TransitionDrawable trans = new TransitionDrawable(color);
        //This will work also on old devices. The latest API says you have to use setBackground instead.
        linearLayoutUpgradeBar.setBackgroundDrawable(trans);
        trans.startTransition(5000);

//        linearLayoutUpgradeBar.setBackgroundColor(Color.argb(0xFF, i, i, 200));

        driveMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(HomeScreen.this, DriveModeDialog.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        viewPager = (CustomViewPager) findViewById(R.id.viewpager);
        viewPager.setPagingEnabled(false);
//        viewPager.setOffscreenPageLimit(5);
        setupCustomViewPager(viewPager);

        tabLayout = (CustomTabLayout) findViewById(R.id.tabs);
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.blue));
        tabLayout.setTabTextColors(getResources().getColor(R.color.another_grey), getResources().getColor(R.color.white));
        tabLayout.setSelectedTabIndicatorHeight(8);
        //  changeTabsFont();
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.getPosition() == 7){
                    checkBTStatus();
                }else if(tab.getPosition() == 1){
                   if(blacklistFragment != null){
                       blacklistFragment.onResume();
                   }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        findViewById(R.id.upgrade_banner_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (handler != null) {
                    handler.removeCallbacks(runnable);
                }
                linearLayoutUpgradeBar.setVisibility(View.GONE);
            }
        });


        loadProfile();
        refresh();
    }

    private void createDefaultVoiceMessageGroup() {
        //Read previous value from database
        if (new Select().from(VoiceMessageModel.class).where("voiceMessageId='" + ConstantCodes.FREE_VOICE_MAIL_TITLE + "'").execute().size() <= 0) {
            VoiceMessageModel voiceMessageModel = new VoiceMessageModel();
            voiceMessageModel.voiceMessageId = ConstantCodes.FREE_VOICE_MAIL_TITLE;
            voiceMessageModel.title = ConstantCodes.DEFAULT_VOICEMESSAGE;
            voiceMessageModel.message = getString(R.string.message_default_voice_mail);
            voiceMessageModel.save();
        }

        if (new Select().from(VoiceMessageModel.class).where("voiceMessageId='" + ConstantCodes.FREE_VOICE_MAIL_TITLE_FOR_DRIVING + "'").execute().size() <= 0) {
            VoiceMessageModel voiceMessageModel = new VoiceMessageModel();
            voiceMessageModel.voiceMessageId = ConstantCodes.FREE_VOICE_MAIL_TITLE_FOR_DRIVING;
            voiceMessageModel.title = ConstantCodes.DEFAULT_VOICEMESSAGE_DRIVING;
            voiceMessageModel.message = getString(R.string.message_default_voice_mail_driving);
            voiceMessageModel.save();
        }
    }

    AlertDialog alert;

    @Override
    protected void onResume() {
        super.onResume();
        //Checking for default sms app
        // Otherwise... check if we're not the default SMS app
//        showDefaultSMSDialog();
    }

    private boolean hasSetupMms() {
        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (TextUtils.isEmpty(mPrefs.getString(com.moez.QKSMS.ui.settings.SettingsFragment.MMSC_URL, ""))
                && TextUtils.isEmpty(mPrefs.getString(com.moez.QKSMS.ui.settings.SettingsFragment.MMS_PROXY, ""))
                && TextUtils.isEmpty(mPrefs.getString(com.moez.QKSMS.ui.settings.SettingsFragment.MMS_PORT, ""))) {

            Intent intent = new Intent(HomeScreen.this, MMSSetupActivity.class);
            startActivity(intent);
            overridePendingTransition(0, 0);
            return false;
        }

        return true;
    }

    private void showDefaultSMSDialog() {
        boolean isDefaultSmsApp = Utils.isDefaultSmsApp(HomeScreen.this);
        if (isDefaultSmsApp) {
            hasSetupMms();
            return;
        }

        if (alert == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(HomeScreen.this);

            //  builder.setTitle("Confirm");
            builder.setMessage(getString(R.string.not_default_sms_app));

            builder.setPositiveButton("Give Permission", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
                    intent.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, getPackageName());
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                }
            });
            alert = builder.create();
            alert.setCancelable(false);
            alert.setCanceledOnTouchOutside(false);
        }
        if (alert.isShowing() == false)
            alert.show();
    }

    private void refresh() {
        if (Utility.isPaidApplication(HomeScreen.this)) {
            linearLayoutUpgradeBar.clearAnimation();
            linearLayoutUpgradeBar.setVisibility(View.GONE);
        } else {
            linearLayoutUpgradeBar.setVisibility(View.VISIBLE);
            startAnimation();

            linearLayoutUpgradeBar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    upgradeAccountUsingPaypal();
                }
            });
        }
    }

    ProfileController profileController;

    private void loadProfile() {

        profileController = new ProfileController(this, new ProfileController.ProfileCallback() {
            @Override
            public void onSuccess(User user) {

                Utility.saveLoggedInUser(HomeScreen.this, user);
                refresh();
                /*if (user.token_varified == 1) {
                    if ("failed".equalsIgnoreCase(user.ePaymentStatus)) {
                        Intent intent = new Intent(HomeScreen.this, PaymentInfoActivity.class);
                        intent.putExtra(ConstantCodes.DATA, Utility.getJsonString(user));
                        startActivity(intent);
                    } else {
                        Utility.saveLoggedInUser(HomeScreen.this, user);
                        Intent intent = new Intent(HomeScreen.this, HomeScreen.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    Intent intent = new Intent(HomeScreen.this, EnterCodeActivity.class);
                    intent.putExtra(ConstantCodes.DATA, Utility.getJsonString(user));
                    startActivity(intent);
                }*/
            }

            @Override
            public void onFail(String message) {

            }
        });
        User user = Utility.getLoggedInUser(this);
        profileController.login(user.iUserID);
    }

    int counter = 0;
    ColorDrawable[] color = {new ColorDrawable(Color.GREEN), new ColorDrawable(Color.RED), new ColorDrawable(Color.BLUE)};
    ColorDrawable[] colorReverse = {new ColorDrawable(Color.RED), new ColorDrawable(Color.GREEN), new ColorDrawable(Color.BLUE)};
    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            startAnimation();
        }
    };

    TransitionDrawable trans;

    private void startAnimation() {
        trans = new TransitionDrawable(counter % 2 == 0 ? color : colorReverse);
        //This will work also on old devices. The latest API says you have to use setBackground instead.
        linearLayoutUpgradeBar.setBackgroundDrawable(trans);
        trans.startTransition(2500);
        handler.postDelayed(runnable, 2500);
        counter++;
    }

    public void cancelAccountUsingPaypal() {
        User user = Utility.getLoggedInUser(this);
        Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtra(ConstantCodes.INTENT_ID, user.iUserID);
        intent.putExtra(ConstantCodes.INTENT_FROM, ConstantCodes.PAYMENT_CANCEL);
        startActivityForResult(intent, REQUEST_PAYMENT_CANCEL);
    }

    //called from settingsfragment screen
    public void upgradeAccountUsingPaypal() {
        User user = Utility.getLoggedInUser(this);
        if (user != null) {
            Intent intent = new Intent(this, WebViewActivity.class);
            intent.putExtra(ConstantCodes.INTENT_ID, user.iUserID);
            intent.putExtra(ConstantCodes.INTENT_FROM, ConstantCodes.PAYMENT_PROCESS);
            startActivityForResult(intent, REQUEST_PAYMENT);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_PAYMENT && resultCode == RESULT_OK) {
            Toast.makeText(this, "Thanks for payment confirmation", Toast.LENGTH_SHORT).show();
            //Saving inforamtion in preference
            User user = Utility.getLoggedInUser(this);
            user.auto_renewel = 1;
            user.setIsPaidApplication(true);
            Utility.saveLoggedInUser(this, user);
            refresh();
            BroadcastHelper.sendBroadcast(HomeScreen.this, ConstantCodes.BROADCAST_PAYMENT_REFRESH);
        } else if (requestCode == REQUEST_PAYMENT && (resultCode == RESULT_CANCELED || resultCode == RESULT_FIRST_USER)) {
            Toast.makeText(this, "Failed, Please Try again", Toast.LENGTH_SHORT).show();
        } else if (requestCode == REQUEST_PAYMENT_CANCEL && resultCode == RESULT_OK) {
            Toast.makeText(this, "Your Auto renewal plan is removed.", Toast.LENGTH_SHORT).show();
            User user = Utility.getLoggedInUser(HomeScreen.this);
            user.auto_renewel = 0;
            Utility.saveLoggedInUser(HomeScreen.this, user);
            refresh();
            BroadcastHelper.sendBroadcast(HomeScreen.this, ConstantCodes.BROADCAST_PAYMENT_REFRESH);
        } else if (requestCode == REQUEST_PAYMENT_CANCEL && (resultCode == RESULT_CANCELED || resultCode == RESULT_FIRST_USER)) {
            Toast.makeText(this, "Failed, Please Try again", Toast.LENGTH_SHORT).show();
        }else if (requestCode == 100) {
            Toast.makeText(this, "Failed, Please Try again", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateDNDIcon() {
        if (Utility.getSharedPreference(this).contains(ConstantCodes.DND_TYPE)) {
            manageblock.setImageResource(R.drawable.main_dnd_active);
        } else {
            manageblock.setImageResource(R.drawable.main_dnd);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (handler != null)
            handler.removeCallbacks(runnable);
        if (broadcastHelper != null) {
            broadcastHelper.unRegister();
        }
    }

    private void setupCustomViewPager(CustomViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setPagingEnabled(true);
        adapter.addFrag(homeFragment, "HOME");
        adapter.addFrag(blacklistFragment, "BLACKLIST");
        adapter.addFrag(whitelistFragment, "WHITELIST");
        adapter.addFrag(activityFragment, "ACTIVITY");
        adapter.addFrag(contactsFragment, "CONTACTS");
        adapter.addFrag(messagingFragment, "MESSAGING");

        //Checking if logged in user is free user then show voice message tab.
//        if (Utility.isPaidApplication(this))
        adapter.addFrag(voiceMessageFragment, "VOICE MESSAGE");
        adapter.addFrag(bluetoothFragment, "BLUETOOTH");
        adapter.addFrag(myProfileFragment, "MY PROFILE");
        adapter.addFrag(settingsFragment, "SETTINGS");
        viewPager.setAdapter(adapter);

    }


    class ViewPagerAdapter extends FragmentStatePagerAdapter{
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }


        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {

            return mFragmentTitleList.get(position);
        }


    }
    /*Check for blue tooth*/
    android.bluetooth.BluetoothAdapter mBluetoothAdapter;
    protected void checkBTStatus(){
        mBluetoothAdapter = android.bluetooth.BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            alertBTNoSupport();
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                alertBTEnable();
            }else{
                startService(new Intent(this, BluetoothService.class));
            }
        }
    }

    private void alertBTNoSupport(){
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeScreen.this);

        //  builder.setTitle("Confirm");
        builder.setMessage("Sorry,Bluetooth not support with device!");


        builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });



        AlertDialog alert = builder.create();
        alert.show();
    }
    private void alertBTEnable(){
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeScreen.this);

        //  builder.setTitle("Confirm");
        builder.setMessage("Do you want to enable Bluetooth?");


        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
// Bluetooth is not enable :)
                startService(new Intent(HomeScreen.this, BluetoothService.class));
                mBluetoothAdapter.enable();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }
}
