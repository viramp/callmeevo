package callme.evo.activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.activeandroid.query.Update;
import com.andraskindler.quickscroll.QuickScroll;
import com.andraskindler.quickscroll.Scrollable;
import com.github.siyamed.shapeimageview.CircularImageView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import callme.evo.R;
import callme.evo.adapter.IndexAdapter;
import callme.evo.global.BroadcastHelper;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.Contacts;
import callme.evo.models.VoiceMessageModel;
import customtextviews.MyTextViewBook;

/**
 * Created by sahil desai on 6/21/2016.
 */
public class VoiceMailDetailActivity extends Activity {

    private ListView mListView;
    private CustomAdapter mAdapter;

    MyTextViewBook priority, voicemail;

    String voiceMessageId;
    private ProgressBar progressBar;
    private TextView txtError;
    String title = "", message = "";
    TextView textTitle, textMessage;

    private GestureDetector mGestureDetector;
    private List<String> alphabet = new ArrayList<String>();
    private HashMap<String, Integer> sections = new HashMap<String, Integer>();
    private int sideIndexHeight;
    private static float sideIndexX;
    private static float sideIndexY;
    private int indexListSize;


    IndexAdapter indexAdapter;
    ListView sideIndex;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_mail_detail);

        findViewById(R.id.imageOpenDrawer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        txtError = (TextView) findViewById(R.id.txt_error);

        sideIndex = (ListView) findViewById(R.id.layout_alphabats);
        sideIndex.setFastScrollEnabled(true);

        mListView = (ListView) findViewById(R.id.listView);
        mAdapter = new CustomAdapter(this);
        mListView.setAdapter(mAdapter);



        indexAdapter = new IndexAdapter(this);
        sideIndex.setAdapter(indexAdapter);




        final QuickScroll fastTrack = QuickScroll.class.cast(findViewById(R.id.quickscroll));
        fastTrack.init(QuickScroll.TYPE_POPUP, mListView, mAdapter, QuickScroll.STYLE_NONE);
        fastTrack.setFixedSize(2);
        fastTrack.setPopupColor(ContextCompat.getColor(this, R.color.dark_grey),
                QuickScroll.BLUE_LIGHT_SEMITRANSPARENT, 1, Color.WHITE, 1);
        fastTrack.setVisibility(View.VISIBLE);

//        createAlphabetTrack();

//        final QuickScroll quickscroll = (QuickScroll) findViewById(R.id.quickscroll);
//        quickscroll.init(QuickScroll.TYPE_INDICATOR, mListView, mAdapter, QuickScroll.STYLE_HOLO);
//        quickscroll.setVisibility(View.VISIBLE);

        textTitle = (TextView) findViewById(R.id.text_title);
        textMessage = (TextView) findViewById(R.id.text_message);


        if (getIntent().hasExtra(ConstantCodes.INTENT_ID)) {
            voiceMessageId = getIntent().getStringExtra(ConstantCodes.INTENT_ID);
            title = getIntent().getStringExtra(ConstantCodes.INTENT_TITLE);
            message = getIntent().getStringExtra(ConstantCodes.INTENT_MESSAGE);
            textTitle.setText(title);
            textMessage.setText(message);
            if (ConstantCodes.FREE_VOICE_MAIL_TITLE.equalsIgnoreCase(voiceMessageId)) {
                textTitle.setText(ConstantCodes.DEFAULT_VOICEMESSAGE);
                findViewById(R.id.layout_contact).setVisibility(View.GONE);
//                findViewById(R.id.overlay).setClickable(true);
            } else if (ConstantCodes.FREE_VOICE_MAIL_TITLE_FOR_DRIVING.equalsIgnoreCase(voiceMessageId)) {
                textTitle.setText(ConstantCodes.DEFAULT_VOICEMESSAGE_DRIVING);
                findViewById(R.id.layout_contact).setVisibility(View.GONE);
//                findViewById(R.id.overlay).setClickable(true);
            } else {
                findViewById(R.id.layout_contact).setVisibility(View.VISIBLE);
            }
            loadRelatedContacts();
        } else {
            finish();
        }

        findViewById(R.id.tvEdit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConstantCodes.FREE_VOICE_MAIL_TITLE.equalsIgnoreCase(voiceMessageId)) {
                    showAddEditMessageDialog(voiceMessageId, title, textMessage.getText().toString());
                } else if (ConstantCodes.FREE_VOICE_MAIL_TITLE_FOR_DRIVING.equalsIgnoreCase(voiceMessageId)) {
                    showAddEditMessageDialog(voiceMessageId, title, textMessage.getText().toString());
                } else {
                    showAddEditMessageDialog(voiceMessageId, textTitle.getText().toString(), textMessage.getText().toString());
                }
            }
        });
    }

    private ViewGroup createAlphabetTrack() {

//        final LinearLayout layout = new LinearLayout(getActivity());
        final LinearLayout layout = (LinearLayout) findViewById(R.id.layout_alphabats);
        layout.setVisibility(View.VISIBLE);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int) (30 * getResources().getDisplayMetrics().density), RelativeLayout.LayoutParams.MATCH_PARENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        layout.setLayoutParams(params);
        layout.setOrientation(LinearLayout.VERTICAL);

        final LinearLayout.LayoutParams textparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        textparams.weight = 1;
        final int height = getResources().getDisplayMetrics().heightPixels;
        int iterate = 0;
        if (height >= 1024) {
            iterate = 1;
            layout.setWeightSum(26);
        } else {
            iterate = 2;
            layout.setWeightSum(13);
        }
        for (char character = 'a'; character <= 'z'; character += iterate) {
            final TextView textview = new TextView(this);
            textview.setLayoutParams(textparams);
            textview.setGravity(Gravity.CENTER_HORIZONTAL);
            textview.setText(Character.toString(character));
            layout.addView(textview);
        }

        return layout;
    }

    private void showAddEditMessageDialog(String voicemailId, String title, String message) {
        final View view = LayoutInflater.from(VoiceMailDetailActivity.this).inflate(R.layout.dialog_create_voice_msg, null, false);
        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(VoiceMailDetailActivity.this);
        alertDialogBuilder.setView(view);
        final android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        ((EditText) view.findViewById(R.id.edt_title)).setText(title);
        ((EditText) view.findViewById(R.id.edt_message)).setText(message);
        if (ConstantCodes.FREE_VOICE_MAIL_TITLE.equalsIgnoreCase(voicemailId) || ConstantCodes.FREE_VOICE_MAIL_TITLE_FOR_DRIVING.equalsIgnoreCase(voicemailId)) {
//            ((EditText) view.findViewById(R.id.edt_title)).setText(ConstantCodes.DEFAULT_VOICEMESSAGE);
            ((EditText) view.findViewById(R.id.edt_title)).setEnabled(false);
        } else {
            ((EditText) view.findViewById(R.id.edt_title)).setEnabled(true);
        }


        view.findViewById(R.id.text_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                VoiceMessageModel voiceMessageModel = new VoiceMessageModel();
                voiceMessageModel.voiceMessageId = voicemailId;
                voiceMessageModel.title = ((EditText) view.findViewById(R.id.edt_title)).getText().toString();
                if (ConstantCodes.FREE_VOICE_MAIL_TITLE.equalsIgnoreCase(voicemailId)) {
                    voiceMessageModel.title = ConstantCodes.DEFAULT_VOICEMESSAGE;
                } else if (ConstantCodes.FREE_VOICE_MAIL_TITLE_FOR_DRIVING.equalsIgnoreCase(voicemailId)) {
                    voiceMessageModel.title = ConstantCodes.DEFAULT_VOICEMESSAGE_DRIVING;
                }
                voiceMessageModel.message = ((EditText) view.findViewById(R.id.edt_message)).getText().toString();
//                voiceMessageModel.update(new String[]{"title","message"});

                new Update(VoiceMessageModel.class).set("title='" + voiceMessageModel.title + "', message='" + voiceMessageModel.message + "'").where("voiceMessageId='" + voicemailId + "'").execute();
                BroadcastHelper.sendBroadcast(VoiceMailDetailActivity.this, ConstantCodes.BROADCAST_CONTACT_REFRESH);

                textTitle.setText(voiceMessageModel.title);
                if (ConstantCodes.FREE_VOICE_MAIL_TITLE.equalsIgnoreCase(voicemailId)) {
                    textTitle.setText(ConstantCodes.DEFAULT_VOICEMESSAGE);
                } else if (ConstantCodes.FREE_VOICE_MAIL_TITLE_FOR_DRIVING.equalsIgnoreCase(voicemailId)) {
                    textTitle.setText(ConstantCodes.DEFAULT_VOICEMESSAGE_DRIVING);
                }

                textMessage.setText(voiceMessageModel.message);
                alertDialog.dismiss();
            }
        });
    }

    private void loadRelatedContacts() {
        new AsyncTask<Void, Void, ArrayList<Contacts>>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected ArrayList<Contacts> doInBackground(Void... params) {

                Log.i("CONTACT", "READING VOICE MESSAGE");

                ArrayList<Contacts> list = new Select().from(Contacts.class).where("voiceMessageId='" + voiceMessageId + "' OR voiceMessageId='' OR voiceMessageId IS NULL").orderBy("voiceMessageId DESC,name COLLATE NOCASE ASC").execute();
                Log.i("CONTACT", "READING VOICE MESSAGE size" + list.size());

                return list;
            }

            @Override
            protected void onPostExecute(ArrayList<Contacts> list) {
                super.onPostExecute(list);

                progressBar.setVisibility(View.GONE);

                Log.i("CONTACT", "READING CONTACT size" + list.size());

                if (list != null && list.size() > 0) {

                    mAdapter.updateItem(list);

                    mListView.setVisibility(View.VISIBLE);

                    txtError.setVisibility(View.GONE);

                    bindIndex(list);

                } else {

                    txtError.setText("There are no numbers in contacts");

                    txtError.setVisibility(View.VISIBLE);

                    mListView.setVisibility(View.GONE);

                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public class CustomAdapter extends BaseAdapter  implements Scrollable{

        Context context;
        private ArrayList<Contacts> list;

        public CustomAdapter(Context context) {
            this.context = context;
            this.list = new ArrayList<>();
        }

        public CustomAdapter(Context context, ArrayList<Contacts> list) {

            this.context = context;
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        public void updateItem(ArrayList<Contacts> list) {
            this.list = list;
            notifyDataSetChanged();
        }

        public void removeItem(int position) {
            if (list.size() > position) {
                list.remove(position);
                notifyDataSetInvalidated();
            }
        }

        @Override
        public Contacts getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            final ViewHolder holder;
            if (convertView == null) {
                convertView = View.inflate(context, R.layout.list_item_contact_own, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            final Contacts model = getItem(position);

            holder.txtName.setText(model.name);
            holder.txtNumber.setText(model.number);

            if (!TextUtils.isEmpty(model.thumbnailImage)) {
//                Glide.with(getActivity()).load(Uri.parse(getItem(position).thumbnailImage)).into(holder.imgProfile);
                Utility.showCircularImageView(context, holder.imgProfile, Uri.parse(model.thumbnailImage));
            } else {
                holder.imgProfile.setImageResource(R.drawable.ph_contact);
            }

            holder.checkbox.setVisibility(View.VISIBLE);
            holder.checkbox.setChecked(!TextUtils.isEmpty(model.voiceMessageId));
            holder.checkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CheckBox checkBox = (CheckBox) v;
                    if (checkBox.isChecked()) {
                        Toast.makeText(context, "Checked true", Toast.LENGTH_SHORT).show();
                        model.voiceMessageId = voiceMessageId;
                        getItem(position).voiceMessageId = voiceMessageId;
                    } else {
                        model.voiceMessageId = null;
                        getItem(position).voiceMessageId = "";
                    }
                    model.update(new String[]{"voiceMessageId"});
                }
            });

            return convertView;
        }



        @Override
        public String getIndicatorForPosition(int childposition, int groupposition) {
            return Character.toString(list.get(childposition).name.charAt(0));
        }

        @Override
        public int getScrollPosition(int childposition, int groupposition) {
            return childposition;
        }

        class ViewHolder {
            public CircularImageView imgProfile;
            public TextView txtName, txtNumber;
            public CheckBox checkbox;

            public ViewHolder(View view) {
                imgProfile = (CircularImageView) view.findViewById(R.id.profileImage);
                txtName = (TextView) view.findViewById(R.id.txt_name);
                txtNumber = (TextView) view.findViewById(R.id.txt_number);
                checkbox = (CheckBox) view.findViewById(R.id.checkbox);
            }
        }
    }


    /*NEw added */
    private void bindIndex(ArrayList<Contacts> list){
//        List<Row> rows = new ArrayList<Row>();
        int start = 0;
        int end = 0;
        String previousLetter = null;
        Pattern numberPattern = Pattern.compile("[0-9]");
        Pattern specialPattern = Pattern.compile("^[a-zA-Z0-9]*$");
        sections = new HashMap<>();
        alphabet = new ArrayList<>();
        alphabet.clear();
        for (int i =0; i < list.size(); i++) {
            if( list.get(i).name == null){
                Contacts contacts = list.get(i);
                contacts.name = list.get(i).number;
            }else if(list.get(i).equals("")){
                Contacts contacts = list.get(i);
                contacts.name = list.get(i).number;
            }
            String firstLetter = list.get(i).name.substring(0, 1);

            // Group numbers together in the scroller
            if (!specialPattern.matcher(firstLetter).matches()) {
                Log.e("specialPattern","specialPattern"+firstLetter);
                firstLetter = "..";
            }
            if (numberPattern.matcher(firstLetter).matches()) {
                firstLetter = "#";
            }


            // If we've changed to a new letter, add the previous letter to the alphabet scroller
            if (previousLetter != null && !firstLetter.equalsIgnoreCase(previousLetter)) {
//                end = rows.size() - 1;
                String tmpIndexItem = previousLetter.toUpperCase(Locale.UK);
                alphabet.add(tmpIndexItem);

                start = end + 1;
            }

            // Check if we need to add a header row
            if (!firstLetter.equalsIgnoreCase(previousLetter)) {
//                rows.add(new Section(firstLetter));
                sections.put(firstLetter.toString().toUpperCase(), i);
            }

            // Add the country to the list
//            rows.add(new Item(country));
            previousLetter = firstLetter;
        }

        if (previousLetter != null) {
            // Save the last letter
           String tmpIndexItem = previousLetter.toUpperCase(Locale.UK);
            alphabet.add(tmpIndexItem);
        }

        HashSet<String> objects = new HashSet<>();
        objects.addAll(alphabet);
        alphabet = new ArrayList<>();
        alphabet.clear();
        alphabet.addAll(objects);
        Collections.sort(alphabet);


        indexAdapter.updateItem((ArrayList<String>) alphabet);
        sections = sortByValues(sections);

        updateList();
    }
    public void updateList() {
        sideIndex.setVisibility(View.VISIBLE);

        sideIndex.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                displayListItem(position);
            }
        });


//        sideIndex.removeAllViews();
        indexListSize = alphabet.size();
        if (indexListSize < 1) {
            return;
        }
    }

    public void displayListItem(int position) {

        sideIndex.setVisibility(View.VISIBLE);
        sideIndexHeight = sideIndex.getHeight();
        // compute number of pixels for every side index item
        double pixelPerIndexItem = (double) sideIndexHeight / indexListSize;

        // compute the item index for given event position belongs to
        int itemPosition = (int) (sideIndexY / pixelPerIndexItem);

        // get the item (we can do it since we know item index)
//        if (itemPosition < alphabet.size()) {
            String indexItem = alphabet.get(position);
            int subitemPosition = sections.get(indexItem);

            //ListView listView = (ListView) findViewById(android.R.id.list);
            mListView.setSelection(subitemPosition);
//        }
    }
    private static HashMap sortByValues(HashMap map) {
        List list = new LinkedList(map.entrySet());
        // Defined Custom Comparator here
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o1)).getValue())
                        .compareTo(((Map.Entry) (o2)).getValue());
            }
        });

        // Here I am copying the sorted list in HashMap
        // using LinkedHashMap to preserve the insertion order
        HashMap sortedHashMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedHashMap.put(entry.getKey(), entry.getValue());
        }
        return sortedHashMap;
    }
}
