package callme.evo.activity;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import callme.evo.R;
import callme.evo.controller.RegisterController;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.User;
import customtextviews.MyTextViewMedium;

/**
 * Created by sahil desai on 6/1/2016.
 */
public class SIgnUpActivity extends Activity {

    MyTextViewMedium tvSignin, tvSignUp;
    RegisterController registerController;
    ProgressDialog progressDialog;

    EditText edtFirstName;
    EditText edtLastName;
    EditText edtUserName;
    EditText edtVerifyPassword;
    EditText edtRefferal;
    EditText edtEmail;
    EditText edtPassword;
    EditText edtPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(0,0);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        edtUserName = (EditText) findViewById(R.id.edt_username);
        edtEmail = (EditText) findViewById(R.id.edt_email);
        edtPassword = (EditText) findViewById(R.id.edt_password);
        edtVerifyPassword = (EditText) findViewById(R.id.edt_verify_password);
        edtPhone = (EditText) findViewById(R.id.edt_phone);
        edtRefferal = (EditText) findViewById(R.id.edt_referral_code);


        tvSignin = (MyTextViewMedium) findViewById(R.id.tvSignin);
        tvSignUp = (MyTextViewMedium) findViewById(R.id.tvSignUp);

        tvSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tvSignUp.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            progressDialog = Utility.progressDialog(SIgnUpActivity.this, "Please wait...");
                                            registerController.register(
                                                    edtUserName.getText().toString().trim(),
                                                    edtEmail.getText().toString().trim(),
                                                    edtPassword.getText().toString().trim(),
                                                    edtVerifyPassword.getText().toString().trim(),
                                                    edtPhone.getText().toString().trim(),
                                                    edtRefferal.getText().toString().trim());
                                        }
                                    }
        );

        registerController = new RegisterController(this, new RegisterController.RegisterCallback() {
            @Override
            public void onSuccess(User user) {
                progressDialog.dismiss();
                Intent intent = new Intent(SIgnUpActivity.this, EnterCodeActivity.class);
                intent.putExtra(ConstantCodes.DATA, Utility.getJsonString(user));
                startActivity(intent);
                overridePendingTransition(0,0);
            }

            @Override
            public void onFail(String message) {
                progressDialog.dismiss();
                if (Utility.isInternetUnavailableMsg(message)) {
                    Toast.makeText(SIgnUpActivity.this, getString(R.string.msg_internet_unavailable), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(SIgnUpActivity.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        }
        );

    }
}
