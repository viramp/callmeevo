package callme.evo.activity;

import android.os.Bundle;
import android.view.View;

import com.moez.QKSMS.ui.base.QKActivity;
import com.moez.QKSMS.ui.dialog.mms.MMSSetupFragment;

import callme.evo.R;

/**
 * Created by pankaj on 20/12/16.
 */
public class MMSSetupActivity extends QKActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mms_setup);
        findViewById(R.id.toolbar).setVisibility(View.GONE);

        // Not so fast! You need to set up MMS first.
        MMSSetupFragment f = new MMSSetupFragment();
        Bundle args = new Bundle();
        args.putBoolean(MMSSetupFragment.ARG_ASK_FIRST, true);
        f.setArguments(args);

        getFragmentManager()
                .beginTransaction()
                .add(f, MMSSetupFragment.TAG)
                .commit();
    }
}
