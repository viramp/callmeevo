package callme.evo.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CallLog;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

import com.activeandroid.query.Select;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import callme.evo.R;
import callme.evo.SyncContactService;
import callme.evo.fragments.intro.Intro_Five;
import callme.evo.fragments.intro.Intro_Four;
import callme.evo.fragments.intro.Intro_One;
import callme.evo.fragments.intro.Intro_Three;
import callme.evo.fragments.intro.Intro_Two;
import callme.evo.global.ConstantCodes;
import callme.evo.global.EasyPermissions;
import callme.evo.global.Utility;
import callme.evo.models.ContactCallLog;
import callme.evo.models.Contacts;
import commonclasses.CircleIndicator.CircleIndicator;
import customtextviews.MyTextViewLight;
import customtextviews.MyTextViewMedium;

/**
 * Created by sahil desai on 6/1/2016.
 */
public class IntroActivity extends FragmentActivity {

    ViewPager viewPager;
    CircleIndicator circleIndicator;
    MyTextViewMedium tvSkip, tvNext;
    MyTextViewLight tvswipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(0,0);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        requestPermission();


//        int i=3/0;
/*        long number = 8460479175l;
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();

        try {
            // phone must begin with '+'
            Phonenumber.PhoneNumber n=new Phonenumber.PhoneNumber();
            n.setNationalNumber(918460479175l);
            Phonenumber.PhoneNumber n2=new Phonenumber.PhoneNumber();
            n2.setNationalNumber(8460479175l);
            ((TelephonyManager)this.getSystemService(this.TELEPHONY_SERVICE)).getSimCountryIso();
            phoneUtil.format(n2, PhoneNumberUtil.PhoneNumberFormat.E164);
            phoneUtil.isNumberMatch(n, n2);
        } catch (Exception e) {
            System.err.println("NumberParseException was thrown: " + e.toString());
        }*/

        if (Utility.getLoggedInUser(this) != null) {

            Intent intent = new Intent(IntroActivity.this, HomeScreen.class);
            intent.putExtra(ConstantCodes.INTENT_AFTER_LOGIN, true);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            overridePendingTransition(0,0);
            return;
        } else if (Utility.isSecondTime(this)) {
            Intent intent = new Intent(IntroActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(0,0);
            return;
        }


        viewPager = (ViewPager) findViewById(R.id.viewPager);
        circleIndicator = (CircleIndicator) findViewById(R.id.circleIndicator);

        tvSkip = (MyTextViewMedium) findViewById(R.id.tvSkip);
        tvNext = (MyTextViewMedium) findViewById(R.id.tvNext);
        tvswipe = (MyTextViewLight) findViewById(R.id.tvswipe);

        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(IntroActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(0,0);
            }
        });

        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewPager.getCurrentItem() == 4) {
                    Intent intent = new Intent(IntroActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(0,0);
                } else {
                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                }

            }
        });

        setupViewPager(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (viewPager.getCurrentItem() == 4) {
                    tvSkip.setVisibility(View.GONE);
                    tvNext.setText(getResources().getString(R.string.letsgetstarted));
                } else {
                    tvSkip.setVisibility(View.VISIBLE);
                    tvNext.setText(getResources().getString(R.string.next));
                }

                if (viewPager.getCurrentItem() == 0) {
                    tvswipe.setVisibility(View.VISIBLE);
                } else {
                    tvswipe.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageSelected(int position) {
                if (viewPager.getCurrentItem() == 4) {
                    tvSkip.setVisibility(View.GONE);
                    tvNext.setText(getResources().getString(R.string.letsgetstarted));
                } else {
                    tvSkip.setVisibility(View.VISIBLE);
                    tvNext.setText(getResources().getString(R.string.next));
                }

                if (viewPager.getCurrentItem() == 0) {
                    tvswipe.setVisibility(View.VISIBLE);
                } else {
                    tvswipe.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 106) {
            EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void requestPermission() {
        EasyPermissions.requestPermissions(this, permissionCallback, null, 106, Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS, Manifest.permission.READ_CALL_LOG, Manifest.permission.WRITE_CALL_LOG, Manifest.permission.READ_SMS);
    }

    EasyPermissions.PermissionCallbacks permissionCallback = new EasyPermissions.PermissionCallbacks() {
        @Override
        public void onPermissionsGranted(int requestCode, List<String> perms) {

            Intent intent = new Intent(IntroActivity.this, SyncContactService.class);
            startService(intent);
            /*syncCallLog();
            syncContacts();
            syncSMS();*/
        }

        @Override
        public void onPermissionsDenied(int requestCode, List<String> perms) {
            AlertDialog.Builder builder = new AlertDialog.Builder(IntroActivity.this);

            //  builder.setTitle("Confirm");
            builder.setMessage("We need permissions (READ_CONTACTS, READ_SMS, READ_CALL_LOGS) to continue?");


            builder.setPositiveButton("Give Permission", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    alert.dismiss();
                    requestPermission();
                }
            });

            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // Do nothing
                    dialog.dismiss();
                    finish();
                }
            });

            alert = builder.create();
            alert.show();
        }

        @Override
        public void onPermissionsPermanentlyDeclined(int requestCode, List<String> perms) {
            Log.i("TAG", "on permissionpermanatlydeclined");
        }
    };

    AlertDialog alert;

    private void syncCallLog() {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {

                long contactCallLogLast = Utility.getSharedPreference(IntroActivity.this).getLong(ConstantCodes.CONTACT_CALLLOG_LAST, 0);

                Cursor managedCursor = managedQuery(CallLog.Calls.CONTENT_URI, null, CallLog.Calls.DATE + ">" + contactCallLogLast, null, CallLog.Calls.DATE + " DESC");
                int name = managedCursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
                int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
                int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
                int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
                int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
//        sb.append("Call Details :");

                int i = 0;
                if (managedCursor.moveToFirst()) {
                    do {

                        if (i == 0) {
                            try {
                                Utility.getSharedPreference(IntroActivity.this).edit().putLong(ConstantCodes.CONTACT_CALLLOG_LAST, Long.parseLong(managedCursor.getString(date))).commit();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        i = 2;

                        String phName = managedCursor.getString(name);
                        String phNumber = managedCursor.getString(number);
                        String callType = managedCursor.getString(type);
                        String callDate = managedCursor.getString(date);
                        Date callDayTime = new Date(Long.valueOf(callDate));
                        String callDuration = managedCursor.getString(duration);
                        String dir = null;
                        int dircode = Integer.parseInt(callType);
                        switch (dircode) {
                            case CallLog.Calls.OUTGOING_TYPE:
                                dir = "OUTGOING";
                                break;

                            case CallLog.Calls.INCOMING_TYPE:
                                dir = "INCOMING";
                                break;

                            case CallLog.Calls.MISSED_TYPE:
                                dir = "MISSED";
                                break;
                        }

                        ContactCallLog contacts = new ContactCallLog();

                        contacts.name = phName;
                        contacts.number = phNumber;
                        contacts.timestamp = Long.valueOf(callDate);
                        //saving call log
                        contacts.save();

                    } while (managedCursor.moveToNext());
                }

                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void syncContacts() {
        new AsyncTask<Void, Void, ArrayList<Contacts>>() {
            @Override
            protected ArrayList<Contacts> doInBackground(Void... params) {

                ArrayList<Contacts> listC = new Select().from(Contacts.class).execute();

                ArrayList<Contacts> list = Utility.readContacts(IntroActivity.this);
                boolean isContinue = true;
                for (Contacts contacts : list) {
                    isContinue = true;
                    for (Contacts contacts1 : listC) {
                        if (Utility.getFinalNumber(contacts1.number).equalsIgnoreCase(Utility.getFinalNumber(contacts.number))) {
                            isContinue = false;
                            Log.e("TTTTT", contacts1.thumbnailImage + "=" + contacts.thumbnailImage);
                            if (contacts1.name.contains("Abdul Salam")) {
                                Log.e("TTTTT", contacts1.thumbnailImage + "=" + contacts.thumbnailImage);
                            }
                            if (contacts1.thumbnailImage != null && !contacts1.thumbnailImage.equalsIgnoreCase(contacts.thumbnailImage)) {
                                contacts.update(new String[]{"name", "thumbnailImage", "photoImage"});
                            } else if (contacts1.photoImage != null && !contacts1.photoImage.equalsIgnoreCase(contacts.photoImage)) {
                                contacts.update(new String[]{"name", "thumbnailImage", "photoImage"});
                            }

                            break;
                        }
                    }
                    if (isContinue) {
                        android.util.Log.d("READ_CONTACT", "save contact");
                        contacts.blackListOrWhiteList = ConstantCodes.WHITE_LIST;
                        contacts.save();
                    }
                }
                return list;
            }

            @Override
            protected void onPostExecute(ArrayList<Contacts> list) {
                super.onPostExecute(list);

                /*int groupSize = 10;
                int i = 0;

                JSONArray jsonArray = new JSONArray();
                for (Contacts contacts : list) {

                    if (i > groupSize) {
                        jsonArray = new JSONArray();
                        i = 0;
                        sendToServer(jsonArray.toString());
                    }

                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("name", contacts.name);
                        jsonObject.put("address", "");
                        jsonObject.put("phone_number", contacts.number);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    jsonArray.put(jsonObject);
                    i++;
                }*/

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void sendToServer(final String numbers) {
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, ConstantCodes.Web.SYNC_CONTACTS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.SYNC_CONTACTS);
                showLog("RESPONSE : " + response.toString());
                showLog("**********");
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.SYNC_CONTACTS);
                showLog("RESPONSE : " + (error.networkResponse == null ? (error != null ? error.getLocalizedMessage() : "NullVolleyError") : new String(error.networkResponse.data)));
                showLog("**********");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = Utility.getDefaultParams();
                params.put(ConstantCodes.data, numbers);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = Utility.getDefaultHeaders();
                return headers;
                /*Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token 04af7328b2346602ad7d581ff738eb25a5482cbf");
                params.put("Apikey", "JXn8e6C29jhZ065i8wyQktY33YD3s9zy");
                return params;*/
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        Utility.getRequestQueue().add(jsonObjReq);
    }

    private static void showLog(String message) {
        Log.i("ws", message + "");
    }

//    public void syncSMS() {
//
//        new AsyncTask<Void, Void, Void>() {
//
//            @Override
//            protected Void doInBackground(Void... params) {
//                Uri message = null;
//                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
//                    message = Telephony.MmsSms.CONTENT_CONVERSATIONS_URI;//Telephony.Sms.CONTENT_URI
//                } else {
//                    message = Uri.parse("content://mms-sms/conversations/"); //content://sms/
//                }
//
//                String[] columns = new String[]{Telephony.Sms._ID, Telephony.Sms.PERSON, Telephony.Sms.ADDRESS, Telephony.Sms.BODY, Telephony.Sms.DATE};
//
//                ContentResolver cr = getContentResolver();
//                Cursor c = cr.query(message, columns, null, null, Telephony.Sms.DATE + " DESC");
//
//                int idPos = c.getColumnIndex(Telephony.Sms._ID);
//                int namePos = c.getColumnIndex(Telephony.Sms.PERSON);
//                int addressPos = c.getColumnIndex(Telephony.Sms.ADDRESS);
//                int bodyPos = c.getColumnIndex(Telephony.Sms.BODY);
//                int datePos = c.getColumnIndex(Telephony.Sms.DATE);
////        startManagingCursor(c);
//
//                int totalSMS = c.getCount();
//
//                Sms objSms;
//                if (c.moveToFirst()) {
//
//                    for (int i = 0; i < totalSMS; i++) {
//
//                        objSms = new Sms();
//                        objSms.smsId = c.getLong(idPos);
//                        objSms.number = c.getString(addressPos);
//                        objSms.name = c.getString(namePos);
//                        objSms.message = c.getString(bodyPos);
//                        objSms.date = c.getLong(datePos);
//                        android.util.Log.d("READ_CONTACT", "save conversation+" + objSms.date);
//                        objSms.save();
//
////                        //saving sms in calllog for activity fragment
////                        ContactCallLog contactCallLog = new ContactCallLog();
////                        contactCallLog.number = Utility.getFinalNumber(c.getString(addressPos));
////                        contactCallLog.name = c.getString(namePos);
////                        contactCallLog.message = c.getString(bodyPos);
////                        contactCallLog.timestamp = c.getLong(datePos);
////                        contactCallLog.save();
//
//                        c.moveToNext();
//                    }
//                }
//                return null;
//            }
//        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//
////        ArrayList<Sms> lstSms = new ArrayList<Sms>();
////        Sms objSms = new Sms();
//
////      Uri message = Uri.parse("content://sms/");
//        Uri message = null;
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
//            message = Telephony.MmsSms.CONTENT_CONVERSATIONS_URI;//Telephony.Sms.CONTENT_URI
//        } else {
//            message = Uri.parse("content://mms-sms/conversations/"); //content://sms/
//        }
//    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new Intro_One(), "HOME");
        adapter.addFrag(new Intro_Two(), "BLACKLIST");
        adapter.addFrag(new Intro_Three(), "WHITELIST");
        adapter.addFrag(new Intro_Four(), "ACTIVITY");
        adapter.addFrag(new Intro_Five(), "CONTACTS");

        viewPager.setAdapter(adapter);

        circleIndicator.setViewPager(viewPager);

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }
    }
}
