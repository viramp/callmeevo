package callme.evo.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import callme.evo.R;
import callme.evo.global.ConstantCodes;

/**
 * Created by pankaj on 9/11/16.
 */
public class WebViewActivity extends AppCompatActivity {

    /*private String PAYPAL_UNSUBSCRIBE = "http://kanjariya.com/callmeevo/ws/paypal/cancelplan/%s";
    private String PAYPAL_URL = "http://kanjariya.com/callmeevo/ws/paypal/paypalprocess/%s";
    private String PAYPAL_SUCCESS_URL = "http://kanjariya.com/callmeevo/ws/paypal/status/success";
    private String PAYPAL_FAILED_URL = "http://kanjariya.com/callmeevo/ws/paypal/status/failed";
    private String PAYPAL_CANCEL_URL = "http://kanjariya.com/callmeevo//ws/paypal/cancel";*/

    private String PAYPAL_UNSUBSCRIBE = ConstantCodes.Web.BASE_URL + "paypal/cancelplan/%s";
    private String PAYPAL_URL = ConstantCodes.Web.BASE_URL + "paypal/paypalprocess/%s";
    private String PAYPAL_SUCCESS_URL = ConstantCodes.Web.BASE_URL + "paypal/status/success";
    private String PAYPAL_FAILED_URL = ConstantCodes.Web.BASE_URL + "paypal/status/failed";
    private String PAYPAL_CANCEL_URL = ConstantCodes.Web.BASE_URL + "paypal/cancel";

    private static final String TAG = "WebViewActivity";
    WebView webView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(0,0);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        webView = (WebView) findViewById(R.id.webview);
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i(TAG, "Processing webview url click...");
                if (PAYPAL_SUCCESS_URL.equalsIgnoreCase(url)) {
                    //Success
                    Toast.makeText(WebViewActivity.this, "Success", Toast.LENGTH_LONG).show();
                    setResult(RESULT_OK);
                    finish();
                } else if (PAYPAL_FAILED_URL.equalsIgnoreCase(url)) {
                    //Failed
                    Toast.makeText(WebViewActivity.this, "Failed", Toast.LENGTH_LONG).show();
                    setResult(RESULT_CANCELED);
                    finish();
                } else if (PAYPAL_CANCEL_URL.equalsIgnoreCase(url)) {
                    //Cancelled By User
                    Toast.makeText(WebViewActivity.this, "Canceled", Toast.LENGTH_LONG).show();
                    setResult(RESULT_CANCELED);
                    finish();
                } else {
                    view.loadUrl(url);
                }
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                Log.i(TAG, "Finished loading URL: " + url);
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.e(TAG, "Error: " + description);
                Toast.makeText(WebViewActivity.this, "Oh no! " + description, Toast.LENGTH_LONG).show();
                setResult(RESULT_CANCELED);
                finish();
            }
        });


        String iUserId = getIntent().getStringExtra(ConstantCodes.INTENT_ID);

        if (ConstantCodes.PAYMENT_PROCESS.equalsIgnoreCase(getIntent().getStringExtra(ConstantCodes.INTENT_FROM))) {
            Log.i(TAG, "Start first Loading URL: " + String.format(PAYPAL_URL, iUserId));
            webView.loadUrl(String.format(PAYPAL_URL, iUserId));
        } else if (ConstantCodes.PAYMENT_CANCEL.equalsIgnoreCase(getIntent().getStringExtra(ConstantCodes.INTENT_FROM))) {
            Log.i(TAG, "Start first Loading URL: " + String.format(PAYPAL_URL, iUserId));
            webView.loadUrl(String.format(PAYPAL_UNSUBSCRIBE, iUserId));
        } else {
            finish();
        }

    }
}
