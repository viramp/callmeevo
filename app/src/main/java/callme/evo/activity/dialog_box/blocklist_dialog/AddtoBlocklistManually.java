package callme.evo.activity.dialog_box.blocklist_dialog;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.activeandroid.query.Update;

import java.util.ArrayList;

import callme.evo.R;
import callme.evo.global.BroadcastHelper;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.Contacts;


/**
 * Created by sahil desai on 5/5/2016.
 */


public class AddtoBlocklistManually extends Activity {

    private LinearLayout linearBlockCalls, linearBlockTexts;
    private CheckBox chkBlockCalls, chkBlockTexts;
    private TextView txtAdd, txtCancel;
    private EditText edtNumber;
    String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //   requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.add_to_block_list_number_details);
        getWindow().setLayout((getWindowManager().getDefaultDisplay().getWidth() * 80 / 100), ViewGroup.LayoutParams.WRAP_CONTENT);

        edtNumber = (EditText) findViewById(R.id.edt_number);

        if (getIntent() != null && getIntent().hasExtra(ConstantCodes.INTENT_NUMBER)) {
            name = getIntent().getStringExtra(ConstantCodes.INTENT_NAME);
            edtNumber.setText(getIntent().getStringExtra(ConstantCodes.INTENT_NUMBER));
            edtNumber.setEnabled(false);
        } else {
            edtNumber.setEnabled(true);
        }

        txtAdd = (TextView) findViewById(R.id.txt_add);

        chkBlockCalls = (CheckBox) findViewById(R.id.chk_block_calls);
        chkBlockTexts = (CheckBox) findViewById(R.id.chk_block_texts);

        txtCancel = (TextView) findViewById(R.id.txt_cancel);

        linearBlockCalls = (LinearLayout) findViewById(R.id.linear_block_calls);
        linearBlockTexts = (LinearLayout) findViewById(R.id.linear_block_texts);

        chkBlockCalls.setChecked(true);
        ((TextView) linearBlockCalls.getChildAt(1)).setSelected(chkBlockCalls.isChecked());

        chkBlockTexts.setChecked(true);
        ((TextView) linearBlockTexts.getChildAt(1)).setSelected(chkBlockTexts.isChecked());

        linearBlockCalls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                chkBlockCalls.setChecked(!chkBlockCalls.isChecked());
                ((TextView) linearBlockCalls.getChildAt(1)).setSelected(chkBlockCalls.isChecked());

            }
        });

        linearBlockTexts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                chkBlockTexts.setChecked(!chkBlockTexts.isChecked());
                ((TextView) linearBlockTexts.getChildAt(1)).setSelected(chkBlockTexts.isChecked());

            }
        });

        txtAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(edtNumber.getText().toString().trim())) {

                    Toast.makeText(AddtoBlocklistManually.this, "Enter some numbers first", Toast.LENGTH_SHORT).show();

                    return;

                } else if (chkBlockCalls.isChecked() == false && chkBlockTexts.isChecked() == false) {

                    Toast.makeText(AddtoBlocklistManually.this, "Block atleast one from call or text", Toast.LENGTH_SHORT).show();

                    return;
                }

                Contacts contacts = new Contacts();
                contacts.name = name;
                contacts.number = edtNumber.getText().toString().trim();
                contacts.cleanNumber = Utility.getFinalNumber(edtNumber.getText().toString().trim());
                if (chkBlockCalls.isChecked() == chkBlockTexts.isChecked() == true) {
                    contacts.block_type = ConstantCodes.BLOCK_TYPE_BOTH;
                } else if (chkBlockCalls.isChecked() == true) {
                    contacts.block_type = ConstantCodes.BLOCK_TYPE_CALLS;
                } else if (chkBlockTexts.isChecked() == true) {
                    contacts.block_type = ConstantCodes.BLOCK_TYPE_MESSAGE;
                }

                contacts.blackListOrWhiteList = ConstantCodes.BLACK_LIST;

                ArrayList<Contacts> list = new Select().from(Contacts.class).where("number <> '' AND (number LIKE '%" + contacts.number + "' OR '" + contacts.number + "' LIKE '%'||contacts.number)").execute();
                if (list != null && list.size() > 0) {
                    new Update(Contacts.class).set("blackListOrWhiteList=?,block_type=? ", new String[]{
                            contacts.blackListOrWhiteList,
                            contacts.block_type
                    }).where("number LIKE '%" + contacts.number + "' OR '" + contacts.number + "' LIKE '%'||contacts.number").execute();
                } else {
                    contacts.contactId = System.currentTimeMillis();
                    contacts.save();
                }

                Intent intentBroadcast = new Intent(ConstantCodes.BROADCAST_REFRESH);
                BroadcastHelper.sendBroadcast(AddtoBlocklistManually.this, intentBroadcast);

//                Toast.makeText(AddtoBlocklistManually.this, "Contact will add to block list", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent();
                intent.putExtra(ConstantCodes.INTENT_TO_SCREEN, 4);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


}
