package callme.evo.activity.dialog_box.drive_mode;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import callme.evo.R;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.services.ForgroundService;

/**
 * Created by sahil desai on 6/2/2016.
 */
public class DriveModeDialog extends FragmentActivity {

    ViewPager dialogViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(0, 0);
        super.onCreate(savedInstanceState);
        //   requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.dialog_pager);
        Window window = getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        // wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

        fragment1 = new DriveMode_1();
        fragment2 = new DriveMode_2();

        dialogViewPager = (ViewPager) findViewById(R.id.dialogViewPager);

        setupViewPager(dialogViewPager);




        dialogViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 1) {

                    //Saving values in preferences
                    if (fragment1 != null) {
                        Utility.getSharedPreference(DriveModeDialog.this).edit().putBoolean(ConstantCodes.DRIVE_MODE_INTERCEPT_CALLS_SMS, fragment1.switch_intercept_calls_sms.isChecked()).commit();
                        Utility.getSharedPreference(DriveModeDialog.this).edit().putBoolean(ConstantCodes.DRIVE_MODE_WHITELIST_CALLS, fragment1.switch_whitelist_calls.isChecked()).commit();
                        Utility.getSharedPreference(DriveModeDialog.this).edit().putBoolean(ConstantCodes.DRIVE_MODE_UNKNOWN_CALLS, fragment1.switch_unknown_calls.isChecked()).commit();
                        Utility.getSharedPreference(DriveModeDialog.this).edit().putBoolean(ConstantCodes.DRIVE_MODE_UNWANTED_CALLS, fragment1.switch_unwanted_calls.isChecked()).commit();
                    }

//                    Intent intentSummary = new Intent(getApplicationContext(), DrivingModeSummaryActivity.class);
//                    startActivity(intentSummary);

                    ForgroundService.startServiceForDrivingMode(DriveModeDialog.this, false, true);
                    finish();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    DriveMode_1 fragment1;
    DriveMode_2 fragment2;

    void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(fragment1, "");
        adapter.addFrag(fragment2, "");
        viewPager.setAdapter(adapter);


    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}