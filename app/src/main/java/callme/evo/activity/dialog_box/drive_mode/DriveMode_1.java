package callme.evo.activity.dialog_box.drive_mode;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import callme.evo.R;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;


public class DriveMode_1 extends Fragment implements CompoundButton.OnCheckedChangeListener {

    public CheckBox switch_intercept_calls_sms, switch_whitelist_calls, switch_unknown_calls, switch_unwanted_calls;

    public DriveMode_1() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_drivemode_1, container, false);

        switch_intercept_calls_sms = (CheckBox) view.findViewById(R.id.switch_intercept_calls_sms);
        switch_whitelist_calls = (CheckBox) view.findViewById(R.id.switch_whitelist_calls);
        switch_unknown_calls = (CheckBox) view.findViewById(R.id.switch_unknown_calls);
        switch_unwanted_calls = (CheckBox) view.findViewById(R.id.switch_unwanted_calls);


        view.findViewById(R.id.txt_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        switch_intercept_calls_sms.setChecked(Utility.getSharedPreference(getActivity()).getBoolean(ConstantCodes.DRIVE_MODE_INTERCEPT_CALLS_SMS, false));
        switch_whitelist_calls.setChecked(Utility.getSharedPreference(getActivity()).getBoolean(ConstantCodes.DRIVE_MODE_WHITELIST_CALLS, false));
        switch_unknown_calls.setChecked(Utility.getSharedPreference(getActivity()).getBoolean(ConstantCodes.DRIVE_MODE_UNKNOWN_CALLS, false));
        switch_unwanted_calls.setChecked(Utility.getSharedPreference(getActivity()).getBoolean(ConstantCodes.DRIVE_MODE_UNWANTED_CALLS, false));

        switch_intercept_calls_sms.setOnCheckedChangeListener(this);
        switch_whitelist_calls.setOnCheckedChangeListener(this);
//        switch_unknown_calls .setOnCheckedChangeListener(this);
//        switch_unwanted_calls.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.switch_intercept_calls_sms:
                if (isChecked) {
                    switch_whitelist_calls.setChecked(false);
                }

                break;
            case R.id.switch_whitelist_calls:
                if (isChecked) {
                    switch_intercept_calls_sms.setChecked(false);
                }
                break;
        }
    }
}