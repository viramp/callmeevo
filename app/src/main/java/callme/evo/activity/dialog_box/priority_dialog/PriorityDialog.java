package callme.evo.activity.dialog_box.priority_dialog;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.activeandroid.ActiveAndroid;

import callme.evo.R;
import callme.evo.global.BroadcastHelper;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.Contacts;
import customtextviews.MyTextViewBook;


/**
 * Created by sahil desai on 5/5/2016.
 */


public class PriorityDialog extends Activity {

    MyTextViewBook manually;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //   requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.priority_dialog);
        getWindow().setLayout((getWindowManager().getDefaultDisplay().getWidth() * 80 / 100), ViewGroup.LayoutParams.WRAP_CONTENT);


//        final String number = getIntent().getStringExtra(ConstantCodes.INTENT_NUMBER);
        findViewById(R.id.radio_high).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePriority(1);
            }
        });
        findViewById(R.id.radio_normal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePriority(0);
            }
        });

        if ("Normal".equalsIgnoreCase(getIntent().getStringExtra(ConstantCodes.INTENT_PRIORITY))) {
            ((RadioButton) findViewById(R.id.radio_normal)).setChecked(true);
        } else {
            ((RadioButton) findViewById(R.id.radio_high)).setChecked(true);
        }

    }

    private void changePriority(int priority) {

        ContentValues values = new ContentValues();
        values.put("priority", priority + "");
        int affectedRows = ActiveAndroid.getDatabase().update("Contacts", values, " number like '%" + getIntent().getStringExtra(ConstantCodes.INTENT_NUMBER) + "'"+" OR "+" '" + getIntent().getStringExtra(ConstantCodes.INTENT_NUMBER) + "' like '%'||number ", null);

        if (affectedRows <= 0) {
            Contacts contacts = new Contacts();
            contacts.name = getIntent().getStringExtra(ConstantCodes.INTENT_NAME);
            contacts.number = getIntent().getStringExtra(ConstantCodes.INTENT_NUMBER);
            contacts.cleanNumber = Utility.getFinalNumber(getIntent().getStringExtra(ConstantCodes.INTENT_NUMBER));
            contacts.save();
        }
        /*String query = "UPDATE Contacts SET priority=" + priority + " WHERE number='" + getIntent().getStringExtra(ConstantCodes.INTENT_NUMBER) + "'";
        ActiveAndroid.getDatabase().execSQL(query);*/

        Intent intentBroadcast = new Intent(ConstantCodes.BROADCAST_REFRESH);
        BroadcastHelper.sendBroadcast(PriorityDialog.this, intentBroadcast);

        Intent intent = new Intent();
        intent.putExtra(ConstantCodes.INTENT_PRIORITY, priority == 1 ? "High" : "Normal");
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
