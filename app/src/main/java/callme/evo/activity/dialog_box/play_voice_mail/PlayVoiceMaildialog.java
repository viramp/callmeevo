package callme.evo.activity.dialog_box.play_voice_mail;

import android.app.Activity;
import android.os.Bundle;
import android.view.ViewGroup;

import callme.evo.R;

/**
 * Created by sahil desai on 6/22/2016.
 */
public class PlayVoiceMaildialog extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_voice_mail_dialog);

        getWindow().setLayout((getWindowManager().getDefaultDisplay().getWidth() * 80 / 100), ViewGroup.LayoutParams.WRAP_CONTENT);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
