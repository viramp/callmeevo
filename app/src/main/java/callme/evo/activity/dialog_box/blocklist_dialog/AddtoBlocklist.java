package callme.evo.activity.dialog_box.blocklist_dialog;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import callme.evo.R;
import callme.evo.global.ConstantCodes;
import customtextviews.MyTextViewBook;


/**
 * Created by sahil desai on 5/5/2016.
 */


public class AddtoBlocklist extends Activity {

    MyTextViewBook manually;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //   requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.add_to_block_list);
        getWindow().setLayout((getWindowManager().getDefaultDisplay().getWidth() * 80 / 100), ViewGroup.LayoutParams.WRAP_CONTENT);

        findViewById(R.id.txt_from_recent_calls).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(ConstantCodes.INTENT_TO_SCREEN, 3);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        findViewById(R.id.txt_from_recent_texts).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(ConstantCodes.INTENT_TO_SCREEN, 5);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        findViewById(R.id.txt_from_contacts).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(ConstantCodes.INTENT_TO_SCREEN, 4);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        manually = (MyTextViewBook) findViewById(R.id.manually);
        manually.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AddtoBlocklist.this, AddtoBlocklistManually.class);
                startActivity(intent);
                finish();
                overridePendingTransition(0,0);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
