package callme.evo.activity.dialog_box.blockmode;

import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

import callme.evo.R;
import callme.evo.global.ConstantCodes;
import callme.evo.global.DateHelper;
import callme.evo.global.Utility;
import callme.evo.receiver.CancelDNDReceiver;
import callme.evo.services.ForgroundService;

/**
 * Created by sahil desai on 6/2/2016.
 */
public class BlockModeDialog extends FragmentActivity {

    RadioButton radioUntilYouTurnOff, radioFor1Hr, radioSpecifiedTime;
    int choice = 1;
    Calendar calendar;
    String startText;
    CheckBox chkAllowPriorityContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //   requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.dialog_blockmode);
        Window window = getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        // wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

        chkAllowPriorityContact = (CheckBox) findViewById(R.id.chk_allow_priority_contact);

        boolean isChecked = Utility.getSharedPreference(BlockModeDialog.this).
                getBoolean(ConstantCodes.DND_ALLOW_PRIORITY_CONTACT, chkAllowPriorityContact.isChecked());

        if(isChecked){
            chkAllowPriorityContact.setChecked(true);
        }else{
            chkAllowPriorityContact.setChecked(false);
        }
        radioUntilYouTurnOff = (RadioButton) findViewById(R.id.radio_until_you_turn_off);
        radioFor1Hr = (RadioButton) findViewById(R.id.radio_for_one_hour);
        radioSpecifiedTime = (RadioButton) findViewById(R.id.radio_until_specified_time);

        startText = radioSpecifiedTime.getText().toString();

        switch (Utility.getSharedPreference(this).getInt(ConstantCodes.DND_TYPE_CHOISE, 1)) {
            case 1:
                choice = 1;
                radioUntilYouTurnOff.setChecked(true);
                break;
            case 2:
                choice = 2;
                radioFor1Hr.setChecked(true);
                break;
            case 3:
                choice = 3;
                radioSpecifiedTime.setChecked(true);
                long time = Utility.getSharedPreference(this).getLong(ConstantCodes.TIME, 0);
                if (time > 0 && time > Calendar.getInstance().getTimeInMillis()) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTimeInMillis(time);
                    String hhmmss = calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE);
                    String formatedDate=DateHelper.getFormatedDate(calendar, DateHelper.DATE_FORMAT_DATE + " " + DateHelper.DATE_FORMAT_TIME);
                    radioSpecifiedTime.setText(startText + " " + formatedDate);
                } else {
                    Utility.getSharedPreference(BlockModeDialog.this).edit().remove(ConstantCodes.DND_TYPE_CHOISE).commit();
                    Utility.getSharedPreference(BlockModeDialog.this).edit().remove(ConstantCodes.DND_TYPE).commit();
                    choice = 0;
                    radioSpecifiedTime.setText(startText);
                }
                break;
        }


        TextView txtDone = (TextView) findViewById(R.id.txt_done);
        if (Utility.getSharedPreference(this).contains(ConstantCodes.DND_TYPE)) {
//            radioUntilYouTurnOff.setText("Turn Off This");
            txtDone.setText("STOP DND");
        } else {
        }

        radioUntilYouTurnOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    choice = 1;
            }
        });

        radioFor1Hr.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    choice = 2;
            }
        });

        radioSpecifiedTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar = Calendar.getInstance();
                TimePickerDialog timePickerDialog = new TimePickerDialog(BlockModeDialog.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendar.set(Calendar.MINUTE, minute);

                        //previous logic
                        /*if (Calendar.getInstance().getTimeInMillis() > calendar.getTimeInMillis()) {
                            Toast.makeText(BlockModeDialog.this, "Time should be greater then today", Toast.LENGTH_SHORT).show();
                            radioUntilYouTurnOff.setChecked(true);
                        } else {
                            choice = 3;
                            String hhmmss = calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE);
                            radioSpecifiedTime.setText(startText + " " + hhmmss);
                        }*/


                        if (Calendar.getInstance().getTimeInMillis() > calendar.getTimeInMillis()) {
                            Toast.makeText(BlockModeDialog.this, "DND till " + DateHelper.getFormatedDate(calendar, DateHelper.DATE_FORMAT_DATE + " " + DateHelper.DATE_FORMAT_TIME) + " activated", Toast.LENGTH_SHORT).show();
                        }
                        choice = 3;
                        String hhmmss = calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE);
                        String formatedDate=DateHelper.getFormatedDate(calendar, DateHelper.DATE_FORMAT_DATE + " " + DateHelper.DATE_FORMAT_TIME);
                        radioSpecifiedTime.setText(startText + " " + formatedDate);


                    }
                }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false);
                timePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        calendar = null;
                        radioSpecifiedTime.setText(startText);
                        radioUntilYouTurnOff.setChecked(true);
                    }
                });
                timePickerDialog.show();
            }
        });
        radioSpecifiedTime.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                }
            }
        });
        txtDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (txtDone.getText().toString().trim().equalsIgnoreCase("STOP DND")) {
                    Utility.getSharedPreference(BlockModeDialog.this).edit().remove(ConstantCodes.DND_TYPE_CHOISE).commit();
                    Utility.getSharedPreference(BlockModeDialog.this).edit().remove(ConstantCodes.DND_TYPE).commit();
                    Utility.getSharedPreference(BlockModeDialog.this).edit().remove(ConstantCodes.DND_ALLOW_PRIORITY_CONTACT).commit();
                    radioUntilYouTurnOff.setText("Until you turn this off");

                    ForgroundService.startServiceForDND(BlockModeDialog.this, true);
                } else {

                    Utility.getSharedPreference(BlockModeDialog.this).edit().putBoolean(ConstantCodes.DND_ALLOW_PRIORITY_CONTACT, chkAllowPriorityContact.isChecked()).commit();

                    switch (choice) {
                        case 1:
                            if (Utility.getSharedPreference(BlockModeDialog.this).contains(ConstantCodes.DND_TYPE)) {
                                Utility.getSharedPreference(BlockModeDialog.this).edit().remove(ConstantCodes.DND_TYPE_CHOISE).commit();
                                Utility.getSharedPreference(BlockModeDialog.this).edit().remove(ConstantCodes.DND_TYPE).commit();
                                radioUntilYouTurnOff.setText("Until you turn this off");

                                ForgroundService.startServiceForDND(BlockModeDialog.this, true);
                            } else {
                                Utility.getSharedPreference(BlockModeDialog.this).edit().putInt(ConstantCodes.DND_TYPE_CHOISE, choice).commit();
                                Utility.getSharedPreference(BlockModeDialog.this).edit().putString(ConstantCodes.DND_TYPE, ConstantCodes.BLOCK_ALL).commit();
                                radioUntilYouTurnOff.setText("Turn Off This");

                                ForgroundService.startServiceForDND(BlockModeDialog.this, false);
                            }
                            break;
                        case 2:
                            Calendar calendar1 = Calendar.getInstance();
                            calendar1.add(Calendar.HOUR_OF_DAY, +1);
                            Utility.getSharedPreference(BlockModeDialog.this).edit().putInt(ConstantCodes.DND_TYPE_CHOISE, choice).commit();
                            Utility.getSharedPreference(BlockModeDialog.this).edit().putString(ConstantCodes.DND_TYPE, ConstantCodes.BLOCK_TIME).commit();
                            Utility.getSharedPreference(BlockModeDialog.this).edit().putLong(ConstantCodes.TIME, calendar1.getTimeInMillis()).commit();

                            calendar1.set(Calendar.SECOND, 0);
                            calendar1.set(Calendar.MILLISECOND, 0);

                            //Setting alarm to disable settings
                            PendingIntent pendingIntent = PendingIntent.getBroadcast(BlockModeDialog.this, (int) calendar1.getTimeInMillis(), new Intent(BlockModeDialog.this, CancelDNDReceiver.class), PendingIntent.FLAG_CANCEL_CURRENT);

                        /*AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar1.getTimeInMillis(), pendingIntent);*/

                            Utility.setAlarm(BlockModeDialog.this, calendar1.getTimeInMillis(), pendingIntent);
                            ForgroundService.startServiceForDND(BlockModeDialog.this, false);

                            break;
                        case 3:

                            if (calendar != null) {
//                        Calendar calendar2 = Calendar.getInstance();
//                        calendar2.add(Calendar.HOUR_OF_DAY, +2);
                                Utility.getSharedPreference(BlockModeDialog.this).edit().putInt(ConstantCodes.DND_TYPE_CHOISE, choice).commit();
                                Utility.getSharedPreference(BlockModeDialog.this).edit().putString(ConstantCodes.DND_TYPE, ConstantCodes.BLOCK_TIME).commit();
                                Utility.getSharedPreference(BlockModeDialog.this).edit().putLong(ConstantCodes.TIME, calendar.getTimeInMillis()).commit();

                                calendar.set(Calendar.SECOND, 0);
                                calendar.set(Calendar.MILLISECOND, 0);

                                //Setting alarm to disable settings
                                PendingIntent pendingIntent1 = PendingIntent.getBroadcast(BlockModeDialog.this, (int) calendar.getTimeInMillis(), new Intent(BlockModeDialog.this, CancelDNDReceiver.class), PendingIntent.FLAG_CANCEL_CURRENT);

                            /*AlarmManager alarmManager1 = (AlarmManager) getSystemService(ALARM_SERVICE);
                            alarmManager1.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent1);*/

                                Utility.setAlarm(BlockModeDialog.this, calendar.getTimeInMillis(), pendingIntent1);
                                ForgroundService.startServiceForDND(BlockModeDialog.this, false);
                            }
                            break;
                    }
                }
                finish();
            }
        });
        findViewById(R.id.txt_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
