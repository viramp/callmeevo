package callme.evo.activity.dialog_box.add_to_voicemail;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.activeandroid.query.Select;
import com.activeandroid.query.Update;

import java.util.ArrayList;
import java.util.UUID;

import callme.evo.R;
import callme.evo.global.BroadcastHelper;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.Contacts;
import callme.evo.models.VoiceMessageModel;


/**
 * Created by sahil desai on 5/5/2016.
 */


public class AddtoVoiceMail extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //   requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.add_to_voicemail);
        getWindow().setLayout((getWindowManager().getDefaultDisplay().getWidth() * 80 / 100), ViewGroup.LayoutParams.WRAP_CONTENT);

        findViewById(R.id.txt_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.txt_create_message).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                loadRelatedContacts();

            }
        });

        findViewById(R.id.txt_new_message).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddEditMessageDialog();
            }
        });
    }

    private void showAddEditMessageDialog() {
        final View view = LayoutInflater.from(AddtoVoiceMail.this).inflate(R.layout.dialog_create_voice_msg, null, false);
        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(AddtoVoiceMail.this);
        alertDialogBuilder.setView(view);
        final android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        view.findViewById(R.id.text_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VoiceMessageModel voiceMessageModel = new VoiceMessageModel();
                voiceMessageModel.voiceMessageId = UUID.randomUUID().toString();
                voiceMessageModel.title = ((EditText) view.findViewById(R.id.edt_title)).getText().toString();
                voiceMessageModel.message = ((EditText) view.findViewById(R.id.edt_message)).getText().toString();
                voiceMessageModel.save();
                alertDialog.dismiss();

                new Update(Contacts.class).set("voiceMessageId='" + voiceMessageModel.voiceMessageId + "'").where("number LIKE '%" + getIntent().getStringExtra(ConstantCodes.INTENT_NUMBER) + "'").execute();

                BroadcastHelper.sendBroadcast(AddtoVoiceMail.this, ConstantCodes.BROADCAST_CONTACT_REFRESH);
                finish();
            }
        });
    }

    AlertDialog alertDialog;
    ProgressDialog progressBar;

    private void loadRelatedContacts() {
        new AsyncTask<Void, Void, ArrayList<VoiceMessageModel>>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar = Utility.progressDialog(AddtoVoiceMail.this, "Please wait...");
            }

            @Override
            protected ArrayList<VoiceMessageModel> doInBackground(Void... params) {

                Log.i("CONTACT", "READING VOICE MESSAGE");

//                ArrayList<VoiceMessageModel> list = new Select().from(VoiceMessageModel.class).execute();

                ArrayList<VoiceMessageModel> list = new Select().from(VoiceMessageModel.class).where("voiceMessageId<>'" + ConstantCodes.FREE_VOICE_MAIL_TITLE + "' AND voiceMessageId<>'" + ConstantCodes.FREE_VOICE_MAIL_TITLE_FOR_DRIVING + "'").execute();
                Log.i("CONTACT", "READING VOICE MESSAGE size" + list.size());


                return list;
            }

            @Override
            protected void onPostExecute(ArrayList<VoiceMessageModel> list) {
                super.onPostExecute(list);

                progressBar.dismiss();

                Log.i("CONTACT", "READING CONTACT size" + list.size());

                if (list != null) {

                    int i = 0;
                    String names[] = new String[list.size()];
                    for (VoiceMessageModel voiceMessageModel : list) {
                        names[i++] = voiceMessageModel.title;
                    }


                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(AddtoVoiceMail.this);
                    alertBuilder.setItems(names, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
//                            Toast.makeText(AddtoVoiceMail.this, list.get(which).title + " was clicked", Toast.LENGTH_SHORT).show();
                            new Update(Contacts.class).set("voiceMessageId='" + list.get(which).voiceMessageId + "'").where("number LIKE '%" + getIntent().getStringExtra(ConstantCodes.INTENT_NUMBER) + "'").execute();
                            alertDialog.dismiss();
                            finish();
                        }
                    });
                    alertDialog = alertBuilder.create();
                    alertDialog.show();
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}