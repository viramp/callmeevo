package callme.evo;

import android.Manifest;
import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.CallLog;
import android.provider.Telephony;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.moez.QKSMS.data.Contact;
import com.moez.QKSMS.data.Conversation;
import com.moez.QKSMS.data.RecipientIdCache;
import com.moez.QKSMS.transaction.SmsHelper;

import java.util.ArrayList;
import java.util.Date;

import callme.evo.global.BroadcastHelper;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.ContactCallLog;
import callme.evo.models.Contacts;
import callme.evo.models.Sms;

/**
 * Created by pankaj on 16/9/16.
 */
public class SyncContactService extends IntentService {

    Context context;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public SyncContactService(String name) {
        super(name);
    }

    public SyncContactService() {
        super("SyncContactService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        context = getApplicationContext();
        Intent intentBroadcast = new Intent(ConstantCodes.BROADCAST_CONTACT_REFRESH);
        Log.e("SyncContactService ", " readContacts() -- 1 "+ System.currentTimeMillis());
        syncContacts();
        BroadcastHelper.sendBroadcast(context, intentBroadcast);
        Log.e("SyncContactService ", "readContacts() -- 2 "+ System.currentTimeMillis());
        syncCallLog();
        BroadcastHelper.sendBroadcast(context, intentBroadcast);
        Log.e("SyncContactService ", "readContacts() -- 3 "+ System.currentTimeMillis());
//        syncSMS();
        syncSMSNew();
        BroadcastHelper.sendBroadcast(context, intentBroadcast);
        Log.e("SyncContactService ", "readContacts() -- 4 "+ System.currentTimeMillis());



    }

    private void syncCallLog() {

        long contactCallLogLast = Utility.getSharedPreference(context).getLong(ConstantCodes.CONTACT_CALLLOG_LAST, 0);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Cursor managedCursor = context.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, CallLog.Calls.DATE + ">" + contactCallLogLast, null, CallLog.Calls.DATE + " DESC");
        int idColumn = managedCursor.getColumnIndex(CallLog.Calls._ID);
        int name = managedCursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);

        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
//        sb.append("Call Details :");


        int i = 0;
        if (managedCursor.moveToFirst()) {
            do {
                if (i == 0) {
                    try {
                        Utility.getSharedPreference(context).edit().putLong(ConstantCodes.CONTACT_CALLLOG_LAST, Long.parseLong(managedCursor.getString(date))).commit();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                i = 2;

                long id = managedCursor.getLong(idColumn);
                String phName = managedCursor.getString(name);
                String phNumber = managedCursor.getString(number);
                String callType = managedCursor.getString(type);
                String callDate = managedCursor.getString(date);
                Date callDayTime = new Date(Long.valueOf(callDate));
                String callDuration = managedCursor.getString(duration);
                String dir = null;
                int dircode = Integer.parseInt(callType);
                switch (dircode) {
                    case CallLog.Calls.OUTGOING_TYPE:
                        dir = "OUTGOING";
                        break;

                    case CallLog.Calls.INCOMING_TYPE:
                        dir = "INCOMING";
                        break;

                    case CallLog.Calls.MISSED_TYPE:
                        dir = "MISSED";
                        break;
                }

                ContactCallLog contacts = new ContactCallLog();
                contacts.callLogId = id;
                contacts.name = phName;
                contacts.number = phNumber;
                contacts.cleanNumber = Utility.getFinalNumber(phNumber);
                contacts.timestamp = Long.valueOf(callDate);
                contacts.duration = Long.valueOf(callDuration);
                //saving call log
                contacts.save();

            } while (managedCursor.moveToNext());
            managedCursor.close();
        }
    }

    private void syncContacts() {

        boolean needToSync = false;
        /*int alreadyExist = 0;
        Cursor cursor = ActiveAndroid.getDatabase().rawQuery("SELECT COUNT(1) as total FROM Contacts", null);
        if (cursor != null && cursor.moveToFirst()) {
            alreadyExist = Integer.parseInt(cursor.getString(cursor.getColumnIndex("total")));
        }
        cursor.close();*/
//        ArrayList<Contacts> listC = new Select().from(Contacts.class).execute();


        ArrayList<Contacts> list = Utility.readContacts(context);
        /*if(list.size()!=alreadyExist){
            boolean needToSend;
        }*/

        boolean isContinue = true;
        long deleteFlag = System.currentTimeMillis();
//        ActiveAndroid.beginTransaction();
        for (Contacts contacts : list) {

            ArrayList<Contacts> item = new Select().from(Contacts.class).where("contactId='" + contacts.contactId + "'").execute();
            if (item.size() > 0) {
                //update
                contacts.setId(item.get(0).getId());
                android.util.Log.d("READ_CONTACT", "update contact " + contacts.name);
                contacts.deleteFlag = deleteFlag;
                contacts.update(new String[]{"name", "thumbnailImage", "photoImage", "deleteFlag"});
            } else {
                //insert
                android.util.Log.d("READ_CONTACT", "save contact " + contacts.name);
                contacts.blackListOrWhiteList = ConstantCodes.WHITE_LIST;
                contacts.deleteFlag = deleteFlag;
                contacts.save();
                needToSync = true;
            }

            /*isContinue = true;
            for (Contacts contacts1 : listC) {
                if (Utility.getFinalNumber(contacts1.number).equalsIgnoreCase(Utility.getFinalNumber(contacts.number))) {
                    isContinue = false;
                    contacts.setId(contacts1.getId());
                    contacts.update(new String[]{"name", "thumbnailImage", "photoImage"});
                    Log.e("TTTTT", contacts1.thumbnailImage + "=" + contacts.thumbnailImage);
                    *//*if (contacts1.name.contains("Abdul Salam")) {
                        Log.e("TTTTT", contacts1.thumbnailImage + "=" + contacts.thumbnailImage);
                    }
                    if (contacts1.thumbnailImage != null && !contacts1.thumbnailImage.equalsIgnoreCase(contacts.thumbnailImage)) {
                        contacts.update(new String[]{"name", "thumbnailImage", "photoImage"});
                    } else if (contacts1.photoImage != null && !contacts1.photoImage.equalsIgnoreCase(contacts.photoImage)) {
                        contacts.update(new String[]{"name", "thumbnailImage", "photoImage"});
                    }*//*

                    break;
                }
            }
            if (isContinue) {
                android.util.Log.d("READ_CONTACT", "save contact");
                contacts.blackListOrWhiteList = ConstantCodes.WHITE_LIST;
                contacts.save();
            }*/
        }
//        ActiveAndroid.endTransaction();
        new Delete().from(Contacts.class).where("deleteFlag != '" + deleteFlag + "'")
                .and("deleteFlag>0")
                .and("blackListOrWhiteList!='" + ConstantCodes.BLACK_LIST + "'")
                .execute();

        if (needToSync) {
            Intent intent = new Intent(context, SyncToServerService.class);
            startService(intent);
        }
        Log.d("READ_CONTACT", "readContacts() called with: " );
    }


    private static void showLog(String message) {
        Log.i("ws", message + "");
    }

    public void syncSMS() {

        Uri message = null;
        /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            message = Telephony.MmsSms.CONTENT_CONVERSATIONS_URI;//Telephony.Sms.CONTENT_URI
        } else {
            message = Uri.parse("content://mms-sms/conversations/"); //content://sms/  ?simple=true
        }*/


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            message = Telephony.Sms.Inbox.CONTENT_URI;
        } else {
            message = Uri.parse("content://sms/inbox");
        }
//        Telephony.Sms.Conversations.CONTENT_URI;


        String[] columns = new String[]{Telephony.Sms._ID, Telephony.Sms.PERSON, Telephony.Sms.ADDRESS, Telephony.Sms.BODY, Telephony.Sms.DATE, Telephony.Sms.THREAD_ID};

        ContentResolver cr = getContentResolver();
        Cursor c = cr.query(message, columns, null, null, Telephony.Sms.DATE + " DESC");

        int idPos = c.getColumnIndex(Telephony.Sms._ID);
        int threadIdPos = c.getColumnIndex(Telephony.Sms.THREAD_ID);
        int namePos = c.getColumnIndex(Telephony.Sms.PERSON);
        int addressPos = c.getColumnIndex(Telephony.Sms.ADDRESS);
        int bodyPos = c.getColumnIndex(Telephony.Sms.BODY);
        int datePos = c.getColumnIndex(Telephony.Sms.DATE);
//        startManagingCursor(c);

        int totalSMS = c.getCount();

        String threadIds = "";
        Sms objSms;

        if (c.moveToFirst()) {
            for (String s : c.getColumnNames()) {
                android.util.Log.e("SMS_READ", s + " : " + c.getString(c.getColumnIndex(s)));
            }
            do {
//                threadIds += c.getString(c.getColumnIndex("thread_id")) + ",";

                objSms = new Sms();
                objSms.smsId = c.getLong(idPos);
                objSms.threadId = c.getLong(threadIdPos);
                objSms.number = Utility.getFinalNumber(c.getString(addressPos));
                 /*Added by Viram Purohit*/
                ArrayList<Contacts> item = new Select().from(Contacts.class).
                        where("number='" + objSms.number + "'").execute();
                if (item.size() > 0) {
                    //update
                    objSms.priority = item.get(0).priority;
                }
                objSms.cleanNumber = Utility.getFinalNumber(c.getString(addressPos));
                objSms.name = c.getString(namePos);
                objSms.message = c.getString(bodyPos);
                objSms.date = c.getLong(datePos);
                android.util.Log.d("READ_CONTACT", "save conversation+" + objSms.date);
                objSms.save();

            } while (c.moveToNext());
            threadIds = threadIds.replaceFirst(",$", "");
        }
        android.util.Log.e("SMS_READ", threadIds);
        /*Cursor curSms = cr.query(Uri.parse("content://sms/inbox"), null, "thread_id IN (" + threadIds + ")) (GROUP BY thread_id", null, Telephony.Sms.DATE + " DESC");
        if (curSms.moveToFirst()) {
            do {
                android.util.Log.e("SMS_READ_SMS", "================");
                for (String s : curSms.getColumnNames()) {
                    android.util.Log.e("SMS_READ_SMS", s + " : " + curSms.getString(curSms.getColumnIndex(s)));
                }
                threadIds += curSms.getString(curSms.getColumnIndex("thread_id"));
            } while (curSms.moveToNext());
            android.util.Log.e("SMS_READ_SMS", "================");
        }*/

            /*for (int i = 0; i < totalSMS; i++) {

                objSms = new Sms();
                objSms.smsId = c.getLong(idPos);
                objSms.number = c.getString(addressPos);
                objSms.cleanNumber = Utility.getFinalNumber(c.getString(addressPos));
                objSms.name = c.getString(namePos);
                objSms.message = c.getString(bodyPos);
                objSms.date = c.getLong(datePos);
                android.util.Log.d("READ_CONTACT", "save conversation+" + objSms.date);
                objSms.save();

                c.moveToNext();
            }*/
    }

    private void syncSMSNew() {

        ContentResolver cr = getContentResolver();
        Cursor data = cr.query(SmsHelper.CONVERSATIONS_CONTENT_PROVIDER, Conversation.ALL_THREADS_PROJECTION, null, null, "date DESC");

        if (data != null && data.moveToFirst()) {

            ArrayList<Sms> list = new ArrayList<>();
            do {
                    /*for (String columnName : data.getColumnNames()) {
                        Log.e("SMS==", columnName + " = " + data.getString(data.getColumnIndex(columnName)));
                    }*/

                Sms sms = new Sms();
                sms.smsId = data.getLong(data.getColumnIndex("_id"));
                sms.threadId = data.getLong(data.getColumnIndex("_id"));
                sms.message = data.getString(data.getColumnIndex("snippet"));
                sms.date = data.getLong(data.getColumnIndex("date"));
                sms.isRead = 1;

                try {
                    String spaceSepIds = data.getString(data.getColumnIndex("recipient_ids"));

                    for (RecipientIdCache.Entry entry : RecipientIdCache.getAddresses(spaceSepIds)) {
                        if (entry != null && !TextUtils.isEmpty(entry.number)) {
                            Contact contact = Contact.get(entry.number, false);
                            contact.setRecipientId(entry.id);
                            if (contact.isNamed())
                                sms.name = contact.getName();
                            sms.number = Utility.getFinalNumber(contact.getNumber());
                            ArrayList<Contacts> item = new Select().from(Contacts.class).
                                    where("number='" + sms.number + "'").execute();
                            if (item.size() > 0) {
                                //update
                                sms.priority = item.get(0).priority;
                            }
                            sms.thumbnailImage = contact.getPhoneUri() + "";
                            contact.getName();
                            break;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.e("READ_SMS", sms.toString());
                android.util.Log.d("READ_SMS", "save conversation+" + sms.date);

                list.add(sms);
                sms.save();

            } while (data.moveToNext());
        }
    }
}
