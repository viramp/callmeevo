package callme.evo.services;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.activeandroid.query.Select;

import java.util.ArrayList;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import callme.evo.bluetooth.BluetoothObject;
import callme.evo.global.BroadcastHelper;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.BluetoothModel;

/**
 * Created by Viram on 01/06/2017.
 */
public class BluetoothService extends Service {
    private ArrayList<BluetoothObject> arrayOfFoundBTDevices;
    private BluetoothAdapter mBluetoothAdapter;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public int counter=0;
    Context mContext;
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        mContext = this;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        Log.e("TAG" ," Calling.......... onStartCommand" +Utility.getSharedPreference(getApplicationContext()).
                getBoolean(ConstantCodes.IS_DRIVING_MODE_OPEN,false));
        if(Utility.getSharedPreference(getApplicationContext()).
                getBoolean(ConstantCodes.IS_DRIVING_MODE_OPEN,false) == false){
            startTimer();
        }

        return START_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        stoptimertask();
    }
    private Timer timer;
    private TimerTask timerTask;
    long oldTime=0;
    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, to wake up every 1 second
        timer.schedule(timerTask, 10000, 3000); //
    }

    /**
     * it sets the timer to print the counter every x seconds
     */
    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
            public void run() {
                Log.e("TAG" ," Calling.......... initializeTimerTask" );
                // start looking for bluetooth devices
                displayListOfFoundDevices();
            }
        };
    }

    /**
     * not needed
     */
    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            mBluetoothAdapter.cancelDiscovery();
            Log.e("TAG" ," Calling.......... stoptimertask" );
            timer.cancel();
            timer.purge();
            timerTask.cancel();
            timer = null;
            timerTask = null;

            stopSelf();
        }
    }

    private void displayListOfFoundDevices()
    {
        arrayOfFoundBTDevices = new ArrayList<BluetoothObject>();

        // start looking for bluetooth devices
        mBluetoothAdapter.startDiscovery();

        // Discover new devices

        // Register the BroadcastReceiver
        try {
            IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            registerReceiver(mReceiver, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    final BroadcastReceiver mReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            Log.e("TAG" ,"onReceive Calling.......... initializeTimerTask" );

            String action = intent.getAction();
            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action))
            {
                // Get the bluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                // Get the "RSSI" to get the signal strength as integer,
                // but should be displayed in "dBm" units
                int rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI,Short.MIN_VALUE);

                // Create the device object and add it to the arrayList of devices
                BluetoothObject bluetoothObject = new BluetoothObject();
                bluetoothObject.setBluetooth_name(device.getName());
                bluetoothObject.setBluetooth_address(device.getAddress());
                bluetoothObject.setBluetooth_state(device.getBondState());
//                    bluetoothObject.setBluetooth_type(device.getType());    // requires API 18 or higher
                bluetoothObject.setBluetooth_uuids(device.getUuids());
                bluetoothObject.setBluetooth_rssi(rssi);

                arrayOfFoundBTDevices.add(bluetoothObject);


                ArrayList<BluetoothModel> list = new Select().
                        from(BluetoothModel.class).
                        where("address='" + bluetoothObject.getBluetooth_address() + "' AND isDefault='1'").
                        execute();


                Log.e("TAG ", bluetoothObject.getBluetooth_address()+"\n" +
                        " getName "+bluetoothObject.getBluetooth_name()+
                        " ----BluetoothModel--- "+list.size());
                if (list != null && list.size() == 0) {
                    BluetoothModel bluetoothModel = new BluetoothModel();
                    try {

                        bluetoothModel.address = bluetoothObject.getBluetooth_address();
                        bluetoothModel.name = bluetoothObject.getBluetooth_name();
                        bluetoothModel.isDefault = 0;

                        bluetoothModel.save();
                    } catch (Exception e) {
                        e.printStackTrace();
                        bluetoothModel.update(new String[]{"address", "name", "isDefault"});
                    }finally {
                        bluetoothModel = null;
                    }

                    BroadcastHelper.sendBroadcast(mContext, ConstantCodes.BROADCAST_BLUETOOTH_REFRESH);
                }else{
                    try {
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.e("TAG" ," Calling.......... getApplicationContext()" +
                            "onStartCommand---- "
                            +Utility.getSharedPreference(mContext).
                            getBoolean(ConstantCodes.IS_DRIVING_MODE_OPEN,true));

                    if(Utility.getSharedPreference(mContext).
                            getBoolean(ConstantCodes.IS_DRIVING_MODE_OPEN,false) == false){
                        ForgroundService.startServiceForDrivingMode(mContext, false, false);
                    }else{
                        return;
                    }

                }
            }
        }
    };


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    private void displayListOfPairedBluetoothDevices()
    {
        Log.e("TAG" ," Calling.......... displayListOfFoundDevices" );

        arrayOfFoundBTDevices = new ArrayList<>();

        arrayOfFoundBTDevices = getArrayOfAlreadyPairedBluetoothDevices();
        if(arrayOfFoundBTDevices.size() > 0){
            for (BluetoothObject bluetoothObject:
                 arrayOfFoundBTDevices) {
                ArrayList<BluetoothModel> list = new Select().
                        from(BluetoothModel.class).
                        where("address='" + bluetoothObject.getBluetooth_address() + "' AND isDefault='1'").
                        execute();


                Log.e("TAG ", bluetoothObject.getBluetooth_address()+"\n" +
                        " getName "+bluetoothObject.getBluetooth_name()+
                        " ----BluetoothModel--- "+list.size());
                if (list != null && list.size() == 0) {
                    BluetoothModel bluetoothModel = new BluetoothModel();
                    bluetoothModel.address = bluetoothObject.getBluetooth_address();
                    bluetoothModel.name = bluetoothObject.getBluetooth_name();
                    bluetoothModel.isDefault = 0;

                    bluetoothModel.save();

                    BroadcastHelper.sendBroadcast(mContext, ConstantCodes.BROADCAST_BLUETOOTH_REFRESH);
                }else{
//                    Runnable runnable = new Runnable() {
//                        public void run() {
//                            Toast.makeText(mContext, "FROm Service CONNECTED TO CARS's BlUETOOTH", Toast.LENGTH_LONG).show();
//                        }
//                    };
                    Log.e("TAG" ," Calling.......... onStartCommand" +Utility.getSharedPreference(getApplicationContext()).
                            getBoolean(ConstantCodes.IS_DRIVING_MODE_OPEN,false));

                    if(Utility.getSharedPreference(getApplicationContext()).
                            getBoolean(ConstantCodes.IS_DRIVING_MODE_OPEN,false) == false){
                        ForgroundService.startServiceForDrivingMode(mContext, false, false);
                    }else{
                        return;
                    }

                }
            }
        }

        // Register the BroadcastReceiver
//        IntentFilter filter = new IntentFilter();
//        filter.addAction(BluetoothDevice.ACTION_FOUND);
//        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
//        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
//        registerReceiver(mReceiver, filter);


        // Discover new devices
        // Create a BroadcastReceiver for ACTION_FOUND


    }
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    private ArrayList<BluetoothObject> getArrayOfAlreadyPairedBluetoothDevices()
    {
        ArrayList<BluetoothObject> arrayOfAlreadyPairedBTDevices = null;

        // Query paired devices
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        // If there are any paired devices
        if (pairedDevices.size() > 0)
        {
            arrayOfAlreadyPairedBTDevices = new ArrayList<BluetoothObject>();

            // Loop through paired devices
            for (BluetoothDevice device : pairedDevices)
            {
                // Create the device object and add it to the arrayList of devices
                BluetoothObject bluetoothObject = new BluetoothObject();
                bluetoothObject.setBluetooth_name(device.getName());
                bluetoothObject.setBluetooth_address(device.getAddress());
                bluetoothObject.setBluetooth_state(device.getBondState());
//                bluetoothObject.setBluetooth_type(device.getType());    // requires API 18 or higher
                bluetoothObject.setBluetooth_uuids(device.getUuids());

                arrayOfAlreadyPairedBTDevices.add(bluetoothObject);
            }
        }

        return arrayOfAlreadyPairedBTDevices;
    }

//    final BroadcastReceiver mReceiver = new BroadcastReceiver()
//    {
//        @Override
//        public void onReceive(Context context, Intent intent)
//        {
//            Log.e("TAG" ," Calling.......... displayListOfFoundDevices onReceive" );
//
//            String action = intent.getAction();
//            // When discovery finds a device
//            if (BluetoothDevice.ACTION_FOUND.equals(action))
//            {
//                // Get the bluetoothDevice object from the Intent
//                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
//
//                // Get the "RSSI" to get the signal strength as integer,
//                // but should be displayed in "dBm" units
//                int rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI,Short.MIN_VALUE);
//
//                // Create the device object and add it to the arrayList of devices
//
//                ArrayList<BluetoothModel> list = new Select().
//                        from(BluetoothModel.class).
//                        where("address='" + device.getAddress() + "' AND isDefault='1'").
//                        execute();
//
//
//                Log.e("TAG ", device.getAddress()+"\n" +
//                        " getName "+device.getName()+
//                        " ----BluetoothModel--- "+list.size());
//                if (list != null && list.size() == 0) {
//                    BluetoothModel bluetoothObject = new BluetoothModel();
//                    bluetoothObject.address = device.getAddress();
//                    bluetoothObject.name = device.getName();
//                    bluetoothObject.isDefault = 0;
//
//                    bluetoothObject.save();
//                    BroadcastHelper.sendBroadcast(mContext, ConstantCodes.BROADCAST_BLUETOOTH_REFRESH);
//                }else{
//                    Toast.makeText(mContext, "FROm Service CONNECTED TO CARS's BlUETOOTH", Toast.LENGTH_LONG).show();
//                    ForgroundService.startServiceForDrivingMode(mContext, false, false);
//                }
//
////                    bluetoothObject.setBluetooth_state(device.getBondState());
////                    bluetoothObject.setBluetooth_type(device.getType());    // requires API 18 or higher
////                    bluetoothObject.setBluetooth_uuids(device.getUuids());
////                    bluetoothObject.setBluetooth_rssi(rssi);
//
//
//
//            }
//        }
//    };

}