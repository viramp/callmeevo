package callme.evo.services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import callme.evo.R;
import callme.evo.activity.DrivingModeSummaryActivity;
import callme.evo.activity.dialog_box.drive_mode.DriveModeDialog;
import callme.evo.global.BroadcastHelper;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.DrivingModeSummary;

/**
 * Created by pankaj on 23/9/16.
 */
public class ForgroundService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    View removeView;
    BroadcastHelper broadcastHelper;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        if (intent != null && ConstantCodes.ACTION_DND.equals(intent.getAction())) {

            if (intent.getBooleanExtra(ConstantCodes.ACTION_CANCEL, false)) {

                Utility.getSharedPreference(getApplicationContext()).edit().remove(ConstantCodes.DND_TYPE_CHOISE).commit();
                Utility.getSharedPreference(getApplicationContext()).edit().remove(ConstantCodes.DND_TYPE).commit();
                Utility.getSharedPreference(getApplicationContext()).edit().remove(ConstantCodes.TIME).commit();
                stopForeground(true);
                BroadcastHelper.sendBroadcast(getApplicationContext(), ConstantCodes.BROADCAST_DND);

                Intent intentSummary = new Intent(getApplicationContext(), DrivingModeSummaryActivity.class);
                intentSummary.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intentSummary);

                if (broadcastHelper != null) {
                    broadcastHelper.unRegister();
                    broadcastHelper = null;
                }

                stopSelf();
            } else {
                Intent cancelIntent = new Intent(getApplicationContext(), ForgroundService.class);
                cancelIntent.setAction(ConstantCodes.ACTION_DND);
                cancelIntent.putExtra(ConstantCodes.ACTION_CANCEL, true);
                PendingIntent pendingIntentCancel = PendingIntent.getService(getApplicationContext(), ConstantCodes.NOTIFICATION_CANCEL_DND, cancelIntent, PendingIntent.FLAG_CANCEL_CURRENT);

                if (broadcastHelper == null) {
                    broadcastHelper = new BroadcastHelper(getApplicationContext(), ConstantCodes.BROADCAST_REFRESH);
                    broadcastHelper.register(new BroadcastReceiver() {
                        @Override
                        public void onReceive(Context context, Intent intent) {
                            if (intent.getBooleanExtra(ConstantCodes.INTENT_BLOCKED_CALL, false)) {

                                String incommingNumber = intent.getStringExtra(ConstantCodes.INTENT_NUMBER);
                                String body = intent.getStringExtra(ConstantCodes.INTENT_MESSAGE);

                                String type = intent.getStringExtra(ConstantCodes.INTENT_BLOCKED_TYPE);
                                long timestamp = System.currentTimeMillis();

                                DrivingModeSummary drivingModeSummary = new DrivingModeSummary();
                                drivingModeSummary.number = incommingNumber;
                                drivingModeSummary.type = type;
                                drivingModeSummary.timestamp = timestamp;
                                drivingModeSummary.body = body;
                                drivingModeSummary.save();

                                if (removeView != null) {
                                    TextView txtCallsReceived = (TextView) removeView.findViewById(R.id.text_calls_receive);
                                    TextView txtCallsBlocked = (TextView) removeView.findViewById(R.id.text_calls_blocked);
                                    TextView txtMessageReceived = (TextView) removeView.findViewById(R.id.text_message_received);

                                    if (ConstantCodes.CALL.equalsIgnoreCase(type))
                                        txtCallsBlocked.setText((Integer.parseInt(txtCallsBlocked.getText().toString().trim()) + 1) + "");
                                    else if (ConstantCodes.SMS.equalsIgnoreCase(type))
                                        txtMessageReceived.setText((Integer.parseInt(txtMessageReceived.getText().toString().trim()) + 1) + "");
                                }
                            }
                        }
                    });
                }

                // Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                        .setWhen(System.currentTimeMillis())
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setAutoCancel(false)
                        .setSmallIcon(R.mipmap.app_icon)
                        .setContentTitle("DO NOT DISTURB, CallMeEvo")
                        .setContentText("Do Not Disturb is Enabled")
                        .addAction(R.drawable.voicemail_close, "CANCEL", pendingIntentCancel);


                startForeground(ConstantCodes.NOTIFICATION_DND, mBuilder.build());
            }
        } else if (intent != null && ConstantCodes.ACTION_DRIVE_MODE.equals(intent.getAction())) {

            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

            boolean isRecentCreated = false;
            if (removeView == null) {
                removeView = inflater.inflate(R.layout.dialog_drivemode_2, null);
                isRecentCreated = true;
            }

            boolean isManual = intent.getBooleanExtra(ConstantCodes.IS_MANUAL, false);
            if (isManual) {
                removeView.findViewById(R.id.btn_exit).setVisibility(View.VISIBLE);
                removeView.findViewById(R.id.back).setVisibility(View.VISIBLE);
            } else {
                removeView.findViewById(R.id.btn_exit).setVisibility(View.GONE);
                removeView.findViewById(R.id.back).setVisibility(View.GONE);
            }

            final WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);

            if (intent.getBooleanExtra(ConstantCodes.ACTION_CANCEL, false)) {

                Utility.getSharedPreference(getApplicationContext()).edit().
                        putBoolean(ConstantCodes.IS_DRIVING_MODE_OPEN,false).apply();

                Log.e("TAG" ,"onCreate Calling..........onDestroy DrivingModeSummaryActivity" +
                        Utility.getSharedPreference(getApplicationContext()).
                                getBoolean(ConstantCodes.IS_DRIVING_MODE_OPEN,false));

                //code to stop Driving Mode
                stopForeground(true);
                if (removeView != null && removeView.findViewById(R.id.btn_exit) != null) {
                    Utility.getSharedPreference(getApplicationContext()).edit().remove(ConstantCodes.DRIVING_MODE_ENABLED).commit();
                    if (isRecentCreated == false) {
                        wm.removeViewImmediate(removeView);
                        //open activity from here
                        Intent intentSummary = new Intent(getApplicationContext(), DrivingModeSummaryActivity.class);
                        intentSummary.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intentSummary);
                    }
                }
                if (broadcastHelper != null) {
                    broadcastHelper.unRegister();
                    broadcastHelper = null;
                }
                stopSelf();
            } else {

                 /*@added by viram */
                Utility.getSharedPreference(getApplicationContext()).edit().
                        putBoolean(ConstantCodes.IS_DRIVING_MODE_OPEN,true).apply();

                Log.e("TAG" ,"onCreate Calling.......... DrivingModeSummaryActivity" +
                        Utility.getSharedPreference(getApplicationContext()).
                                getBoolean(ConstantCodes.IS_DRIVING_MODE_OPEN,false));

                Utility.getSharedPreference(getApplicationContext()).edit().putBoolean(ConstantCodes.DRIVING_MODE_ENABLED, true).commit();
                if (broadcastHelper == null) {
                    broadcastHelper = new BroadcastHelper(getApplicationContext(), ConstantCodes.BROADCAST_REFRESH);
                    broadcastHelper.register(new BroadcastReceiver() {
                        @Override
                        public void onReceive(Context context, Intent intent) {
                            if (intent.getBooleanExtra(ConstantCodes.INTENT_BLOCKED_CALL, false)) {

                                String incommingNumber = intent.getStringExtra(ConstantCodes.INTENT_NUMBER);
                                String body = intent.getStringExtra(ConstantCodes.INTENT_MESSAGE);

                                String type = intent.getStringExtra(ConstantCodes.INTENT_BLOCKED_TYPE);
                                long timestamp = System.currentTimeMillis();

                                DrivingModeSummary drivingModeSummary = new DrivingModeSummary();
                                drivingModeSummary.number = incommingNumber;
                                drivingModeSummary.type = type;
                                drivingModeSummary.timestamp = timestamp;
                                drivingModeSummary.body = body;
                                drivingModeSummary.save();

                                if (removeView != null) {
                                    TextView txtCallsReceived = (TextView) removeView.findViewById(R.id.text_calls_receive);
                                    TextView txtCallsBlocked = (TextView) removeView.findViewById(R.id.text_calls_blocked);
                                    TextView txtMessageReceived = (TextView) removeView.findViewById(R.id.text_message_received);

                                    if (ConstantCodes.CALL.equalsIgnoreCase(type))
                                        txtCallsBlocked.setText((Integer.parseInt(txtCallsBlocked.getText().toString().trim()) + 1) + "");
                                    else if (ConstantCodes.SMS.equalsIgnoreCase(type))
                                        txtMessageReceived.setText((Integer.parseInt(txtMessageReceived.getText().toString().trim()) + 1) + "");
                                }
                            }
                        }
                    });
                }
                removeView.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utility.getSharedPreference(getApplicationContext()).edit().remove(ConstantCodes.DRIVING_MODE_ENABLED).commit();
                        stopForeground(true);
                        wm.removeViewImmediate(removeView);
                        stopSelf();

                        Intent intentSummary = new Intent(getApplicationContext(), DriveModeDialog.class);
                        intentSummary.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intentSummary);
                    }
                });
                removeView.findViewById(R.id.btn_exit).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utility.getSharedPreference(getApplicationContext()).edit().remove(ConstantCodes.DRIVING_MODE_ENABLED).commit();
                        stopForeground(true);
                        wm.removeViewImmediate(removeView);
                        stopSelf();

                        Intent intentSummary = new Intent(getApplicationContext(), DrivingModeSummaryActivity.class);
                        intentSummary.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intentSummary);
                    }
                });
                WindowManager.LayoutParams paramRemove = new WindowManager.LayoutParams(
                        WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.TYPE_PHONE,
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                        PixelFormat.TRANSLUCENT);
                paramRemove.gravity = Gravity.TOP | Gravity.LEFT;

                //Trying to solve has already been added to the window manager.
                try {
                    wm.removeViewImmediate(removeView);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if (removeView.getParent() != null) {
                        ((ViewGroup) removeView.getParent()).removeView(removeView);
                    }
                    wm.addView(removeView, paramRemove);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Intent cancelIntent = new Intent(getApplicationContext(), ForgroundService.class);
                cancelIntent.setAction(ConstantCodes.ACTION_DRIVE_MODE);
                cancelIntent.putExtra(ConstantCodes.ACTION_CANCEL, true);
                PendingIntent pendingIntentCancel = PendingIntent.getService(getApplicationContext(), ConstantCodes.NOTIFICATION_CANCEL_DRIVE_MODE, cancelIntent, PendingIntent.FLAG_CANCEL_CURRENT);

                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                        .setWhen(System.currentTimeMillis())
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setAutoCancel(false)
                        .setSmallIcon(R.mipmap.app_icon)
                        .setContentTitle("Driving Mode, CallMeEvo")
                        .setContentText("Driving Mode is Enabled")
                        .addAction(R.drawable.voicemail_close, "CANCEL", pendingIntentCancel);


                startForeground(ConstantCodes.NOTIFICATION_DRIVE_MODE, mBuilder.build());
            }
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (broadcastHelper != null) {
            broadcastHelper.unRegister();
        }
        broadcastHelper = null;
    }

    public static void startServiceForDND(Context context, boolean isStop) {
        Intent intent = new Intent(context, ForgroundService.class);
        intent.setAction(ConstantCodes.ACTION_DND);
        if (isStop) {
            intent.putExtra(ConstantCodes.ACTION_CANCEL, true);
        }
        context.startService(intent);

        BroadcastHelper.sendBroadcast(context, ConstantCodes.BROADCAST_DND);
    }

    public static void startServiceForDrivingMode(Context context, boolean isStop, boolean isManual) {
        Intent intent = new Intent(context, ForgroundService.class);
        intent.setAction(ConstantCodes.ACTION_DRIVE_MODE);
        if (isStop) {
            intent.putExtra(ConstantCodes.ACTION_CANCEL, true);
        } else {
            intent.putExtra(ConstantCodes.IS_MANUAL, isManual);
        }
        context.startService(intent);
    }
}