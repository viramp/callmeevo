package callme.evo.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by pankaj on 1/12/16.
 */
@Table(name = "DrivingModeSummary")
public class DrivingModeSummary extends Model {

    @Column
    public String number;

    @Column
    public String type; //sms, call

    @Column
    public Long timestamp;

    @Column
    public String body;
}
