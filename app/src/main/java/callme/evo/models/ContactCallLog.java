package callme.evo.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by pankaj on 25/8/16.
 */

@Table(name = "ContactCallLog")
public class ContactCallLog extends Model {

    @Column(unique = true, onUniqueConflicts = Column.ConflictAction.REPLACE)
    public long callLogId;

    @Column
    public String name;

    @Column
    public String number;

    @Column
    public String cleanNumber;

    @Column
    public String message;

    @Column
    public String type;

    @Column
    public long timestamp;
    @Column
    public long duration;

    public String blackListOrWhiteList;
    public String thumbnailImage;

    /*public ContactCallLog(Contacts contacts) {
        this.name = contacts.name;
        this.number = contacts.number;
        this.timestamp = contacts.timestamp;
    }*/
}
