package callme.evo.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by pankaj on 15/12/16.
 */
@Table(name = "BluetoothModel")
public class BluetoothModel extends Model {

    @Column(unique = true, onUniqueConflicts = Column.ConflictAction.IGNORE)
    public String address;
    @Column
    public String name;
    @Column
    public int isDefault = 0;
}
