package callme.evo.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by pankaj on 2/10/16.
 */
@Table(name = "SpamNumbers")
public class SpamNumbers extends Model {

    @Column(unique = true, onUniqueConflicts = Column.ConflictAction.IGNORE)
    public String number;
    @Column
    public String type; //hidden, spam
}
