package callme.evo.models;

import android.database.sqlite.SQLiteDatabase;

import com.activeandroid.Cache;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.ArrayList;

/**
 * Created by pankaj on 8/10/16.
 */
@Table(name = "Sms")
public class Sms extends Model {

    @Column(unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public long smsId;
    @Column
    public long threadId;
    @Column
    public String name;
    @Column
    public long date;
    @Column
    public int isRead;
    @Column
    public String number;
    @Column
    public String cleanNumber;
    @Column
    public String blackListOrWhiteList;

    public String thumbnailImage;
    @Column
    public String message;
    @Column
    public int priority;
//    public String readStatus;
//    public String time;
//    public String folderName;


    @Override
    public String toString() {
        return "Sms{" +
                "smsId=" + smsId +
                ", threadId=" + threadId +
                ", name='" + name + '\'' +
                ", date=" + date +
                ", number='" + number + '\'' +
                ", cleanNumber='" + cleanNumber + '\'' +
                ", blackListOrWhiteList='" + blackListOrWhiteList + '\'' +
                ", thumbnailImage='" + thumbnailImage + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

    @Override
    public Long save() {
        final SQLiteDatabase db = Cache.openDatabase();
        ArrayList<Sms> list = new Select().from(Sms.class).where("smsId='" + smsId + "'").execute();
        if (list.size() > 0) {
            String query = "UPDATE Sms " +
                    "SET " +
                    "  name=\"" + (name!=null?name.replace("\"", "\"\""):"") + "\"" +
                    ", threadId='" + threadId + "'" +
                    ", date='" + date + "'" +
                    ", message=\"" + (message!=null?message.replace("\"", "\"\""):"") + "\"" +
                    ", number='" + number + "'" +
                    (isRead == 0 ? ", isRead='0'" : "") +
                    ", cleanNumber='" + cleanNumber + "'" +
                    ", blackListOrWhiteList='" + blackListOrWhiteList + "'" +
                    " WHERE " +
                    "  smsId='" + smsId + "'";
            db.execSQL(query);
            return 0l;
        } else {
            return super.save();
        }
    }


}