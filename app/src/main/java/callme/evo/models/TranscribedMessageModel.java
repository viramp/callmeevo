package callme.evo.models;

import org.json.JSONException;
import org.json.JSONObject;

import callme.evo.global.DateHelper;

/**
 * Created by pankaj on 9/10/16.
 */
public class TranscribedMessageModel {
    public String iScriptID;
    public String iUserID;
    public String eType;
    public String tScript;
    public String phone_number;
    public String dAddedDate;
    public String isHelpfull;
    public String name;
    public String date;
    public String time;
    public int priority;

    public TranscribedMessageModel() {

    }

    public TranscribedMessageModel(JSONObject jsonTranscribedMessage) throws JSONException {
        iScriptID = jsonTranscribedMessage.getString("iScriptID");
        iUserID = jsonTranscribedMessage.getString("iUserID");
        eType = jsonTranscribedMessage.getString("eType");
        tScript = jsonTranscribedMessage.getString("tScript");
//        phone_number = jsonTranscribedMessage.getString("phone_number");
        phone_number = jsonTranscribedMessage.getString("vPhone");
        dAddedDate = jsonTranscribedMessage.getString("dAddedDate");
        isHelpfull = jsonTranscribedMessage.getString("is_helpful");

        String dateTime = jsonTranscribedMessage.getString("dAddedDate");
        date = DateHelper.getFormatedDateFromServer(dateTime, DateHelper.DATE_FORMAT_DATE);
        time = DateHelper.getFormatedDateFromServer(dateTime, DateHelper.DATE_FORMAT_TIME);

    }
}
/*"iScriptID": "11",
        "iUserID": "5",
        "eType": "male",
        "tScript": "i am calling you",
        "phone_number": "+918460479175",
        "dAddedDate": "2016-10-09 12:13:58"*/
