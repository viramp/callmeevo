package callme.evo.models;


import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by pankaj on 6/10/16.
 */

@Table(name = "VoiceMessageModel")
public class VoiceMessageModel extends Model {

    @Column(unique = true, onUniqueConflicts = Column.ConflictAction.REPLACE)
    public String voiceMessageId;
    @Column
    public String title;
    @Column
    public String message;
}

