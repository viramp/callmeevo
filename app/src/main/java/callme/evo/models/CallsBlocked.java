package callme.evo.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by pankaj on 8/10/16.
 */
@Table(name = "CallsBlocked")
public class CallsBlocked extends Model {

    @Column
    public String name;

    @Column
    public String number;

    @Column
    public Long timestamp;

    @Column
    public String blackListOrWhiteList;

}
