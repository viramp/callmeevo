package callme.evo.models;

import android.text.TextUtils;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "Contacts")
public class Contacts extends Model {

    @Column(unique = true, onUniqueConflicts = Column.ConflictAction.REPLACE)
    public long contactId;

    @Column
    public String name;

    @Column
    public String number;

    @Column
    public String cleanNumber;

    @Column
    public int priority;

    @Column
    public long timestamp;

    @Column
    public long duration;

    public String email;

    @Column
    public String block_type;

    @Column
    public String blackListOrWhiteList;

    @Column
    public String thumbnailImage;

    @Column
    public String photoImage;

    @Column
    public String voiceMessageId;

    @Column
    public long deleteFlag;

    @Override
    public String toString() {
        return name + (TextUtils.isEmpty(number) ? "" : " [" + number + "]");
    }

    @Override
    public Long save() {
//        cleanNumber = PhoneUtils.getProperContact(Application.getContext(), cleanNumber);
        return super.save();
    }

    //used to store header
    public String header;

    //used to store message;
    public String testMessage;

    public Contacts() {

    }

    public Contacts(String header) {
        this.header = header;
    }
}