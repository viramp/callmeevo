package callme.evo.models;

import android.text.TextUtils;

/**
 * Created by pankaj on 25/8/16.
 */
public class User {

    public String iUserID;
    public String vUserName;
    public String vEmail;
    public String vPhone;
    public String vHmac;
    public String eType;
    //    public int isPaidApplication; //1==true 0==false
    public String voiceType = "male"; //1==true 0==false
    public String activation_code;
    public int token_varified;
    public String referal_code;

    public String vProfilePic;

    public String expired_date;

    public String getExpiredDate() {
        if (!TextUtils.isEmpty(expired_date)) {
            return expired_date.split(" ")[0];
        }
        return expired_date;
    }

    //Current status of auto-renuwal, 1 or 0
    public int auto_renewel;

    //whether user any time payment is success or not , "success" "failed"
    public String ePaymentStatus;


    /*3 cheeze(flags) hai
    1. ePaymentStatus = 1 bhi bar auto renewal successful ho gaya to ye hamesha  "success" hi rahega
    2. auto_renewel =   user ka current auto-renewal status , user renewal cancel ya opt in karta hai to sirf iski value change hogi.
    3. eType          = ye status "paid" ya free, agar usne cancel auto renewal kiya to ye CHANGE NAHI HOGA, iski value keval time expire ho jaye tab hogi.*/

    //Returns is Free Application(Full Feature) true
    public boolean isPaidApplication() {
        return "free".equalsIgnoreCase(eType) == false;
    }

    public void setIsPaidApplication(boolean isPaidApplication) {
        if (isPaidApplication) { //we are opositing values
            eType = "paid";
        } else
            eType = "free";
    }
}
