package callme.evo.global;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.activeandroid.Cache;
import com.activeandroid.app.Application;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.moez.QKSMS.data.Contact;
import com.moez.QKSMS.data.Conversation;
import com.moez.QKSMS.data.RecipientIdCache;
import com.moez.QKSMS.transaction.SmsHelper;

import org.ispeech.SpeechSynthesis;
import org.ispeech.SpeechSynthesisEvent;
import org.ispeech.error.BusyException;
import org.ispeech.error.InvalidApiKeyException;
import org.ispeech.error.NoNetworkException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Modifier;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import callme.evo.R;
import callme.evo.models.ContactCallLog;
import callme.evo.models.Contacts;
import callme.evo.models.Sms;
import callme.evo.models.User;

/**
 * Created by pankaj on 31/7/16.
 */
public class Utility {

    /*private void readAllContacts() {
        new AsyncTask<Void, Void, ArrayList<Contacts>>() {
            @Override
            protected ArrayList<Contacts> doInBackground(Void... params) {
                ArrayList<Contacts> list = Utility.readContacts(AddtoBlocklistManually.this);
                return list;
            }

            @Override
            protected void onPostExecute(ArrayList<Contacts> list) {
                super.onPostExecute(list);
            }
        }.execute();
    }*/
    public static void delete(Context context, long id) {
//        Uri uri = Uri.withAppendedPath(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, id + "");
//        context.getContentResolver().delete(uri, null, null);

        ContentResolver contentResolver = context.getContentResolver();
        Cursor cur = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.Contacts._ID + "="
                + id, null, null);

        while (cur.moveToNext()) {
            try {
                Cursor cc = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID)), null, null);
                if (cc.getCount() <= 1) {
                    String lookupKey = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.LOOKUP_KEY));
                    Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, lookupKey);
                    contentResolver.delete(uri, ContactsContract.Contacts._ID + "=" + id, null);
                } else {
                    Uri uri = Uri.withAppendedPath(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, id + "");
                    context.getContentResolver().delete(uri, null, null);
                }

            } catch (Exception e) {
                System.out.println(e.getStackTrace());
            }
        }
        cur.close();
    }

    public static ArrayList<Contacts> readContacts(Context activity) {
        Log.d("READ_CONTACT", "readContacts() called with: " + "activity = [" + activity + "]");
        ArrayList<Contacts> list = new ArrayList<>();
        Cursor phones = activity.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                null,
                null,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE NOCASE ASC");

//        Cursor phones = activity.getContentResolver().query(
// ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null,
// ContactsContract.CommonDataKinds.Phone.NUMBER + " DESC");
        Log.d("READ_CONTACT", "readContacts() " + phones.getCount());
        long lastId = 0;

        String lastNumbers = "";
        while (phones.moveToNext()) {

            long contact_id = phones.getLong(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
            long id = phones.getLong(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID));
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            if (lastNumbers.contains(Utility.getFinalNumber(phoneNumber))) {
                continue;
            }
            lastNumbers += Utility.getFinalNumber(phoneNumber) + ",";

//            Log.i("CONTACTSYNC", name + "=" + phoneNumber);

            Contacts contact = new Contacts();
            contact.contactId = phones.getLong(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID));
            contact.name = name;
            contact.number = Utility.getFinalNumber(phoneNumber);
            contact.cleanNumber = Utility.getFinalNumber(phoneNumber);
            contact.thumbnailImage = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI));
            contact.photoImage = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
            contact.email = "";
            list.add(contact);
        }
        phones.close();
        Log.d("READ_CONTACT", "actual readContacts() " + list.size());
        return list;
    }

    public static <T> Object getValueIfNull(T value, T defaultValue) {
        return value == null ? defaultValue : value;
    }

    public static void setAlarm(Context context, long triggerAtMillis, PendingIntent pendingIntent) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (Build.VERSION.SDK_INT < 23) {
            if (Build.VERSION.SDK_INT >= 19) {
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, triggerAtMillis, pendingIntent);
            } else {
                alarmManager.set(AlarmManager.RTC_WAKEUP, triggerAtMillis, pendingIntent);
            }
        } else {
            //TODO: Comment For Previous Version
//            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, triggerAtMillis, pendingIntent);

        }
    }

    public static boolean compareTwoNumbers(String number1, String number2) {
        String num1 = number1.replaceAll("[^0-9]", "");
        String num2 = number2.replaceAll("[^0-9]", "");
        return num1.contains(num2) || num2.contains(num1);
//        return number1.replaceAll("[^0-9]", "").equals(number2.replaceAll("[^0-9]", ""));
    }

    public static boolean isInternetUnavailableMsg(String message) {
        if (message != null && message.contains("UnknownHostException")) {
            return true;
        }
        return false;
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
//            return true;
        }
    }

    public static String getFinalNumber(String number) {
//        return number;
        if (number != null) {
            if (number.matches(".*[a-zA-Z]+.*"))
                return number;
            String str = number.replaceAll("[^0-9]", "");
            if (str.length() > 0) {
                return str.replaceFirst("^0", "");
            }
        }
        return number;
//        return getLast(str);
//        return getLast(str);
//        return number; 9173889837
    }

    public static String getOnlyDigitNumber(String number) {
        if (number != null) {
            String str = number.replaceAll("[^0-9]", "");
            return str;
        }
        return number;
    }

    public static String getLast1(String myString) {
        if (myString.length() > 10)
            return myString.substring(myString.length() - 10);
        else
            return myString;
    }

    public static SharedPreferences getSharedPreference(Context context) {
        return context.getSharedPreferences("PREF", Context.MODE_PRIVATE);
    }

    public static String getHrsMinsSec(double secondsCompleted, String format) {
        long hours = (long) (secondsCompleted / 3600);
        long minutes = (long) ((secondsCompleted % 3600) / 60);
        long seconds = (long) (secondsCompleted % 60);

        format = format.replaceAll("hh", String.format("%02d", hours));
        format = format.replaceAll("mm", String.format("%02d", minutes));
        format = format.replaceAll("ss", String.format("%02d", seconds));

        format = format.replaceAll("h", String.format("%01d", hours));
        format = format.replaceAll("m", String.format("%01d", minutes));
        format = format.replaceAll("s", String.format("%01d", seconds));

        return format;

//        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }


    //=========================================
    //================WEB======================
    //=========================================
    public static HashMap<String, String> getDefaultParams() {
        HashMap<String, String> params = new HashMap<>();
        params.put(ConstantCodes.vDeviceToken, "4f5f6f4dd4564f6f464s6546");
        params.put(ConstantCodes.ePlatform, "Android");
        return params;
    }

    public static HashMap<String, String> getDefaultHeaders() {
        HashMap<String, String> params = new HashMap<>();
        params.put("X-API-KEY", "f2278baf7e70c1e9d23615581a5f27f043a03482");
        return params;
    }

    private static RequestQueue requestQueue;

    public static RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(Application.getContext());
        }
        return requestQueue;
    }


    //GSON
    private static Gson gson;

    public static Gson getGson() {
        if (gson == null) {
            gson = new GsonBuilder()
                    .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                    .serializeNulls()
                    .create();
        }
        return gson;
    }

    public static <T> ArrayList<T> parseArrayFromString(String jsonData, Class modelClass) {
        if (gson == null) {
            gson = new GsonBuilder()
                    .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                    .serializeNulls()
                    .create();
        }

        return new ArrayList<T>(Arrays.asList((T[]) gson.fromJson(jsonData, modelClass)));

    }

    public static Object parseFromString(String jsonData, Class modelClass) {
        if (gson == null) {
            gson = new GsonBuilder()
                    .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                    .serializeNulls()
                    .create();
        }
        return gson.fromJson(jsonData, modelClass);
    }

    public static String getJsonString(Object modelClass) {
        if (gson == null) {
            gson = new GsonBuilder()
                    .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                    .serializeNulls()
                    .create();
        }
        return gson.toJson(modelClass);
    }

    private static User user;

    public static void saveLoggedInUser(Context context, User user) {
        Utility.user = user;
        Utility.getSharedPreference(context).edit().putString(ConstantCodes.USER_DATA, Utility.getJsonString(user)).commit();
    }

    public static void logoutAndRemoveLoggedInUser(Context context) {
        user = null;
        Utility.getSharedPreference(context).edit().remove(ConstantCodes.USER_DATA).commit();
    }

    public static User getLoggedInUser(Context context) {
        if (user == null) {
            if (Utility.getSharedPreference(context).contains(ConstantCodes.USER_DATA))
                user = (User) parseFromString(Utility.getSharedPreference(context).getString(ConstantCodes.USER_DATA, ""), User.class);
        }
        return user;
    }

    public static String getBackgroundUrl(Context context) {
        return Utility.getSharedPreference(context).getString(ConstantCodes.BACKGROUND_URL, "");
    }

    public static void saveBackgroundUrl(Context context, String backgroundUrl) {
        Utility.getSharedPreference(context).edit().putString(ConstantCodes.BACKGROUND_URL, backgroundUrl).commit();
    }

    public static void showCircularImageView(Context context, ImageView img, String url) {
//    public static void showCircularImageView(Context context, ImageView img, String url) {
        /*Glide
                .with(context)
                .load(url)
                .placeholder(placeholder)
                .centerCrop()
                .crossFade()
                .into(img);*/

        Glide
                .with(context)
                .load(url)
                .transform(new CircleTransform(context))
                .placeholder(R.drawable.ph_contact)
//                    .centerCrop()
                .error(R.drawable.ph_contact)
                .crossFade()
                .into(img);
    }

    public static void showCircularImageView(Context context, ImageView img, Uri url) {
//    public static void showCircularImageView(Context context, ImageView img, String url) {
        /*Glide
                .with(context)
                .load(url)
                .placeholder(placeholder)
                .centerCrop()
                .crossFade()
                .into(img);*/

        Glide
                .with(context)
                .load(url)
                .transform(new CircleTransform(context))
                .placeholder(R.drawable.ph_contact)
//                    .centerCrop()
                .error(R.drawable.ph_contact)
                .listener(new RequestListener<Uri, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, Uri model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, Uri model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        return false;
                    }
                })
                .crossFade()
                .into(img);
    }

    public static ProgressDialog progressDialog(Context mContext, String message) {
        ProgressDialog progressDialog = new ProgressDialog(mContext);
        progressDialog.setMessage(message);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        return progressDialog;
    }

    public static void saveLastCallLogInOurDatabase(Context context) {
        Cursor managedCursor = context.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, CallLog.Calls.DATE + " DESC LIMIT 1");
        if (managedCursor != null && managedCursor.moveToFirst()) {

            int idColumn = managedCursor.getColumnIndex(CallLog.Calls._ID);
            int name = managedCursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
            int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);

            int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
            int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
            int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);

            long id = managedCursor.getLong(idColumn);
            String phName = managedCursor.getString(name);
            String phNumber = managedCursor.getString(number);
            String callType = managedCursor.getString(type);
            String callDate = managedCursor.getString(date);
            Date callDayTime = new Date(Long.valueOf(callDate));
            String callDuration = managedCursor.getString(duration);
            String dir = null;
            int dircode = Integer.parseInt(callType);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "OUTGOING";
                    break;

                case CallLog.Calls.INCOMING_TYPE:
                    dir = "INCOMING";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "MISSED";
                    break;
            }
            ContactCallLog contacts = new ContactCallLog();
            try {

                contacts.callLogId = id;
                contacts.name = phName;
                contacts.number = phNumber;
                contacts.cleanNumber = Utility.getFinalNumber(phNumber);
                contacts.timestamp = Long.valueOf(callDate);
                contacts.save();
            } catch (NumberFormatException e) {
                e.printStackTrace();
                contacts.update(new String[]{"callLogId","name", "number", "cleanNumber","timestamp"});
            }
        }
    }

    public static void updateReadStatus(Context context, String threadId) {
        String query = "UPDATE Sms " +
                "SET " +
                "  isRead=1" +
                " WHERE " +
                "  smsId='" + threadId + "'";
        Cache.openDatabase().execSQL(query);
        BroadcastHelper.sendBroadcast(context, ConstantCodes.BROADCAST_READ_FLAG);
    }

    public static void saveLastSmsToOurDatabase(Context context, String snippet) {


        ContentResolver cr = context.getContentResolver();
        Cursor data = cr.query(SmsHelper.CONVERSATIONS_CONTENT_PROVIDER, Conversation.ALL_THREADS_PROJECTION, null, null, "date DESC");//

        if (data != null && data.moveToFirst()) {


                    /*for (String columnName : data.getColumnNames()) {
                        Log.e("SMS==", columnName + " = " + data.getString(data.getColumnIndex(columnName)));
                    }*/

            Sms sms = new Sms();
            sms.smsId = data.getLong(data.getColumnIndex("_id"));
            sms.threadId = data.getLong(data.getColumnIndex("_id"));
            sms.isRead = 0;

            if (!TextUtils.isEmpty(data.getString(data.getColumnIndex("snippet"))))
                sms.message = data.getString(data.getColumnIndex("snippet"));
            else
                sms.message = snippet;


            sms.date = data.getLong(data.getColumnIndex("date"));

            try {
                String spaceSepIds = data.getString(data.getColumnIndex("recipient_ids"));

                for (RecipientIdCache.Entry entry : RecipientIdCache.getAddresses(spaceSepIds)) {
                    if (entry != null && !TextUtils.isEmpty(entry.number)) {
                        Contact contact = Contact.get(entry.number, false);
                        contact.setRecipientId(entry.id);
                        if (contact.isNamed())
                            sms.name = contact.getName();
                        sms.number = Utility.getFinalNumber(contact.getNumber());
                        sms.thumbnailImage = contact.getPhoneUri() + "";
                        contact.getName();
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.e("READ_SMS", sms.toString());
            android.util.Log.d("READ_SMS", "save conversation+" + sms.date);

            sms.save();

        }


        /*Uri message = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            message = Telephony.Sms.Inbox.CONTENT_URI;
        } else {
            message = Uri.parse("content://sms/inbox");
        }

        String[] columns = new String[]{Telephony.Sms._ID, Telephony.Sms.PERSON, Telephony.Sms.ADDRESS, Telephony.Sms.BODY, Telephony.Sms.DATE};

        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(message, columns, null, null, Telephony.Sms.DATE + " DESC LIMIT 1");
        if (c != null && c.moveToFirst()) {
            int idPos = c.getColumnIndex(Telephony.Sms._ID);
            int namePos = c.getColumnIndex(Telephony.Sms.PERSON);
            int addressPos = c.getColumnIndex(Telephony.Sms.ADDRESS);
            int bodyPos = c.getColumnIndex(Telephony.Sms.BODY);
            int datePos = c.getColumnIndex(Telephony.Sms.DATE);

            Sms objSms = new Sms();
            objSms.smsId = c.getLong(idPos);
            objSms.number = Utility.getFinalNumber(c.getString(addressPos));
            objSms.cleanNumber = Utility.getFinalNumber(c.getString(addressPos));
            objSms.name = c.getString(namePos);
            objSms.message = c.getString(bodyPos);
            objSms.date = c.getLong(datePos);
            android.util.Log.d("READ_CONTACT", "save conversation+" + objSms.date);
            objSms.save();
        }*/
    }

    public static String arrayToCSV(String[] name) {
        StringBuilder sb = new StringBuilder();
        for (String n : name) {
            if (sb.length() > 0) sb.append(',');
            sb.append("").append(n).append("");
        }
        return sb.toString();
    }

    public static void saveMessageInCallMeEvoDb(Context context, long threadId, String body, String[] spaceSepIds) {


        Sms sms = new Sms();
        sms.smsId = threadId;
        sms.threadId = threadId;
        sms.message = body;
        sms.date = System.currentTimeMillis();
        sms.isRead = 1; //1 means normal

        try {
//                String spaceSepIds = data.getString(data.getColumnIndex("recipient_ids"));

            String names = "";
            String arr = arrayToCSV(spaceSepIds);
            for (RecipientIdCache.Entry entry : RecipientIdCache.getAddresses(arr)) {
                if (entry != null && !TextUtils.isEmpty(entry.number)) {
                    Contact contact = Contact.get(entry.number, false);
                    contact.setRecipientId(entry.id);
                    if (contact.isNamed()) {
//                        sms.name = contact.getName();
                        names += contact.getName() + ",";
                    }
                    sms.number = Utility.getFinalNumber(contact.getNumber());
                    sms.thumbnailImage = contact.getPhoneUri() + "";
                    /*Added by Viram Purohit*/
//                    ArrayList<Contacts> item = new Select().from(Contacts.class).
//                            where("number='" + sms.number + "'").execute();
//                    if (item.size() > 0) {
//                        //update
//                        sms.priority = item.get(0).priority;
//                    }

//                    break;
                }
            }
            sms.name = names.replaceAll(",$", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        sms.number = Utility.getFinalNumber(spaceSepIds[0]);
        Log.e("READ_SMS", sms.toString());
        android.util.Log.d("READ_SMS", "save conversation+" + sms.date);
        sms.save();

    }

    public static boolean isPaidApplication(Context context) {
        return Utility.getLoggedInUser(context) != null && Utility.getLoggedInUser(context).isPaidApplication();
//        return false;
    }

    public static void deleteCallLog(FragmentActivity activity, long callLogId, String number) {
        Uri uri = Uri.withAppendedPath(CallLog.Calls.CONTENT_URI, "");
        ContentResolver cr = activity.getContentResolver();
//        cr.delete(uri, CallLog.Calls._ID + "=" + callLogId, null);

//        cr.delete(uri, CallLog.Calls.NUMBER + " LIKE '%" + number + "'", null);
        cr.delete(android.provider.CallLog.Calls.CONTENT_URI,"number = "+ number,null);


    }

    public static void deleteSms(FragmentActivity activity, long smsId) {


        Uri uriToDel = Uri.withAppendedPath(SmsHelper.CONVERSATIONS_CONTENT_PROVIDER, smsId + "");
        ContentResolver cr = activity.getContentResolver();
        cr.delete(uriToDel, null, null);

        /*Uri message = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            message = Telephony.MmsSms.CONTENT_CONVERSATIONS_URI;//Telephony.Sms.CONTENT_URI
        } else {
            message = Uri.parse("content://mms-sms/conversations/"); //content://sms/
        }

        Uri uri = Uri.withAppendedPath(message, "");
        ContentResolver cr = activity.getContentResolver();

        Uri uriToDel = Uri.withAppendedPath(message, smsId + "");
        //way1
        int no = cr.delete(uriToDel, null, null);
        //way2
        no = cr.delete(uriToDel, Telephony.Sms._ID + "=" + smsId, null);
        no = 3;
        Cursor cursor = cr.query(uri, null, Telephony.Sms._ID + "=" + smsId, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            cursor.getCount();
            long threadId = cursor.getLong(cursor.getColumnIndex("thread_id"));
            uriToDel = Uri.withAppendedPath(message, threadId + "");
            //way3
            cr.delete(uriToDel, null, null);
        }*/
    }

    //TTS
    private static SpeechSynthesis synthesis;

    public static void speak(Context context, String voiceType, String message) {
        if (TextUtils.isEmpty(message)) {
            Toast.makeText(context, "Can't speak empty message", Toast.LENGTH_LONG).show();
            return;
        }
        if (synthesis == null) {
            return;
        }
        try {
            if (ConstantCodes.VOICE_FEMALE.equalsIgnoreCase(voiceType)) {
                synthesis.setVoiceType("usenglishfemale"); //usenglishfemale
            } else {
                synthesis.setVoiceType("usenglishmale"); //usenglishfemale
            }
            synthesis.speak(message);
        } catch (BusyException e) {
            android.util.Log.e("SPEECH", "SDK is busy");
            e.printStackTrace();
            Toast.makeText(context, "ERROR: SDK is busy", Toast.LENGTH_LONG).show();
        } catch (NoNetworkException e) {
            android.util.Log.e("SPEECH", "Network is not available\n" + e.getStackTrace());
            Toast.makeText(context, "ERROR: Network is not available", Toast.LENGTH_LONG).show();
        }
    }

    public static void prepareTTSEngine(final Activity context) {
        try {
            synthesis = SpeechSynthesis.getInstance(context);

            synthesis.setSpeechSynthesisEvent(new SpeechSynthesisEvent() {

                public void onPlaySuccessful() {
                    android.util.Log.i("Speech", "onPlaySuccessful");
                }

                public void onPlayStopped() {
                    android.util.Log.i("Speech", "onPlayStopped");
                }

                public void onPlayFailed(Exception e) {
                    android.util.Log.e("Speech", "onPlayFailed");
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage(Html.fromHtml("<font color='black'>" +
                            "Error[TTSActivity]: " + e.toString() +
                            "</font>"))
//                    builder.setMessage("Error[TTSActivity]: " + e.toString())
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            });
                    AlertDialog alert = builder.create();
                    if(!context.isFinishing()){
                        alert.show();
                    }

                }

                public void onPlayStart() {
                    android.util.Log.i("Speech", "onPlayStart");
                }

                @Override
                public void onPlayCanceled() {
                    android.util.Log.i("Speech", "onPlayCanceled");
                }
            });
            synthesis.setStreamType(AudioManager.STREAM_MUSIC);
        } catch (InvalidApiKeyException e) {
            android.util.Log.e("Speech", "Invalid API key\n" + e.getStackTrace());
            Toast.makeText(context, "ERROR: Invalid API key", Toast.LENGTH_LONG).show();
        }

    }

    public static boolean isSecondTime(Context context) {
        boolean isSecondTime = Utility.getSharedPreference(context).getBoolean(ConstantCodes.IS_SECOND_TIME, false);
        Utility.getSharedPreference(context).edit().putBoolean(ConstantCodes.IS_SECOND_TIME, true).commit();
        return isSecondTime;
    }

    /*@author Viram Purohit*/
    public static String changeDateFormat(String webDate){
        String newDate = "";
        if((webDate != null) && (!webDate.isEmpty())){
            SimpleDateFormat dateFormat_web = new SimpleDateFormat("yyyy-mm-dd", Locale.US);
            Date date = null;

            try {
                date = dateFormat_web.parse(webDate);
                SimpleDateFormat dateFormat_require =new SimpleDateFormat("mm-dd-yyyy",Locale.US);
                newDate =dateFormat_require.format(date.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return newDate;
    }
    /*Added by Viram Purohit*/
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager)context. getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}
