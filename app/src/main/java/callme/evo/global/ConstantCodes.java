package callme.evo.global;

/**
 * Created by pankaj on 31/7/16.
 */
public class ConstantCodes {

    public static final int TRUE = 1;
    public static final int FALSE = 0;

    public static final String SPAM = "spam";
    public static final String HIDDEN = "hidden";

    public static final String BLOCK_TYPE_CALLS = "BLOCK_CALLS";
    public static final String BLOCK_TYPE_MESSAGE = "BLOCK_MESSAGE";
    public static final String BLOCK_TYPE_BOTH = "BLOCK_BOTH";

    public static final String CONTACT = "CONTACT";

    public static final String WHITE_LIST = "WHITE_LIST";
    public static final String BLACK_LIST = "BLACK_LIST";

    public static final String INTENT_FROM_MESSAGE = "intent_from_message";
    public static final String INTENT_NAME = "intent_name";
    public static final String INTENT_NUMBER = "intent_number";
    public static final String INTENT_FROM = "intent_from";
    public static final String INTENT_TO_SCREEN = "intent_to_screen";
    public static final String INTENT_PRIORITY = "intent_priority";
    public static final String INTENT_BLOCKED_CALL = "intent_blocked_call";
    public static final String INTENT_BLOCK_FOR = "intent_blocked_call";
    public static final String INTENT_BLOCKED_TYPE = "intent_blocked_type";
    public static final String CALL = "CALL";
    public static final String SMS = "SMS";
    public static final String INTENT_AFTER_LOGIN = "intent_after_login";


    public static final String BROADCAST_CONTACT_REFRESH = "broadcast_contact_refresh";
    public static final String BROADCAST_REFRESH = "broadcast_refresh";
    public static final String BROADCAST_BLOCKED = "broadcast_blocked";
    public static final String BROADCAST_BLUETOOTH_REFRESH = "broadcast_bluetooth_refresh";
    public static final String BROADCAST_READ_FLAG = "broadcast_read_flag";

    public static final String BROADCAST_SMS_RECEIVED = "broadcast_sms_received";
    public static final String BROADCAST_DND = "broadcast_dnd";
    public static final String SUCCESS = "SUCCESS";
    public static final String MESSAGE = "MESSAGE";
    public static final String DND_TYPE = "DND_TYPE";
    public static final String DND_TYPE_CHOISE = "DND_TYPE_CHOISE";

    public static final String DRIVING_MODE_ENABLED = "DRIVING_MODE_ENABLED";
    public static final String IS_DRIVING_MODE_OPEN = "IS_DRIVING_MODE_OPEN";

    public static final String DND_ALLOW_PRIORITY_CONTACT = "DND_ALLOW_PRIORITY_CONTACT";

    public static final String DRIVE_MODE_INTERCEPT_CALLS_SMS = "DRIVE_MODE_INTERCEPT_CALLS_SMS";
    public static final String DRIVE_MODE_WHITELIST_CALLS = "DRIVE_MODE_WHITELIST_CALLS";
    public static final String DRIVE_MODE_UNKNOWN_CALLS = "DRIVE_MODE_UNKNOWN_CALLS";
    public static final String DRIVE_MODE_UNWANTED_CALLS = "DRIVE_MODE_UNWANTED_CALLS";

    //We removed it as per client comment on 11-1-17
    /*public static final String SMS_INTERCEPT_ALL_CALLS = "SMS_INTERCEPT_ALL_CALLS";
    public static final String SMS_UNWANTED_CALLS = "SMS_UNWANTED_CALLS";
    public static final String SMS_UNKNOWN_CALLS = "SMS_UNKNOWN_CALLS";*/

    public static final String BLOCK_ALL = "BLOCK_ALL";
    public static final String BLOCK_TIME = "BLOCK_TIME";
    public static final String TIME = "TIME";
    public static final String DATA = "DATA";
    public static final String USER_DATA = "USER_DATA";
    public static final String BACKGROUND_URL = "BACKGROUND_URL";
    public static final String OTP_CODE = "OTP_CODE";
    public static final String iUserID = "iUserID";
    public static final String IS_SECOND_TIME = "is_second_time";

    public static final String UNKNOWN_NUMBER = "UNKNOWN_NUMBER";
    public static final String HIDDEN_NUMBER = "HIDDEN_NUMBER";
    public static final String DO_NOT_CALL = "DO_NOT_CALL";
    public static final String IDENTIFIED_NUMBER = "IDENTIFIED_NUMBER";
    public static final String ALL_NUMBER = "ALL_NUMBER";
    public static final String MSG_UNKNOWN_NUMBER = "MSG_UNKNOWN_NUMBER";
    public static final String MSG_HIDDEN_NUMBER = "MSG_HIDDEN_NUMBER";
    public static final String MSG_ALL_NUMBER = "MSG_ALL_NUMBER";
    public static final String data = "data";
    public static final String CONTACT_CALLLOG_LAST = "CONTACT_CALLLOG_LAST";
    public static final String INTENT_PHOTO = "intent_photo";
    public static final String ACTION_DND = "ACTION_DND";
    public static final String ACTION_DRIVE_MODE = "ACTION_DRIVE_MODE";
    public static final int NOTIFICATION_DND = 111;
    public static final int NOTIFICATION_CANCEL_DND = 1111;

    public static final int NOTIFICATION_DRIVE_MODE = 222;
    public static final int NOTIFICATION_CANCEL_DRIVE_MODE = 2221;

    public static final String IS_MANUAL = "IS_MANUAL";
    public static final String ACTION_CANCEL = "ACTION_CANCEL";
    public static final String BROADCAST_CALL_STATUS_DISCONNECT = "BROADCAST_CALL_STATUS_DISCONNECT";
    public static final String BROADCAST_PAYMENT_REFRESH = "BROADCAST_PAYMENT_REFRESH";
    public static final String FREE_VOICE_MAIL_TITLE = "FREE_USER";

    public static final String FREE_VOICE_MAIL_TITLE_FOR_DRIVING = "VOICE_FOR_DRIVING";

    public static String INTENT_ID = "INTENT_ID";
    public static String INTENT_TITLE = "INTENT_TITLE";
    public static String INTENT_MESSAGE = "INTENT_MESSAGE";
    public static final String iScriptID = "iScriptID";
    public static final String POSITION = "position";
    public static final String is_helpful = "is_helpful";
    public static final String PAYMENT_PROCESS = "payment_process";
    public static final String PAYMENT_CANCEL = "payment_cancel";
    public static final String FOR_REPORT = "for_report";
    public static final String ACTION = "action";
    public static final String ACTION_DELETE = "action_delete";
    public static final String HELPFULL = "helpfull";
    public static final String ACTION_UPDATE = "action_update";
    public static final String DEFAULT_VOICEMESSAGE = "Default (DND)";
    public static final String DEFAULT_VOICEMESSAGE_DRIVING = "Default (Driving)";

    public static class Web {
        //testing url
//        public static final String BASE_URL = "http://www.kanjariya.com/callmeevo/ws/";

        //live url
        public static final String BASE_URL = "http://callmeevo.com/callmeevoappadmin/ws/";
        public static final String GET_PROFILE = BASE_URL + "user/getProfile";
        public static final String LOGIN = BASE_URL + "user/login";
        public static final String REGISTER = BASE_URL + "user/register";
        public static final String RESEND_OTP = BASE_URL + "user/resendcode";
        public static final String FORGOT_PASSWORD = BASE_URL + "user/forgotpass";
        public static final String SYNC_CONTACTS = BASE_URL + "phone/sync";
        public static final String VERIFY_OTP = BASE_URL + "user/tokenvarified";
        public static final String HIDDEN_LIST = BASE_URL + "phone/hiddenlist";
        public static final String GET_ALL_VOICE_MAIL = BASE_URL + "phone/searchcallsriptsbyphone";
        public static final String UPDATE_PROFILE = BASE_URL + "user/updateProfile";
        public static final String SEND_VOICE_MAIL = BASE_URL + "phone/addcallsripts";
        public static final String IS_USE_FULL = BASE_URL + "phone/helpfulScript";

        public static final String HIDDEN_AND_SPAM_NUMBERS = BASE_URL + "phone/hiddenandspamlist";
        public static final String SAVE_NUMBER = BASE_URL + "phone/addreport";
        public static final String PHONE_SEARCH = BASE_URL + "phone/searchphone";
        public static final String REMOVE_TRANSCRIBE_MESSAGE = BASE_URL + "phone/removecallsripts";
    }


    public static final String vDeviceToken = "vDeviceToken";
    public static final String ePlatform = "ePlatform";
    public static final String vEmail = "vEmail";
    public static final String vFirstName = "vFirstName";
    public static final String vLastName = "vLastName";
    public static final String vUserName = "vUserName";
    public static final String vPhone = "vPhone";
    public static final String referal_code = "referal_code";
    public static final String vPassword = "vPassword";


    public static final String STATUS = "STATUS";

    public static final String VOICE_MALE = "male";
    public static final String VOICE_FEMALE = "female";
}
