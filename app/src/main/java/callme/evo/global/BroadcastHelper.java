package callme.evo.global;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

/**
 * Created by pankaj on 3/8/16.
 */
public class BroadcastHelper {

    Context context;
    String broadcast;
    boolean isRegistered = false;
    BroadcastReceiver receiver;

    public BroadcastHelper(Context context, String broadcast) {
        this.context = context;
        this.broadcast = broadcast;
    }

    public void register(BroadcastReceiver receiver) {

        this.receiver = receiver;

        if (isRegistered == false) {

            LocalBroadcastManager.getInstance(context).registerReceiver(receiver, new IntentFilter(broadcast));
            isRegistered = true;
        }
    }

    public void unRegister() {

        if (isRegistered) {

            LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver);
            isRegistered = false;

        }
    }

    public static void sendBroadcast(Context context, Intent intent) {

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void sendBroadcast(Context context, String broadcast) {
        Intent intentBroadcast = new Intent(broadcast);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intentBroadcast);
    }

}
