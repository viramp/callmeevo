package callme.evo.global;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.RadioGroup;

/**
 * Created by pankaj on 30/9/16.
 */
public class FocusableRadioGroup extends RadioGroup {
    public FocusableRadioGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean isIntercept = false;

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        super.onInterceptTouchEvent(ev);
        if (isIntercept)
            return true;
        return false;
    }
}
