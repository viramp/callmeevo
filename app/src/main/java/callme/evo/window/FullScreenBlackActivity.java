package callme.evo.window;

import android.app.Activity;
import android.os.Bundle;

import callme.evo.R;

/**
 * Created by pankaj on 30/9/16.
 */
public class FullScreenBlackActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_black);
    }
}
