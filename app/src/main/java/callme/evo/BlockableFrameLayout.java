package callme.evo;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;

/**
 * Created by pankaj on 8/10/16.
 */
public class BlockableFrameLayout extends FrameLayout {
    public BlockableFrameLayout(Context context) {
        super(context);
    }
    public BlockableFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public BlockableFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    boolean isBlock = false;

    public void setBlockChild(boolean isBlock) {
        this.isBlock = isBlock;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        super.onInterceptTouchEvent(ev);
        return isBlock;
    }
}
