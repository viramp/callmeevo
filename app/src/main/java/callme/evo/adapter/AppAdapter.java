package callme.evo.adapter;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.andraskindler.quickscroll.Scrollable;
import com.github.siyamed.shapeimageview.CircularImageView;

import java.util.ArrayList;

import callme.evo.R;
import callme.evo.global.Utility;
import callme.evo.models.Contacts;

/**
 * Used in contactsfragment
 */
public class AppAdapter extends BaseAdapter implements Scrollable {

    Context context;
    private ArrayList<Contacts> list;
    public AppAdapter(Context context) {
        this.context = context;
        this.list = new ArrayList<>();
    }
    public static abstract class Row {}

    public static final class Item extends Row {
        public final String text;

        public Item(String text) {
            this.text = text;
        }
    }


    public AppAdapter(Context context, ArrayList<Contacts> list) {

        this.context = context;
        this.list = list;


    }

    @Override
    public int getCount() {
        return list.size();
    }

    public void updateItem(ArrayList<Contacts> list) {
        this.list = list;

//        setUpSectionIndexer();

        notifyDataSetChanged();
    }

    /*private void setUpSectionIndexer(){
        mapIndex = new LinkedHashMap<String, Integer>();

        for (int x = 0; x < list.size(); x++) {
            Contacts contacts = list.get(x);
            String ch = contacts.name.substring(0, 1);
            ch = ch.toUpperCase(Locale.US);

            // HashMap will prevent duplicates
            mapIndex.put(ch, x);
        }

        Set<String> sectionLetters = mapIndex.keySet();

        // create a list from the set to sort
        ArrayList<String> sectionList = new ArrayList<String>(sectionLetters);
        Collections.sort(sectionList);
        sections = new String[sectionList.size()];
        sectionList.toArray(sections);
    }*/
    public void removeItem(int position) {
        if (list.size() > position) {
            list.remove(position);
            notifyDataSetInvalidated();
        }
    }

    @Override
    public Contacts getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.list_item_contact_own, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtName.setText(getItem(position).name);
        holder.txtNumber.setText(getItem(position).number);

        if (!TextUtils.isEmpty(getItem(position).thumbnailImage)) {
//                Glide.with(getActivity()).load(Uri.parse(getItem(position).thumbnailImage)).into(holder.imgProfile);
            Utility.showCircularImageView(context, holder.imgProfile, Uri.parse(getItem(position).thumbnailImage));
        } else {
            holder.imgProfile.setImageResource(R.drawable.ph_contact);
        }


        return convertView;
    }


    class ViewHolder {
        public CircularImageView imgProfile;
        public TextView txtName, txtNumber;

        public ViewHolder(View view) {
            imgProfile = (CircularImageView) view.findViewById(R.id.profileImage);
            txtName = (TextView) view.findViewById(R.id.txt_name);
            txtNumber = (TextView) view.findViewById(R.id.txt_number);
        }
    }

    //================
    @Override
    public String getIndicatorForPosition(int childposition, int groupposition) {
        return Character.toString(list.get(childposition).name.charAt(0));
    }

    @Override
    public int getScrollPosition(int childposition, int groupposition) {
        return childposition;
    }


    /*HashMap<String, Integer> mapIndex;
    String[] sections;

    @Override
    public Object[] getSections() {
        return sections;
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        return mapIndex.get(sections[sectionIndex]);
    }

    @Override
    public int getSectionForPosition(int position) {
        return 0;
    }*/


}