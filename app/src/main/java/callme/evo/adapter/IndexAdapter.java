package callme.evo.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.activeandroid.util.Log;
import com.andraskindler.quickscroll.Scrollable;

import java.util.ArrayList;

import callme.evo.R;

/**
 * Used in contactsfragment
 */
public class IndexAdapter extends BaseAdapter  implements Scrollable {

    Context context;
    private ArrayList<String> list;

    public IndexAdapter(Context context) {
        this.context = context;
        this.list = new ArrayList<>();
    }

    public IndexAdapter(Context context, ArrayList<String> list) {

        this.context = context;
        this.list = list;


    }

    @Override
    public int getCount() {
        return list.size();
    }

    public void updateItem(ArrayList<String> list) {
        this.list = list;

//        setUpSectionIndexer();

        notifyDataSetChanged();
    }



    /*private void setUpSectionIndexer(){
        mapIndex = new LinkedHashMap<String, Integer>();

        for (int x = 0; x < list.size(); x++) {
            Contacts contacts = list.get(x);
            String ch = contacts.name.substring(0, 1);
            ch = ch.toUpperCase(Locale.US);

            // HashMap will prevent duplicates
            mapIndex.put(ch, x);
        }

        Set<String> sectionLetters = mapIndex.keySet();

        // create a list from the set to sort
        ArrayList<String> sectionList = new ArrayList<String>(sectionLetters);
        Collections.sort(sectionList);
        sections = new String[sectionList.size()];
        sectionList.toArray(sections);
    }*/
    public void removeItem(int position) {
        if (list.size() > position) {
            list.remove(position);
            notifyDataSetInvalidated();
        }
    }

    @Override
    public String getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.list_item_index, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txt_index.setText(list.get(position).toString());

        return convertView;
    }



    class ViewHolder {
        public TextView txt_index;

        public ViewHolder(View view) {
            txt_index = (TextView) view.findViewById(R.id.txt_index);
        }
    }

    //================

    @Override
    public String getIndicatorForPosition(int childposition, int groupposition) {
        Log.e("getIndicatorForPosition" , " getIndicatorForPosition "+childposition);
        Log.e("getIndicatorForPosition" , " getIndicatorForPosition "
                +Character.toString(this.list.get(childposition).charAt(0)));
        return Character.toString(this.list.get(childposition).charAt(0));
    }

    @Override
    public int getScrollPosition(int childposition, int groupposition) {
        Log.e("getIndicatorForPosition" , " childposition "+childposition);
        return 0;
    }

    /*HashMap<String, Integer> mapIndex;
    String[] sections;

    @Override
    public Object[] getSections() {
        return sections;
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        return mapIndex.get(sections[sectionIndex]);
    }

    @Override
    public int getSectionForPosition(int position) {
        return 0;
    }*/
}