package callme.evo.adapter;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import callme.evo.R;
import callme.evo.global.Utility;
import callme.evo.models.Contacts;

public class CallGroupedAdapter extends BaseAdapter {

    private static final int VIEW_TYPE_NORMAL = 1;
    private static final int VIEW_TYPE_HEADER = 2;

    ArrayList<Contacts> list;
    Context context;

    public CallGroupedAdapter(Context context) {
        this.context = context;
        this.list = new ArrayList<Contacts>();
    }

    public CallGroupedAdapter(Context context, ArrayList<Contacts> list) {
        this.list = list;
        this.context = context;
    }

    public void updateItem(ArrayList<Contacts> list) {
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Contacts getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public int getItemViewType(int position) {
        if (TextUtils.isEmpty(getItem(position).number) && TextUtils.isEmpty(getItem(position).name)) {
            return VIEW_TYPE_HEADER;
        }
        return VIEW_TYPE_NORMAL;
//        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;


        Contacts item = getItem(position);

        if (getItemViewType(position) == VIEW_TYPE_NORMAL) {

            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(R.layout.list_item_block_today, parent, false);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            if (!TextUtils.isEmpty(item.name)) {
                holder.txtName.setText(item.name);
                holder.txtName.setVisibility(View.VISIBLE);
            } else {
                holder.txtName.setText("");
                holder.txtName.setVisibility(View.GONE);
            }

            holder.txtNumber.setText(item.number);

            if (!TextUtils.isEmpty(item.thumbnailImage)) {
//            Glide.with(context).load(Uri.parse(item.thumbnailImage)).into(holder.imgProfile);
                Utility.showCircularImageView(context, holder.imgProfile, Uri.parse(getItem(position).thumbnailImage));
            } else {
                holder.imgProfile.setImageResource(R.drawable.ph_contact);
            }

            if (item.timestamp > 0) {
                try {

                    long time = item.timestamp;
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTimeInMillis(time);

                    SimpleDateFormat date = new SimpleDateFormat("dd MMM yy");
                    holder.txtDate.setText(date.format(calendar.getTime()));

                    SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
                    holder.txtTime.setText(timeFormat.format(calendar.getTime()));
                    holder.txtDate.setVisibility(View.VISIBLE);
                    holder.txtTime.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                    holder.txtDate.setVisibility(View.GONE);
                    holder.txtTime.setVisibility(View.GONE);
                }
            } else {
                holder.txtDate.setVisibility(View.GONE);
                holder.txtTime.setVisibility(View.GONE);
            }
        } else {

            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(R.layout.list_item_header, parent, false);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.txtName.setText(item.header);

        }
        return convertView;
    }

    public void removeItem(int position) {
        if (list.size() > position) {
            list.remove(position);
            notifyDataSetInvalidated();
        }
    }

    class ViewHolder {
        public CircularImageView imgProfile;
        public TextView txtName, txtNumber, txtDate, txtTime;

        public ViewHolder(View view) {
            imgProfile = (CircularImageView) view.findViewById(R.id.profileImage);
            txtName = (TextView) view.findViewById(R.id.txt_name);
            txtNumber = (TextView) view.findViewById(R.id.txt_number);
            txtDate = (TextView) view.findViewById(R.id.txt_date);
            txtTime = (TextView) view.findViewById(R.id.txt_time);
        }
    }
}