package callme.evo.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import callme.evo.R;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.ContactCallLog;

public class ActivityAdapter extends BaseAdapter {

    ArrayList<ContactCallLog> list;
    Context context;

    public ActivityAdapter(Context context) {
        this.context = context;
        this.list = new ArrayList<ContactCallLog>();
    }

    public ActivityAdapter(Context context, ArrayList<ContactCallLog> list) {
        this.list = list;
        this.context = context;
    }

    public void updateItem(ArrayList<ContactCallLog> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public ContactCallLog getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        ViewHolder holder;
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.list_item_block_today, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ContactCallLog item = getItem(position);

        if (!TextUtils.isEmpty(item.name)) {
            holder.txtName.setText(item.name);
            holder.txtName.setVisibility(View.VISIBLE);
        } else if (!TextUtils.isEmpty(item.message)) {
            holder.txtName.setText(item.number);
            holder.txtName.setVisibility(View.VISIBLE);
        } else {
            holder.txtName.setText(item.number);
            holder.txtName.setVisibility(View.VISIBLE);
        }

        if (!TextUtils.isEmpty(item.message)) {
            holder.txtNumber.setText(item.message);
        } else if (TextUtils.isEmpty(item.name)) {
            holder.txtNumber.setText("Unknown");
        } else {
            holder.txtNumber.setText(item.number);
        }

        if (ConstantCodes.BLACK_LIST.equalsIgnoreCase(item.blackListOrWhiteList)) {
            manageIconDimension(true,holder.imgProfile);
            holder.imgProfile.setImageResource(R.drawable.ph_contact_blocked);
        } else if (!TextUtils.isEmpty(item.message)) {
            manageIconDimension(false,holder.imgProfile);
            holder.imgProfile.setImageResource(R.drawable.text);
        } else if (!TextUtils.isEmpty(item.name)) {
            if (!TextUtils.isEmpty(getItem(position).thumbnailImage)){
                manageIconDimension(true,holder.imgProfile);
                Utility.showCircularImageView(context, holder.imgProfile, Uri.parse(getItem(position).thumbnailImage));
            }
            else{
                manageIconDimension(true,holder.imgProfile);
                holder.imgProfile.setImageResource(R.drawable.ph_contact);
            }

        } else {
            manageIconDimension(false,holder.imgProfile);
            holder.imgProfile.setImageResource(R.drawable.circle_tick);
        }

        /*if (!TextUtils.isEmpty(item.thumbnailImage)) {
//            Glide.with(context).load(Uri.parse(item.thumbnailImage)).into(holder.imgProfile);
            Utility.showCircularImageView(context, holder.imgProfile, Uri.parse(getItem(position).thumbnailImage));
        } else {
            holder.imgProfile.setImageResource(R.drawable.ph_contact);
        }*/

        if (item.timestamp > 0) {
            try {

                long time = item.timestamp;
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(time);

                SimpleDateFormat date = new SimpleDateFormat("dd MMM yy");
                holder.txtDate.setText(date.format(calendar.getTime()));

                SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
                holder.txtTime.setText(timeFormat.format(calendar.getTime()));
                holder.txtDate.setVisibility(View.VISIBLE);
                holder.txtTime.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
                holder.txtDate.setVisibility(View.GONE);
                holder.txtTime.setVisibility(View.GONE);
            }
        } else {
            holder.txtDate.setVisibility(View.GONE);
            holder.txtTime.setVisibility(View.GONE);
        }

        return convertView;
    }

    private void manageIconDimension(boolean isUserProfile,com.github.siyamed.shapeimageview.CircularImageView
            circularImageView){
        if(isUserProfile){
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(dpToPx(55),dpToPx(55));
            circularImageView.setLayoutParams(params);
        }else{
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(dpToPx(45),dpToPx(45));
            params.setMargins(dpToPx(5),dpToPx(5),dpToPx(5),dpToPx(5));
            circularImageView.setLayoutParams(params);
        }

    }
    public static int dpToPx(int dp)
    {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }
    public void removeItem(int position) {
        if (list.size() > position) {
            list.remove(position);
            notifyDataSetInvalidated();
        }
    }

    class ViewHolder {
        public CircularImageView imgProfile;
        public TextView txtName, txtNumber, txtDate, txtTime;

        public ViewHolder(View view) {
            imgProfile = (CircularImageView) view.findViewById(R.id.profileImage);
            txtName = (TextView) view.findViewById(R.id.txt_name);
            txtNumber = (TextView) view.findViewById(R.id.txt_number);
            txtDate = (TextView) view.findViewById(R.id.txt_date);
            txtTime = (TextView) view.findViewById(R.id.txt_time);
        }
    }
}