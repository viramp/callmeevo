package callme.evo.controller;

import android.content.Context;
import android.text.TextUtils;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import callme.evo.R;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.User;

/**
 * Created by pankaj on 14/8/16.
 */
public class LoginController {
    Context context;
    LoginCallback loginCallback;

    public LoginController(Context context, LoginCallback loginCallback) {
        this.context = context;
        this.loginCallback = loginCallback;
    }

    public void login(final String username, final String password) {

//        String response="{\"iUserID\":\"115\",\"vFirstName\":\"\",\"vLastName\":\"\",\"vUserName\":\"pankaj1\",\"vEmail\":\"pankaj1@gmail.com\",\"vPassword\":\"202cb962ac59075b964b07152d234b70\",\"vProfilePic\":\"\",\"eType\":\"paid\",\"vPhone\":\"+12342342323\",\"vTagline\":\"\",\"ePlatform\":\"Android\",\"vDeviceToken\":\"4f5f6f4dd4564f6f464s6546\",\"eStatus\":\"Active\",\"password_token\":\"\",\"referal_code\":\"\",\"activation_code\":\"6239\",\"token_varified\":\"1\",\"dtCreated\":\"2016-11-09 20:33:09\",\"expired_date\":\"2016-12-16 20:33:09\",\"tsModified\":\"2016-11-09 20:34:10\",\"referal_usage_user\":\"0\",\"vHmac\":\"094cc963285ed178b57cd4a9c4bb9774\"}";
//        User user = (User) Utility.parseFromString(response, User.class);
//        loginCallback.onSuccess(user);

        if (TextUtils.isEmpty(username)) {
            loginCallback.onFail(context.getString(R.string.msg_mendatory_user_name_email));
            return;
        } else if (TextUtils.isEmpty(password)) {
            loginCallback.onFail(context.getString(R.string.msg_mendatory_password));
            return;
        }

        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, ConstantCodes.Web.LOGIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.LOGIN);
                showLog("RESPONSE : " + response.toString());
                showLog("**********");

//                response="{\"iUserID\":\"115\",\"vFirstName\":\"\",\"vLastName\":\"\",\"vUserName\":\"pankaj1\",\"vEmail\":\"pankaj1@gmail.com\",\"vPassword\":\"202cb962ac59075b964b07152d234b70\",\"vProfilePic\":\"\",\"eType\":\"free\",\"vPhone\":\"+12342342323\",\"vTagline\":\"\",\"ePlatform\":\"Android\",\"vDeviceToken\":\"4f5f6f4dd4564f6f464s6546\",\"eStatus\":\"Active\",\"password_token\":\"\",\"referal_code\":\"\",\"activation_code\":\"6239\",\"token_varified\":\"1\",\"dtCreated\":\"2016-11-09 20:33:09\",\"expired_date\":\"2016-12-16 20:33:09\",\"tsModified\":\"2016-11-09 20:34:10\",\"referal_usage_user\":\"0\",\"vHmac\":\"094cc963285ed178b57cd4a9c4bb9774\"}";
                /*{"iUserID":"115","vFirstName":"","vLastName":"","vUserName":"pankaj1","vEmail":"pankaj1@gmail.com","vPassword":"202cb962ac59075b964b07152d234b70","vProfilePic":"","eType":"free","vPhone":"+12342342323","vTagline":"","ePlatform":"Android","vDeviceToken":"4f5f6f4dd4564f6f464s6546","eStatus":"Active","password_token":"","referal_code":"","activation_code":"6239","token_varified":"1","dtCreated":"2016-11-09 20:33:09","expired_date":"2016-12-16 20:33:09","tsModified":"2016-11-09 20:34:10","referal_usage_user":"0","vHmac":"094cc963285ed178b57cd4a9c4bb9774"}*/

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has(ConstantCodes.STATUS) && jsonObject.getString(ConstantCodes.STATUS).equalsIgnoreCase(ConstantCodes.SUCCESS)) {
                        if (loginCallback != null) {
                            User user = (User) Utility.parseFromString(jsonObject.optString(ConstantCodes.DATA), User.class);
                            loginCallback.onSuccess(user);
                        }
                    } else {
                        if (loginCallback != null) {
                            if (jsonObject.has(ConstantCodes.MESSAGE)) {
                                loginCallback.onFail(jsonObject.getString(ConstantCodes.MESSAGE));
                            } else {
                                loginCallback.onFail(context.getString(R.string.server_error));
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (loginCallback != null) {
                        loginCallback.onFail(e.getMessage());
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.LOGIN);
                showLogE("RESPONSE : " + (error.networkResponse == null ? (error != null ? error.getLocalizedMessage() : "NullVolleyError") : new String(error.networkResponse.data)));
                showLog("**********");
                if (loginCallback != null) {
                    loginCallback.onFail((error.networkResponse == null ? (error != null ? error.getLocalizedMessage() : "NullVolleyError") : new String(error.networkResponse.data)));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = Utility.getDefaultParams();
                params.put(ConstantCodes.vEmail, username);
                params.put(ConstantCodes.vPassword, password);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = Utility.getDefaultHeaders();
                return headers;
                /*Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token 04af7328b2346602ad7d581ff738eb25a5482cbf");
                params.put("Apikey", "JXn8e6C29jhZ065i8wyQktY33YD3s9zy");
                return params;*/
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        Utility.getRequestQueue().add(jsonObjReq);

    }

    public interface LoginCallback {
        public void onSuccess(User user);

        public void onFail(String message);
    }

    private static void showLogE(String message) {
        Log.e("ws", message + "");
    }

    private static void showLog(String message) {
        Log.e("WS", message + "");
    }
}
