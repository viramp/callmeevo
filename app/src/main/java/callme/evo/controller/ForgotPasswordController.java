package callme.evo.controller;

import android.content.Context;
import android.text.TextUtils;

import android.util.Log;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import callme.evo.R;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;

/**
 * Created by pankaj on 14/8/16.
 */
public class ForgotPasswordController {
    Context context;
    ForgotPasswordCallback loginCallback;

    public ForgotPasswordController(Context context, ForgotPasswordCallback loginCallback) {
        this.context = context;
        this.loginCallback = loginCallback;
    }

    public void forgotPassword(final String email) {
        if (TextUtils.isEmpty(email)) {
            loginCallback.onFail(context.getString(R.string.msg_mendatory_email));
            return;
        } else if (!Utility.isValidEmail(email)) {
            loginCallback.onFail(context.getString(R.string.msg_valid_email));
            return;
        }
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, ConstantCodes.Web.FORGOT_PASSWORD, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.FORGOT_PASSWORD);
                showLog("RESPONSE : " + response.toString());
                showLog("**********");

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has(ConstantCodes.STATUS) && jsonObject.getString(ConstantCodes.STATUS).equalsIgnoreCase(ConstantCodes.SUCCESS)) {
                        if (loginCallback != null) {
                            loginCallback.onSuccess();
                        }
                    } else {
                        if (loginCallback != null) {
                            if (jsonObject.has(ConstantCodes.MESSAGE)) {
                                loginCallback.onFail(jsonObject.getString(ConstantCodes.MESSAGE));
                            } else {
                                loginCallback.onFail(context.getString(R.string.server_error));
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (loginCallback != null) {
                        loginCallback.onFail(e.getMessage());
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.FORGOT_PASSWORD);
                showLogE("RESPONSE : " + (error.networkResponse == null ? (error != null ? error.getLocalizedMessage() : "NullVolleyError") : new String(error.networkResponse.data)));
                showLog("**********");
                if (loginCallback != null) {
                    loginCallback.onFail((error.networkResponse == null ? (error != null ? error.getLocalizedMessage() : "NullVolleyError") : new String(error.networkResponse.data)));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = Utility.getDefaultParams();
                params.put(ConstantCodes.vEmail, email);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = Utility.getDefaultHeaders();
                return headers;
                /*Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token 04af7328b2346602ad7d581ff738eb25a5482cbf");
                params.put("Apikey", "JXn8e6C29jhZ065i8wyQktY33YD3s9zy");
                return params;*/
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        Utility.getRequestQueue().add(jsonObjReq);
    }

    public interface ForgotPasswordCallback {
        public void onSuccess();

        public void onFail(String message);
    }

    private static void showLogE(String message) {
        Log.e("ws", message + "");
    }

    private static void showLog(String message) {
        Log.i("ws", message + "");
    }
}
