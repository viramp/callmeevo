package callme.evo.controller;

import android.content.Context;
import android.text.TextUtils;

import android.util.Log;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import callme.evo.R;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.User;

/**
 * Created by pankaj on 14/8/16.
 */
public class ResendOtpController {
    Context context;
    ResendOtpCallback loginCallback;

    public ResendOtpController(Context context, ResendOtpCallback loginCallback) {
        this.context = context;
        this.loginCallback = loginCallback;
    }

    public void sendOtp(final User user) {
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, ConstantCodes.Web.RESEND_OTP, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.RESEND_OTP);
                showLog("RESPONSE : " + response.toString());
                showLog("**********");

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has(ConstantCodes.STATUS) && jsonObject.getString(ConstantCodes.STATUS).equalsIgnoreCase(ConstantCodes.SUCCESS)) {
                        if (loginCallback != null) {

                            User user = (User) Utility.parseFromString(jsonObject.optString(ConstantCodes.DATA), User.class);
                            loginCallback.onSuccess(user);

                        }
                    } else {
                        if (loginCallback != null) {
                            if (jsonObject.has(ConstantCodes.MESSAGE)) {
                                loginCallback.onFail(jsonObject.getString(ConstantCodes.MESSAGE));
                            } else {
                                loginCallback.onFail(context.getString(R.string.server_error));
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (loginCallback != null) {
                        loginCallback.onFail(e.getMessage());
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.LOGIN);
                showLogE("RESPONSE : " + (error.networkResponse == null ? (error != null ? error.getLocalizedMessage() : "NullVolleyError") : new String(error.networkResponse.data)));
                showLog("**********");
                if (loginCallback != null) {
                    loginCallback.onFail((error.networkResponse == null ? (error != null ? error.getLocalizedMessage() : "NullVolleyError") : new String(error.networkResponse.data)));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = Utility.getDefaultParams();
                params.put(ConstantCodes.iUserID, user.iUserID);
//                params.put(ConstantCodes.vFirstName, vFirstName);
//                params.put(ConstantCodes.vLastName, vLastName);
//                params.put(ConstantCodes.iUserID, user.iUserID);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = Utility.getDefaultHeaders();

                return headers;
                /*Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token 04af7328b2346602ad7d581ff738eb25a5482cbf");
                params.put("Apikey", "JXn8e6C29jhZ065i8wyQktY33YD3s9zy");
                return params;*/
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        Utility.getRequestQueue().add(jsonObjReq);
    }

    public void register(final String vUserName, final String vEmail, final String vPassword, final String vConfirmPassword, final String vPhone, final String referal_code) {

        /*if (TextUtils.isEmpty(vFirstName)) {
            callback.onFail(context.getString(R.string.msg_mendatory_first_name));
            return;
        } else if (TextUtils.isEmpty(vLastName)) {
            callback.onFail(context.getString(R.string.msg_mendatory_last_name));
            return;
        } else*/
        if (TextUtils.isEmpty(vUserName)) {
            loginCallback.onFail(context.getString(R.string.msg_mendatory_user_name));
            return;
        } else if (TextUtils.isEmpty(vEmail)) {
            loginCallback.onFail(context.getString(R.string.msg_mendatory_email));
            return;
        } else if (TextUtils.isEmpty(vPassword)) {
            loginCallback.onFail(context.getString(R.string.msg_mendatory_password));
            return;
        } else if (TextUtils.isEmpty(vConfirmPassword)) {
            loginCallback.onFail(context.getString(R.string.msg_mendatory_confirm_password));
            return;
        } else if (vPassword.equalsIgnoreCase(vConfirmPassword) == false) {
            loginCallback.onFail(context.getString(R.string.msg_mendatory_password_same));
            return;
        } else if (TextUtils.isEmpty(vPhone)) {
            loginCallback.onFail(context.getString(R.string.msg_mendatory_phone));
            return;
        }

        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, ConstantCodes.Web.REGISTER, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.LOGIN);
                showLog("RESPONSE : " + response.toString());
                showLog("**********");

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has(ConstantCodes.STATUS) && jsonObject.getString(ConstantCodes.STATUS).equalsIgnoreCase(ConstantCodes.SUCCESS)) {
                        if (loginCallback != null) {

                            User user = (User) Utility.parseFromString(jsonObject.optString(ConstantCodes.DATA), User.class);
                            loginCallback.onSuccess(user);

                        }
                    } else {
                        if (loginCallback != null) {
                            if (jsonObject.has(ConstantCodes.MESSAGE)) {
                                loginCallback.onFail(jsonObject.getString(ConstantCodes.MESSAGE));
                            } else {
                                loginCallback.onFail(context.getString(R.string.server_error));
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (loginCallback != null) {
                        loginCallback.onFail(e.getMessage());
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.LOGIN);
                showLogE("RESPONSE : " + (error.networkResponse == null ? (error != null ? error.getLocalizedMessage() : "NullVolleyError") : new String(error.networkResponse.data)));
                showLog("**********");
                if (loginCallback != null) {
                    loginCallback.onFail((error.networkResponse == null ? (error != null ? error.getLocalizedMessage() : "NullVolleyError") : new String(error.networkResponse.data)));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = Utility.getDefaultParams();
//                params.put(ConstantCodes.vFirstName, vFirstName);
//                params.put(ConstantCodes.vLastName, vLastName);
                params.put(ConstantCodes.vUserName, vUserName);
                params.put(ConstantCodes.vEmail, vEmail);
                params.put(ConstantCodes.vPassword, vPassword);
                params.put(ConstantCodes.vPhone, vPhone);
                params.put(ConstantCodes.referal_code, referal_code);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = Utility.getDefaultHeaders();
                return headers;
                /*Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token 04af7328b2346602ad7d581ff738eb25a5482cbf");
                params.put("Apikey", "JXn8e6C29jhZ065i8wyQktY33YD3s9zy");
                return params;*/
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        Utility.getRequestQueue().add(jsonObjReq);
    }

    public interface ResendOtpCallback {
        public void onSuccess(User user);

        public void onFail(String message);
    }

    private static void showLogE(String message) {
        Log.e("ws", message + "");
    }

    private static void showLog(String message) {
        Log.i("ws", message + "");
    }
}
