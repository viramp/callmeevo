package callme.evo;

import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.User;
import callme.evo.models.VoiceMessageModel;

/**
 * Created by pankaj on 9/10/16.
 */
public class SendVoiceMailIntentService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public SendVoiceMailIntentService(String name) {
        super(name);
    }

    public SendVoiceMailIntentService() {
        super("SendVoiceMailIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {


        String incomingNumber = intent.getStringExtra(ConstantCodes.INTENT_NUMBER);
        String blockedFor = intent.getStringExtra(ConstantCodes.INTENT_BLOCK_FOR);

        if (Utility.getLoggedInUser(getApplicationContext()) == null) {
            return;
        }

        User user = Utility.getLoggedInUser(getApplicationContext());
        if (Utility.isPaidApplication(getApplicationContext())) {

            boolean isSendSuccess = false;
            if (ConstantCodes.DRIVING_MODE_ENABLED.equalsIgnoreCase(blockedFor)) {
                String query = "SELECT message FROM Contacts\n" +
                        " JOIN VoiceMessageModel ON Contacts.voiceMessageId=VoiceMessageModel.voiceMessageId\n" +
                        " Where Contacts.number like '%" + incomingNumber + "' or  '" + incomingNumber + "' like '%'||Contacts.number";
                Cursor cursor = ActiveAndroid.getDatabase().rawQuery(query, null);
                if (cursor != null && cursor.moveToFirst()) {
                    String message = cursor.getString(cursor.getColumnIndex("message"));
                    sendMessage(user.iUserID, incomingNumber, user.voiceType, message);
                    isSendSuccess = true;
                }
                cursor.close();
            }

            if (isSendSuccess == false) {
                ArrayList<VoiceMessageModel> list = new Select().from(VoiceMessageModel.class).where("voiceMessageId = '" + (ConstantCodes.DRIVING_MODE_ENABLED.equalsIgnoreCase(blockedFor) ? ConstantCodes.FREE_VOICE_MAIL_TITLE_FOR_DRIVING : ConstantCodes.FREE_VOICE_MAIL_TITLE) + "'").execute();
                if (list.size() > 0) {
                    VoiceMessageModel voiceMessageModel = list.get(0);
                    sendMessage(user.iUserID, incomingNumber, user.voiceType, voiceMessageModel.message);
                }
            }
        } else {
            ArrayList<VoiceMessageModel> list = new Select().from(VoiceMessageModel.class).where("voiceMessageId = '" + (ConstantCodes.DRIVING_MODE_ENABLED.equalsIgnoreCase(blockedFor) ? ConstantCodes.FREE_VOICE_MAIL_TITLE_FOR_DRIVING : ConstantCodes.FREE_VOICE_MAIL_TITLE) + "'").execute();
            if (list.size() > 0) {
                VoiceMessageModel voiceMessageModel = list.get(0);
                sendMessage(user.iUserID, incomingNumber, user.voiceType, voiceMessageModel.message);
            }
        }
    }

    private void sendMessage(final String userId, final String incommingNumber, final String voiceType, final String message) {
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, ConstantCodes.Web.SEND_VOICE_MAIL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.LOGIN);
                showLog("RESPONSE : " + response.toString());
                showLog("**********");

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has(ConstantCodes.STATUS) && jsonObject.getString(ConstantCodes.STATUS).equalsIgnoreCase(ConstantCodes.SUCCESS)) {

                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.LOGIN);
                showLogE("RESPONSE : " + (error.networkResponse == null ? (error != null ? error.getLocalizedMessage() : "NullVolleyError") : new String(error.networkResponse.data)));
                showLog("**********");

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = Utility.getDefaultParams();
                params.put("phone_number", incommingNumber);
                params.put("eType", voiceType);
                params.put("tScript", message);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = Utility.getDefaultHeaders();
                headers.put(ConstantCodes.iUserID, userId);
                return headers;
                /*Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token 04af7328b2346602ad7d581ff738eb25a5482cbf");
                params.put("Apikey", "JXn8e6C29jhZ065i8wyQktY33YD3s9zy");
                return params;*/
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        Utility.getRequestQueue().add(jsonObjReq);
    }

    private static void showLogE(String message) {
        Log.e("ws", message + "");
    }

    private static void showLog(String message) {
        Log.i("ws", message + "");
    }
}
