package callme.evo;

import android.app.IntentService;
import android.content.Intent;

import com.activeandroid.query.Select;
import android.util.Log;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.Contacts;
import callme.evo.models.SpamNumbers;
import callme.evo.models.User;

/**
 * Created by pankaj on 25/9/16.
 */
public class SyncToServerService extends IntentService {


    public SyncToServerService() {
        super("SyncToServerService");
    }

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public SyncToServerService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if (Utility.getLoggedInUser(getApplicationContext()) == null) {
            return;
        }

        User user = Utility.getLoggedInUser(getApplicationContext());

        int bunchSize = 10;
        ArrayList<Contacts> listC = new Select().from(Contacts.class).execute();

        //{"name":"ronak patel","address":"ahmedabd","phone_number":"7405401617"}
        JSONArray jsonArray = new JSONArray();
        int i = 0;
        for (Contacts contacts : listC) {
            JSONObject json = new JSONObject();
            try {
                json.put("name", contacts.name + "");
                json.put("phone_number", contacts.number + "");
                json.put("address", "" + user.vEmail);

                jsonArray.put(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (i % bunchSize == 0) {
                sendToServer(jsonArray.toString());
                jsonArray = new JSONArray();
            }
            i++;
        }
        syncHiddenNoToLocal();
    }

    private void syncHiddenNoToLocal() {
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, ConstantCodes.Web.HIDDEN_AND_SPAM_NUMBERS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.SYNC_CONTACTS);
                showLog("RESPONSE : " + response.toString());
                showLog("**********");

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.optJSONArray(ConstantCodes.DATA);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonContact = jsonArray.optJSONObject(i);
                        SpamNumbers spamNumbers = new SpamNumbers();
                        spamNumbers.number = jsonContact.optString("phone_number");
                        spamNumbers.type = jsonContact.optString("type");
                        spamNumbers.save();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.SYNC_CONTACTS);
                showLog("RESPONSE : " + (error.networkResponse == null ? (error != null ? error.getLocalizedMessage() : "NullVolleyError") : new String(error.networkResponse.data)));
                showLog("**********");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = Utility.getDefaultParams();
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = Utility.getDefaultHeaders();
                return headers;
                /*Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token 04af7328b2346602ad7d581ff738eb25a5482cbf");
                params.put("Apikey", "JXn8e6C29jhZ065i8wyQktY33YD3s9zy");
                return params;*/
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        Utility.getRequestQueue().add(jsonObjReq);
    }

    private static void showLog(String message) {
        Log.i("ws", message + "");
    }

    private void sendToServer(final String numbers) {
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, ConstantCodes.Web.SYNC_CONTACTS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.SYNC_CONTACTS);
                showLog("RESPONSE : " + response.toString());
                showLog("**********");
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                showLog("**********");
                showLog("URL [POST]: " + ConstantCodes.Web.SYNC_CONTACTS);
                showLog("RESPONSE : " + (error.networkResponse == null ? (error != null ? error.getLocalizedMessage() : "NullVolleyError") : new String(error.networkResponse.data)));
                showLog("**********");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = Utility.getDefaultParams();
                params.put(ConstantCodes.data, numbers);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = Utility.getDefaultHeaders();
                return headers;
                /*Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token 04af7328b2346602ad7d581ff738eb25a5482cbf");
                params.put("Apikey", "JXn8e6C29jhZ065i8wyQktY33YD3s9zy");
                return params;*/
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        Utility.getRequestQueue().add(jsonObjReq);
    }
}
