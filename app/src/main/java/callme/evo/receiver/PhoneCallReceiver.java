package callme.evo.receiver;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.android.internal.telephony.ITelephony;

import java.lang.reflect.Method;
import java.util.ArrayList;

import callme.evo.SendVoiceMailIntentService;
import callme.evo.activity.IncomingCall;
import callme.evo.global.BroadcastHelper;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.CallsBlocked;
import callme.evo.models.Contacts;
import callme.evo.models.SpamNumbers;
import callme.evo.services.ContactAddDialogService;

public class PhoneCallReceiver extends BroadcastReceiver {
    Context context = null;
    private static final String TAG = "Phonecall";
    private ITelephony telephonyService;


    private AudioManager audiomanager;
    private static int ringerMode = -1;
    private static String lastRingingState;

    @Override
    public void onReceive(final Context context, Intent intent) {
        Log.v(TAG + "1", "Receving...." + intent.getAction());
        Log.v(TAG + "1", "Receving...." + intent.getStringExtra(TelephonyManager.EXTRA_STATE));
        Log.v(TAG + "1", "=======");
        if (!Utility.getSharedPreference(context).contains(ConstantCodes.USER_DATA)) {
            return;
        }

        this.context = context;

        if (intent.getAction().equals("android.intent.action.PHONE_STATE")) {
            String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            System.out.println("--------in state-----");

            if (audiomanager == null)
                audiomanager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

            if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                //Checking if lastringing state is offhook then no need to do any thing it means call is undergoining.
                if("offhook".equalsIgnoreCase(lastRingingState)){
                    //TODO:revert back the ringer volume if already setted.
                    if (audiomanager != null)
                        audiomanager.setRingerMode(ringerMode);
                    return;
                }
                // Incoming call
                String incomingNumber = Utility.getOnlyDigitNumber(intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER));
                System.out.println("--------------my number---------" + incomingNumber);

                boolean allowPriorityContact = Utility.getSharedPreference(context).getBoolean(ConstantCodes.DND_ALLOW_PRIORITY_CONTACT, false);
                if (Utility.getSharedPreference(context).contains(ConstantCodes.DRIVING_MODE_ENABLED)) {

                    //TODO:setting the ringer volume to low and save that ringer volume. for rever back on disconnect
                    ringerMode = audiomanager.getRingerMode();

                    if (Utility.getSharedPreference(context).getBoolean(ConstantCodes.DRIVE_MODE_INTERCEPT_CALLS_SMS, false)) {
                        audiomanager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
                        blockAndUpdate(incomingNumber);
                        sendVoiceMessage(incomingNumber, ConstantCodes.DRIVING_MODE_ENABLED);

                    } else if (Utility.getSharedPreference(context).getBoolean(ConstantCodes.DRIVE_MODE_WHITELIST_CALLS, false)) {
                        showIncommingActivity(incomingNumber);
                    } else if (Utility.getSharedPreference(context).getBoolean(ConstantCodes.DRIVE_MODE_UNKNOWN_CALLS, false)) {
                        audiomanager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
                        blockAndUpdate(incomingNumber);
                        sendVoiceMessage(incomingNumber, ConstantCodes.DRIVING_MODE_ENABLED);

                    } else if (Utility.getSharedPreference(context).getBoolean(ConstantCodes.DRIVE_MODE_UNWANTED_CALLS, false)) {
                        audiomanager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
                        blockAndUpdate(incomingNumber);
                        sendVoiceMessage(incomingNumber, ConstantCodes.DRIVING_MODE_ENABLED);
                    }
                } else if (Utility.getSharedPreference(context).contains(ConstantCodes.DND_TYPE)) {

                    boolean isBlock = true;
                    if (allowPriorityContact) {
                        ArrayList<Contacts> contactsList = new Select().from(Contacts.class)
                                .and(" '" + incomingNumber + "' LIKE '%'||number")
                                .or(" number like '%" + incomingNumber + "'")
                                .execute();
                        if (contactsList != null && contactsList.size() > 0 && contactsList.get(0).priority == 1) {
                            isBlock = false;
                        }
                    }

                    if (isBlock) {
                        audiomanager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
                        if (ConstantCodes.BLOCK_ALL.equalsIgnoreCase(Utility.getSharedPreference(context).getString(ConstantCodes.DND_TYPE, null))) {
                            blockAndUpdate(incomingNumber);
                            sendVoiceMessage(incomingNumber, ConstantCodes.DND_TYPE);
                        } else if (ConstantCodes.BLOCK_TIME.equalsIgnoreCase(Utility.getSharedPreference(context).getString(ConstantCodes.DND_TYPE, null))) {
                           Log.e("BLOCK_TIME",  " -- "+System.currentTimeMillis()+
                           "\n"+ Utility.getSharedPreference(context).getLong(ConstantCodes.TIME, 0));
                            long specific_Time = Utility.getSharedPreference(context).getLong(ConstantCodes.TIME, 0);
                            if (System.currentTimeMillis() < specific_Time) {
                                blockAndUpdate(incomingNumber);
                                sendVoiceMessage(incomingNumber, ConstantCodes.DND_TYPE);
                            } else {
                                showIncommingActivity(incomingNumber);
                            }
                        } else {
                            showIncommingActivity(incomingNumber);
                        }
                    } else {
                        showIncommingActivity(incomingNumber);
                    }

                } else {
                    audiomanager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                    SharedPreferences sharedPreferences = Utility.getSharedPreference(context);
                    boolean unknownNumbers = sharedPreferences.getBoolean(ConstantCodes.UNKNOWN_NUMBER, false);
                    boolean hiddenNumbers = sharedPreferences.getBoolean(ConstantCodes.HIDDEN_NUMBER, false);
                    boolean fccDoNotCall = sharedPreferences.getBoolean(ConstantCodes.DO_NOT_CALL, false);
                    boolean identifiedNumber = sharedPreferences.getBoolean(ConstantCodes.IDENTIFIED_NUMBER, false);//spam and unidentified
                    boolean allNumber = sharedPreferences.getBoolean(ConstantCodes.ALL_NUMBER, false);

                    if (allNumber) {
                        blockAndUpdate(incomingNumber);
//                        sendVoiceMessage(incomingNumber);
                    } else {
                        /*ArrayList<Contacts> contactsList = new Select().from(Contacts.class)
                                .and(" number <> '' ")
                                .and(" '" + incomingNumber + "' LIKE '%'||number")
                                .or(" number like '%" + incomingNumber + "'")
                                .execute();*/

                        ArrayList<Contacts> contactsList = new Select().from(Contacts.class).and(" number <> '' ").and(" ('" + incomingNumber + "' LIKE '%'||number").or(" number like '%" + incomingNumber + "')").execute();

                        if (contactsList != null && contactsList.size() > 0) {
                            //&& !TextUtils.isEmpty(contactsList.get(0).number)

                            boolean isBlock = false;
                            for (Contacts contacts : contactsList) {
                                if (ConstantCodes.BLACK_LIST.equalsIgnoreCase(contacts.blackListOrWhiteList)
                                        && (ConstantCodes.BLOCK_TYPE_CALLS.equalsIgnoreCase(contacts.block_type)
                                        || ConstantCodes.BLOCK_TYPE_BOTH.equalsIgnoreCase(contacts.block_type))) {
                                    blockAndUpdate(incomingNumber);
//                                    sendVoiceMessage(incomingNumber);
                                    isBlock = true;
                                    break;
                                }
                            }
                            if (isBlock == false) {
                                showIncommingActivity(incomingNumber);
                            }

                        } else  if (unknownNumbers) {
                            blockAndUpdate(incomingNumber);
//                                sendVoiceMessage(incomingNumber);
                        } else if (identifiedNumber || hiddenNumbers) {
                            //call webservice
                            ArrayList<SpamNumbers> spamNumberses = new Select().from(SpamNumbers.class)
                                    .and(" number <> '' ")
                                    .and(" '" + incomingNumber + "' LIKE '%'||number")
                                    .or(" number like '%" + incomingNumber + "'")
                                    .execute();
                            if (spamNumberses != null && spamNumberses.size() > 0) {
                                if (ConstantCodes.SPAM.equalsIgnoreCase(spamNumberses.get(0).type)
                                        || ConstantCodes.HIDDEN.equalsIgnoreCase(spamNumberses.get(0).type)) {
                                    blockAndUpdate(incomingNumber);
//                                        sendVoiceMessage(incomingNumber);
                                } else {
                                    showIncommingActivity(incomingNumber);
                                }
                            } else {
                                showIncommingActivity(incomingNumber);
                            }
                                /*
                                identifiednumber
                                    spam or not found then blockAndUpdate(incomingNumber);
                                hiddennumber
                                    hiddennumber=1 then blockAndUpdate(incomingNumber);
                                 */
                        }
                        else {
                            //not saved on local
                            if (unknownNumbers) {
                                blockAndUpdate(incomingNumber);
//                                sendVoiceMessage(incomingNumber);
                            } else if (identifiedNumber || hiddenNumbers) {
                                //call webservice
                                ArrayList<SpamNumbers> spamNumberses = new Select().from(SpamNumbers.class)
                                        .and(" number <> '' ")
                                        .and(" '" + incomingNumber + "' LIKE '%'||number")
                                        .or(" number like '%" + incomingNumber + "'")
                                        .execute();
                                if (spamNumberses != null && spamNumberses.size() > 0) {
                                    if (ConstantCodes.SPAM.equalsIgnoreCase(spamNumberses.get(0).type) || ConstantCodes.HIDDEN.equalsIgnoreCase(spamNumberses.get(0).type)) {
                                        blockAndUpdate(incomingNumber);
//                                        sendVoiceMessage(incomingNumber);
                                    } else {
                                        showIncommingActivity(incomingNumber);
                                    }
                                } else {
                                    showIncommingActivity(incomingNumber);
                                }
                                /*
                                identifiednumber
                                    spam or not found then blockAndUpdate(incomingNumber);
                                hiddennumber
                                    hiddennumber=1 then blockAndUpdate(incomingNumber);
                                 */
                            } else {
                                showIncommingActivity(incomingNumber);
                                Intent intentStatusChanged = new Intent(ConstantCodes.BROADCAST_CALL_STATUS_DISCONNECT);
                                BroadcastHelper.sendBroadcast(context, intentStatusChanged);
                            }
                        }
                    }
                }
            } else {
                //TODO:revert back the ringer volume if already setted.
                if (audiomanager != null)
                    audiomanager.setRingerMode(ringerMode);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Utility.saveLastCallLogInOurDatabase(context);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        String incomingNumber = Utility.getOnlyDigitNumber(intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER));
                        Intent intentBlocked = new Intent(ConstantCodes.BROADCAST_REFRESH);
                        intentBlocked.putExtra(ConstantCodes.INTENT_BLOCKED_CALL, true);
                        intentBlocked.putExtra(ConstantCodes.INTENT_BLOCKED_TYPE, ConstantCodes.CALL);
                        intentBlocked.putExtra(ConstantCodes.INTENT_NUMBER, incomingNumber);
                        BroadcastHelper.sendBroadcast(context, intentBlocked);
                    }
                }, 1000);

                Intent intentStatusChanged = new Intent(ConstantCodes.BROADCAST_CALL_STATUS_DISCONNECT);
                BroadcastHelper.sendBroadcast(context, intentStatusChanged);

                if (showIncommingScreen) {
                    if (intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER) != null) {
                        String incomingNumber = Utility.getOnlyDigitNumber(intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER));
                        Intent intent1 = new Intent(context, ContactAddDialogService.class);
                        intent1.putExtra(ConstantCodes.INTENT_NUMBER, incomingNumber);
                        context.startService(intent1);
                    }
                }
                showIncommingScreen = false;
            }
            lastRingingState=state;
        }
    }

    private void sendVoiceMessage(String incomingNumber, String blockFor) {
        Intent intent = new Intent(context, SendVoiceMailIntentService.class);
        intent.putExtra(ConstantCodes.INTENT_NUMBER, incomingNumber);
        intent.putExtra(ConstantCodes.INTENT_BLOCK_FOR, blockFor);
        context.startService(intent);
    }

    static boolean showIncommingScreen = false;

    private void showIncommingActivity(String incommingNumber) {
        Intent intent = new Intent(context, IncomingCall.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(ConstantCodes.INTENT_NUMBER, incommingNumber);
        context.startActivity(intent);
        showIncommingScreen = true;
    }

    private void blockAndUpdate(String incomingNumber) {
        if (endCall()) {
            CallsBlocked callsBlocked = new CallsBlocked();
            callsBlocked.number = incomingNumber;
            callsBlocked.timestamp = System.currentTimeMillis();
            callsBlocked.save();

            Intent intentBlocked = new Intent(ConstantCodes.BROADCAST_BLOCKED);
            BroadcastHelper.sendBroadcast(context, intentBlocked);

            ArrayList<Contacts> contactsList = new Select().from(Contacts.class)
                    .and("name <> ''")
                    .and(" '" + incomingNumber + "' LIKE '%'||number")
                    .or(" number like '%" + incomingNumber + "'")
                    .execute();

            String name = "Unknown";
            if (contactsList != null && contactsList.size() > 0) {
                name = contactsList.get(0).name;
            }
            Toast toast = Toast.makeText(context, "BLOCKED Call: " + name + "(" + incomingNumber + ")", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    private boolean endCall() { //919712630253
        TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {

            Method m = Class.forName(telephony.getClass().getName()).getDeclaredMethod("getITelephony");
            m.setAccessible(true);
            telephonyService = (ITelephony) m.invoke(telephony);
//            telephonyService.silenceRinger();
            telephonyService.endCall();

            setResultData(null);
            abortBroadcast();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
//            ignoreCallPackageRestart();
        }

        /*try {
            AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
            int currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_RING);
            audioManager.setStreamVolume(AudioManager.STREAM_RING, 0, 0);
            setResultData(null);
            abortBroadcast();
            Class c = Class.forName(telephony.getClass().getName());
            Method m = c.getDeclaredMethod("getITelephony");
            m.setAccessible(true);
            telephonyService = (ITelephony) m.invoke(telephony);
            telephonyService.silenceRinger();
            telephonyService.endCall();
            audioManager.setStreamVolume(AudioManager.STREAM_RING, currentVolume, 0);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        return false;
    }

    /**
     * package restart technique for ignoring calls
     */
    private void ignoreCallPackageRestart() {
        ActivityManager am = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
        am.restartPackage("com.android.providers.telephony");
        am.restartPackage("com.android.phone");
    }
}