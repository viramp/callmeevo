package callme.evo.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.services.ForgroundService;

/**
 * Created by pankaj on 24/8/16.
 */
public class CancelDNDReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Utility.getSharedPreference(context).edit().remove(ConstantCodes.DND_TYPE_CHOISE).commit();
        Utility.getSharedPreference(context).edit().remove(ConstantCodes.DND_TYPE).commit();
        Utility.getSharedPreference(context).edit().remove(ConstantCodes.TIME).commit();
        ForgroundService.startServiceForDND(context, true);
    }
}
