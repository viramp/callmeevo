package callme.evo.receiver;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.SmsMessage;
import android.view.Gravity;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.moez.QKSMS.receiver.MessagingReceiver;

import java.util.ArrayList;

import callme.evo.global.BroadcastHelper;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.Contacts;
import callme.evo.models.SpamNumbers;

public class SMSReceiver extends MessagingReceiver {
    /**
     * Called when the activity is first created.
     */
    /*https://stackoverflow.com/questions/42459079/abortbroadcast-in-android-6-0*/
    private static final String ACTION = "android.provider.Telephony.SEND_SMS";
    public static int MSG_TPE = 0;

    private AudioManager audiomanager;
    private static int ringerMode = -1;

    public void onReceive(final Context context, Intent intent) {

//        boolean isDefaultSmsApp = Utils.isDefaultSmsApp(context);
        if (!Utility.getSharedPreference(context).contains(ConstantCodes.USER_DATA)) {
            super.onReceive(context, intent);
            return;
        }

        if (audiomanager == null)
            audiomanager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        String MSG_TYPE = intent.getAction();

        if (MSG_TYPE.equals("android.provider.Telephony.SMS_RECEIVED")) {
//          Toast toast = Toast.makeText(context,"SMS Received: "+MSG_TYPE , Toast.LENGTH_LONG);
//          toast.show();

            ringerMode = audiomanager.getRingerMode();


            Bundle bundle = intent.getExtras();
            Object messages[] = (Object[]) bundle.get("pdus");
            SmsMessage smsMessage[] = new SmsMessage[messages.length];
            for (int n = 0; n < messages.length; n++) {
                smsMessage[n] = SmsMessage.createFromPdu((byte[]) messages[n]);
            }

            String incomingNumber = smsMessage[0].getOriginatingAddress();

            if (Utility.getSharedPreference(context).contains(ConstantCodes.DRIVING_MODE_ENABLED)) {
                onlyStopNotification(context, smsMessage[0].getOriginatingAddress(), smsMessage[0].getMessageBody(),intent);
            } else if (Utility.getSharedPreference(context).contains(ConstantCodes.DND_TYPE)) {
                if (ConstantCodes.BLOCK_ALL.equalsIgnoreCase(Utility.getSharedPreference(context).getString(ConstantCodes.DND_TYPE, null))) {
                    onlyStopNotification(context, smsMessage[0].getOriginatingAddress(), smsMessage[0].getMessageBody(),intent);
                } else if (ConstantCodes.BLOCK_TIME.equalsIgnoreCase(Utility.getSharedPreference(context).getString(ConstantCodes.DND_TYPE, null))) {
                    if (System.currentTimeMillis() < Utility.getSharedPreference(context).getLong(ConstantCodes.TIME, 0)) {
                        onlyStopNotification(context, smsMessage[0].getOriginatingAddress(), smsMessage[0].getMessageBody(),intent);
                    }
                }
            } else {

                SharedPreferences sharedPreferences = Utility.getSharedPreference(context);
                boolean unknownNumbers = sharedPreferences.getBoolean(ConstantCodes.MSG_UNKNOWN_NUMBER, false);
                boolean hiddenNumbers = sharedPreferences.getBoolean(ConstantCodes.MSG_HIDDEN_NUMBER, false);
                boolean allNumber = sharedPreferences.getBoolean(ConstantCodes.MSG_ALL_NUMBER, false);

                if (allNumber) {
                    abortBroadcast();
                    onlyStopNotification(context, smsMessage[0].getOriginatingAddress(), smsMessage[0].getMessageBody(),intent);

                    blockAndUpdate(context, smsMessage[0].getOriginatingAddress(), smsMessage[0].getMessageBody());
                }else if (unknownNumbers) {
                    abortBroadcast();
                    onlyStopNotification(context, smsMessage[0].getOriginatingAddress(), smsMessage[0].getMessageBody(),intent);

                    blockAndUpdate(context, smsMessage[0].getOriginatingAddress(), smsMessage[0].getMessageBody());
                } else if (hiddenNumbers) {
                    abortBroadcast();
                    onlyStopNotification(context, smsMessage[0].getOriginatingAddress(), smsMessage[0].getMessageBody(),intent);

                    //call webservice
                    ArrayList<SpamNumbers> spamNumberses = new Select().from(SpamNumbers.class)
                            .and(" number <> '' ")
                            .and(" '" + incomingNumber + "' LIKE '%'||number")
                            .or(" number like '%" + incomingNumber + "'")
                            .execute();
                    if (spamNumberses != null && spamNumberses.size() > 0) {
                        if (ConstantCodes.SPAM.equalsIgnoreCase(spamNumberses.get(0).type) || ConstantCodes.HIDDEN.equalsIgnoreCase(spamNumberses.get(0).type)) {
                            blockAndUpdate(context, smsMessage[0].getOriginatingAddress(), smsMessage[0].getMessageBody());
                        }
                    }
                }
                else {
                    ArrayList<Contacts> contactsList = new Select().from(Contacts.class)
                            .where("blackListOrWhiteList=? AND (block_type=? OR block_type=?)",
                                    new String[]{ConstantCodes.BLACK_LIST, ConstantCodes.BLOCK_TYPE_MESSAGE,
                                            ConstantCodes.BLOCK_TYPE_BOTH})
                            .and(" '" + incomingNumber + "' LIKE '%'||number")
                            .or(" number like '%" + incomingNumber + "'")
                            .execute();

                    if (contactsList != null && contactsList.size() > 0) {
                        // show first message

                        if (ConstantCodes.BLACK_LIST.equalsIgnoreCase(contactsList.get(0).blackListOrWhiteList)
                                && (ConstantCodes.BLOCK_TYPE_CALLS.equalsIgnoreCase(contactsList.get(0).block_type)
                                || ConstantCodes.BLOCK_TYPE_BOTH.equalsIgnoreCase(contactsList.get(0).block_type))) {
                            abortBroadcast();
                            blockAndUpdate(context, smsMessage[0].getOriginatingAddress(), smsMessage[0].getMessageBody());
                        } else {
                            //not saved on local
//                            if (unknownNumbers) {
//                                blockAndUpdate(context, smsMessage[0].getOriginatingAddress(), smsMessage[0].getMessageBody());
//                            } else if (hiddenNumbers) {
//
//                                //call webservice
//                                ArrayList<SpamNumbers> spamNumberses = new Select().from(SpamNumbers.class)
//                                        .and(" number <> '' ")
//                                        .and(" '" + incomingNumber + "' LIKE '%'||number")
//                                        .or(" number like '%" + incomingNumber + "'")
//                                        .execute();
//                                if (spamNumberses != null && spamNumberses.size() > 0) {
//                                    if (ConstantCodes.SPAM.equalsIgnoreCase(spamNumberses.get(0).type) || ConstantCodes.HIDDEN.equalsIgnoreCase(spamNumberses.get(0).type)) {
//                                        blockAndUpdate(context, smsMessage[0].getOriginatingAddress(), smsMessage[0].getMessageBody());
//                                    }
//                                }
//                            }
                        }
                    }
                }
            }

            if (isMessageBlocked == false) {
                super.onReceive(context, intent);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Utility.saveLastSmsToOurDatabase(context, null);
                        Intent intentBlocked = new Intent(ConstantCodes.BROADCAST_REFRESH);
                        BroadcastHelper.sendBroadcast(context, intentBlocked);
                    }
                }, 1000);
            }
            isMessageBlocked = false;
            if (audiomanager != null)
                audiomanager.setRingerMode(ringerMode);

        } else if (MSG_TYPE.equals("android.provider.Telephony.SEND_SMS")) {
            Toast toast = Toast.makeText(context, "SMS SENT: " + MSG_TYPE, Toast.LENGTH_LONG);
            toast.show();
            abortBroadcast();

        } else {

            Toast toast = Toast.makeText(context, "SIN ELSE: " + MSG_TYPE, Toast.LENGTH_LONG);
            toast.show();
            abortBroadcast();
            for (int i = 0; i < 8; i++) {
                System.out.println("Blocking SMS **********************");
            }
        }

    }

    private void onlyStopNotification(Context context, String incommingNumber, String body, Intent intent){
        blockAndUpdate(context, incommingNumber, body);
        super.onReceive(context, intent, false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Utility.saveLastSmsToOurDatabase(context, null);
                Intent intentBlocked = new Intent(ConstantCodes.BROADCAST_REFRESH);
                BroadcastHelper.sendBroadcast(context, intentBlocked);
            }
        }, 700);
    }

    boolean isMessageBlocked = false;

    private void blockAndUpdate(Context context, String incommingNumber, String body) {
        if (audiomanager != null)
            audiomanager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
        isMessageBlocked = true;
        abortBroadcast();

        Intent intentBlocked = new Intent(ConstantCodes.BROADCAST_BLOCKED);
        BroadcastHelper.sendBroadcast(context, intentBlocked);

        ArrayList<Contacts> contactsList = new Select().from(Contacts.class)
                .and("name <> ''")
                .and(" '" + incommingNumber + "' LIKE '%'||number")
                .or(" number like '%" + incommingNumber + "'")
                .execute();

        String name = "Unknown";
        if (contactsList != null && contactsList.size() > 0) {
            name = contactsList.get(0).name;
        }
        Toast toast = Toast.makeText(context, "BLOCKED Received SMS: " + name + "(" + incommingNumber + ")", Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

        Intent intentBlockedForDrivingMode = new Intent(ConstantCodes.BROADCAST_REFRESH);
        intentBlockedForDrivingMode.putExtra(ConstantCodes.INTENT_BLOCKED_CALL, true);
        intentBlockedForDrivingMode.putExtra(ConstantCodes.INTENT_BLOCKED_TYPE, ConstantCodes.SMS);
        intentBlockedForDrivingMode.putExtra(ConstantCodes.INTENT_NUMBER, incommingNumber);
        intentBlockedForDrivingMode.putExtra(ConstantCodes.INTENT_MESSAGE, body);
        BroadcastHelper.sendBroadcast(context, intentBlockedForDrivingMode);
    }

}
