package callme.evo.receiver;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.activeandroid.query.Select;

import java.util.ArrayList;

import callme.evo.global.BroadcastHelper;
import callme.evo.global.ConstantCodes;
import callme.evo.global.Utility;
import callme.evo.models.BluetoothModel;
import callme.evo.services.BluetoothService;
import callme.evo.services.ForgroundService;

public class MyBluetoothReceiver extends BroadcastReceiver {

    public final String CONNECTED = "android.bluetooth.device.action.ACL_CONNECTED";//BluetoothDevice.ACTION_ACL_CONNECTED
    public final String DISCONNECTED = "android.bluetooth.device.action.ACL_DISCONNECTED";//BluetoothDevice.ACTION_ACL_DISCONNECTED
    private Context mContext;

    /**
     * Handles changes in widget operation when Bluetooth state changes.
     * @param state one of the BluetoothAdapter on/off and transitioning states
     */
    private void bluetoothStateChanged(int state) {
        switch (state) {
            case BluetoothAdapter.STATE_OFF:
                ForgroundService.startServiceForDrivingMode(mContext, true, false);
                mContext.stopService(new Intent(mContext, BluetoothService.class));
                Utility.getSharedPreference(mContext).edit().
                        putBoolean(ConstantCodes.IS_DRIVING_MODE_OPEN,false).apply();
                break;
            case BluetoothAdapter.STATE_TURNING_ON:
            case BluetoothAdapter.STATE_ON:
                mContext.startService(new Intent(mContext, BluetoothService.class));
                break;
            case BluetoothAdapter.STATE_TURNING_OFF:
                ForgroundService.startServiceForDrivingMode(mContext, true, false);
                mContext.stopService(new Intent(mContext, BluetoothService.class));
                Utility.getSharedPreference(mContext).edit().
                        putBoolean(ConstantCodes.IS_DRIVING_MODE_OPEN,false).apply();
                break;

        }
    }

    /**
     * Handles changes in widget operation when a device's A2DP connection state changes
     * @param device the BluetoothDevice whose A2DP connection state has changed
     * @param state the new state, one of the BluetoothProfile connection and transitioning states
     */
    private void a2dpStateChanged(int state, BluetoothDevice device) {
        switch (state) {
            case BluetoothProfile.STATE_DISCONNECTED:
                ForgroundService.startServiceForDrivingMode(mContext, true, false);
                mContext.stopService(new Intent(mContext, BluetoothService.class));
                Utility.getSharedPreference(mContext).edit().
                        putBoolean(ConstantCodes.IS_DRIVING_MODE_OPEN,false).apply();
                break;
            case BluetoothProfile.STATE_CONNECTING:
            case BluetoothProfile.STATE_CONNECTED:
            case BluetoothProfile.STATE_DISCONNECTING:
                ForgroundService.startServiceForDrivingMode(mContext, true, false);
                mContext.stopService(new Intent(mContext, BluetoothService.class));
                Utility.getSharedPreference(mContext).edit().
                        putBoolean(ConstantCodes.IS_DRIVING_MODE_OPEN,false).apply();
                break;
                // TODO: trigger changes in how widget handles clicks and displays this device

        }
    }

    /**
     * Handles changes in widget operation when a device's bond state changes
     * @param intent
     * @param state the new state, one of the BluetoothDevice BOND and transitioning states
     * @param device the BluetoothDevice whose bond state has changed
     */
    public void bondStateChanged(Intent intent, int state, BluetoothDevice device) {
        switch (state) {
//            Log.d("bond bluetoothStateChanged", "Bluetooth bond state changed to " + state + " for " + device.getName());
            case BluetoothDevice.BOND_NONE:
                Toast.makeText(mContext, "BLUETOOTH DISCONNECTED", Toast.LENGTH_LONG).show();
                ForgroundService.startServiceForDrivingMode(mContext, true, false);
             break;
            case BluetoothDevice.BOND_BONDING:


            case BluetoothDevice.BOND_BONDED:
                if (intent.hasExtra(BluetoothDevice.EXTRA_DEVICE)) {
                    // Get the BluetoothDevice object from the Intent
//                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    StringBuilder fields = new StringBuilder();
                    fields.append("\nNames : " + device.getName());
                    fields.append("\nAddre : " + device.getAddress());
                    fields.append("\nUUIDs : " + device.getUuids());

                    ArrayList<BluetoothModel> list = new Select().from(BluetoothModel.class).where("address='" + device.getAddress() + "' AND isDefault='1'").execute();
                    if (list != null && list.size() > 0) {
                        Toast.makeText(mContext, "CONNECTED TO CARS's BlUETOOTH", Toast.LENGTH_LONG).show();
                        ForgroundService.startServiceForDrivingMode(mContext, false, false);
                    }

                    BluetoothModel bluetoothModel = new BluetoothModel();
                    bluetoothModel.name = device.getName();
                    bluetoothModel.address = device.getAddress();
                    bluetoothModel.save();

                }

                BroadcastHelper.sendBroadcast(mContext, ConstantCodes.BROADCAST_BLUETOOTH_REFRESH);

            break;
                // TODO: trigger changes in how widget handles clicks and displays this device

        }
    }

    private static final int FAIL = -1;
    @Override
    public void onReceive(Context context, Intent intent) {

            mContext = context;
//        Toast.makeText(context, "RECEIVER CALLED!!", Toast.LENGTH_LONG).show();

        /*if (intent.getAction().equals("android.bluetooth.BluetoothDevice.ACTION_ACL_CONNECTED")) {

            // code for Bluetooth connect

            Toast.makeText(context, "BLUETOOTH CONNECTED!!", Toast.LENGTH_LONG).show();
        }

        if (intent.getAction().equals("android.bluetooth.device.action.ACL_DISCONNECTED")) {

            //code for Bluetooth disconnect;
            Toast.makeText(context, "BLUETOOTH DISCONNECTED", Toast.LENGTH_LONG).show();
        }

*/
        String getAction= intent.getAction();
        Bundle extras = intent.getExtras();            // Get all the Intent's extras
        if (extras == null) return;                    // All intents of interest have extras.
        switch (getAction) {


            case "android.bluetooth.adapter.action.STATE_CHANGED": {
                bluetoothStateChanged(extras.getInt("android.bluetooth.adapter.extra.STATE", FAIL));
                break;
            }
            case "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED": {
                a2dpStateChanged(
                        extras.getInt("android.bluetooth.profile.extra.STATE", FAIL),
                        (BluetoothDevice) extras.get("android.bluetooth.device.extra.DEVICE"));
                break;
            }
            case "android.bluetooth.device.action.BOND_STATE_CHANGED": {
                bondStateChanged(intent,
                        extras.getInt("android.bluetooth.device.extra.BOND_STATE", FAIL),
                        (BluetoothDevice) extras.get("android.bluetooth.device.extra.DEVICE"));
                break;
            }
        }


        if (BluetoothDevice.ACTION_ACL_CONNECTED.equalsIgnoreCase(intent.getAction())) {

//            Toast.makeText(context, "BLUETOOTH CONNECTED!!", Toast.LENGTH_LONG).show();
            // code for Bluetooth connect
            if (intent.hasExtra(BluetoothDevice.EXTRA_DEVICE)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                StringBuilder fields = new StringBuilder();
                fields.append("\nNames : " + device.getName());
                fields.append("\nAddre : " + device.getAddress());
                fields.append("\nUUIDs : " + device.getUuids());

                ArrayList<BluetoothModel> list = new Select().from(BluetoothModel.class).where("address='" + device.getAddress() + "' AND isDefault='1'").execute();
                if (list != null && list.size() > 0) {
                    Toast.makeText(context, "CONNECTED TO CARS's BlUETOOTH", Toast.LENGTH_LONG).show();
                    ForgroundService.startServiceForDrivingMode(context, false, false);
                }

                BluetoothModel bluetoothModel = new BluetoothModel();
                bluetoothModel.name = device.getName();
                bluetoothModel.address = device.getAddress();
                bluetoothModel.save();

            }

            BroadcastHelper.sendBroadcast(context, ConstantCodes.BROADCAST_BLUETOOTH_REFRESH);
            return;

        } else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equalsIgnoreCase(intent.getAction())) {
            //code for Bluetooth disconnect;
            Toast.makeText(context, "BLUETOOTH DISCONNECTED", Toast.LENGTH_LONG).show();
            ForgroundService.startServiceForDrivingMode(context, true, false);
            return;
        }

        /*Toast.makeText(context, "BLUETOOTH: " + action, Toast.LENGTH_LONG).show();
        Toast.makeText(context, "BLUETOOTH: " + action, Toast.LENGTH_LONG).show();
        // When discovery finds a device
        if (intent.hasExtra(BluetoothDevice.EXTRA_DEVICE)) {
            // Get the BluetoothDevice object from the Intent
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            StringBuilder fields = new StringBuilder();
            fields.append("\nNames : " + device.getName());
            fields.append("\nAddre : " + device.getAddress());
            fields.append("\nUUIDs : " + device.getUuids());
            //you can get name by device.getName()
            Toast.makeText(context, "BLUETOOTH: " + fields.toString(), Toast.LENGTH_LONG).show();
        }*/
    }
}