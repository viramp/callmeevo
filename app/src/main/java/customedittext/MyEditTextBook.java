package customedittext;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by one on 3/12/15.
 */
public class MyEditTextBook extends EditText {

    public MyEditTextBook(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MyEditTextBook(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyEditTextBook(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/GothamRounded-Book.otf");
            setTypeface(tf);
        }
    }

}