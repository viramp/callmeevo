package com.activeandroid;

import java.util.ArrayList;

/**
 * Created by pankaj on 6/29/16.
 */
public interface OnReadAsync<T extends Model> {
    public void onRead(ArrayList<T> list);
}
