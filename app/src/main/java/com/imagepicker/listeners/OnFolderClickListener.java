package com.imagepicker.listeners;


import com.imagepicker.model.Folder;

/**
 * Created by boss1088 on 8/23/16.
 */
public interface OnFolderClickListener {

    void onFolderClick(Folder bucket);
}
